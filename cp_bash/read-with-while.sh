#!/bin/bash
# @snippet while loop
while read ip name alias
do
          if [ ! -z "$name" ]; then
              echo -en "IP is $ip - its name is $name"
              if [ ! -z "$aliase"]; then
                  echo "   Aliases: $aliase"
              else
                  # just echo a blank line
                  echo
              fi
           fi
done < /etc/hosts
