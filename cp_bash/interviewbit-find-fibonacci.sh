#!/bin/sh
fibonacci()
{
    h=1
    t=1
    res=1
    if [ $1 -eq '1' ]
    then
        echo '1'
        return
    fi

    if [ $1 -eq '2' ]
    then
        echo '1'
        return
    fi

    n=$1
    n=$((n-2))
    for i in `seq 1 "$n"`
    do
        res=$((h+t))
        t="$h"
        h="$res"
    done
    echo "$res"
}

n=1
while read line
do
    i=$((i+1))
    if [ $i -eq 1 ]
    then
        n="$line"
    else
        res=`fibonacci "$line"`
        echo $res
    fi
done < input



