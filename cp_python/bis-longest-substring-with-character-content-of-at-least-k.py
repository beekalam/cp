import collections


# https://binarysearch.com/problems/Longest-Substring-with-Character-Count-of-at-Least-K

class Solution:
    def solve(self, s, k):
        counter = collections.Counter()
        ws = 0
        we = 0
        mx = 0
        while we < len(s):
            counter.update(s[we])
            print(counter)

            if len(set(list(counter.values()))) == 1:
                mx = max(sum(list(counter.values())), mx)

            # print(list(counter.values()))

            while ws < len(s) and len(set(list(counter.values()))) > 1:
                # print(counter)
                counter.subtract(s[ws])
                if counter[s[ws]] == 0:
                    del counter[s[ws]]
                ws += 1
            we += 1

        return mx


s = Solution()
assert s.solve("abccddeefg", 2) == 2, "test1"
