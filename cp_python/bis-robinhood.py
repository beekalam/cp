class Solution:
    def solve(self, n, e, o, t):
        interests = [e,o]
        count = 0
        s = n
        while s < t:
            interest = (interests[count % 2] * s)/100
            s += interest
            count += 1
        return count
