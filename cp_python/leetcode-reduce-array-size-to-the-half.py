# 2021-11-15
import heapq
import collections
from typing import List
class Solution:
    def minSetSize(self, arr: List[int]) -> int:
        n = len(arr)
        freq = collections.Counter(arr).most_common()
        freq.sort(key=lambda x: x[1],reverse=True)
        min_set_size = freq[0][1]
        set_size = 1
        for i in range(1,len(freq)):
            if min_set_size < n // 2:
                min_set_size += freq[i][1]
                set_size += 1
        return set_size




s = Solution()
arr = [3,3,3,3,5,5,5,2,2,7]
assert s.minSetSize(arr) == 2, "test1"
