# https://leetcode.com/problems/build-an-array-with-stack-operations/submissions/
class Solution(object):
    def buildArray(self, target, n):
        """
        :type target: List[int]
        :type n: int
        :rtype: List[str]
        """
        ans = []
        arr = []
        for i in range(1,n+1):
            if i in target:
                arr.append(i)
                ans.append("Push")
            else:
                ans.append("Push")
                ans.append("Pop")
            if arr == target:
                return ans
        return ans
