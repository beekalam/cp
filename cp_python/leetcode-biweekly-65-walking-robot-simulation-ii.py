# @notsolved 2021-11-13
from typing import List


class Robot:

    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height
        self.direction = 'East'
        self.x = 0
        self.y = 0

    def move(self, num: int) -> None:
        i = 0
        while i < num:
            mx = self.max_in_direction(num - i)
            self.x, self.y = self.nextPos(mx)
            if self.x in [self.width, 0] or self.y in [self.height, 0]:
                self.changeDir()
            i += mx

    def max_in_direction(self, rem: int) -> int:
        if self.direction == 'East':
            return min(rem, self.width - self.x)
        if self.direction == 'West':
            return min(rem, self.x - 0)
        if self.direction == 'North':
            return min(rem, self.height - self.y)
        if self.direction == 'South':
            return min(rem, self.y - 0)

    def nextPos(self, dist=1):
        if self.direction == 'East':
            return [self.x + dist, self.y]
        if self.direction == 'West':
            return [self.x - dist, self.y]
        if self.direction == 'North':
            return [self.x, self.y + dist]
        if self.direction == 'South':
            return [self.x, self.y - dist]

    def changeDir(self):
        change = {
            'East': 'North',
            'North': 'West',
            'West': 'South',
            'South': 'East'
        }
        self.direction = change[self.direction]

    def getPos(self) -> List[int]:
        ret = [self.x, self.y]
        print("getPost: ", ret)
        return ret

    def getDir(self) -> str:
        return self.direction


# Your Robot object will be instantiated and called as such:
# obj = Robot(width, height)
# obj.move(num)
# param_2 = obj.getPos()
# param_3 = obj.getDir()
# ----------------------------------------------------
r = Robot(6, 3)
r.move(2)  # It moves two steps East to (2, 0), and faces East.
r.move(2)  # It moves two steps East to (4, 0), and faces East.
assert r.getPos() == [4, 0], 'test1'
assert r.getDir() == 'East', 'test2'
r.move(2)
r.move(1)
r.move(4)
assert r.getPos() == [1, 2], "test3"
assert r.getDir() == "West", "test4"

# r = Robot(10 ** 6, 10 ** 7)
# for i in range(10 ** 4):
#     r.move(1000)
