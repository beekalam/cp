h, w = list(map(int, input().split()))
A = []
for i in range(h):
    A.append(list(map(int, input().split())))
ans = 0
count = 0
for i in range(h - 1):
    for j in range(w - 1):
        count += 1
        if A[i][j] + A[i + 1][j + 1] <= A[i + 1][j] + A[i][j + 1]:
            ans += 1
for j in range(w - 1):
    for i in range(h - 1):
        count += 1
        if A[i][j] + A[i + 1][j + 1] <= A[i + 1][j] + A[i][j + 1]:
            ans += 1
# print(ans)
# print("count: ", count)
if count == ans:
    print("Yes")
else:
    print("No")
