# 2021-12-02
# class Tree:
#     def __init__(self, val, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def do_solve(self, root,ans, deg):
        if root:
            ans.add(deg)
            self.do_solve(root.right, ans, deg + 45)
            self.do_solve(root.left, ans, deg - 45)

    def solve(self, root):
        ans = set()

        self.do_solve(root, ans,0)
        return len(ans)
