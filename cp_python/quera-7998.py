# 2021-12-22
n = int(input())
caps = False
ans = ""
for _ in range(n):
    c = input()
    if c == 'CAPS':
        caps = not caps
    else:
        if caps:
            ans += c.upper()
        else:
            ans += c
print(ans)
