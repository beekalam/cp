# 2021-11-10
from collections import Counter
class Solution(object):
    def findLucky(self, arr):
        """
        :type arr: List[int]
        :rtype: int
        """
        ans = [i for i,j in Counter(arr).most_common() if i == j]
        return -1 if len(ans) == 0 else max(ans)
