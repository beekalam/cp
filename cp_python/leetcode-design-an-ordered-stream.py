class OrderedStream(object):

    def __init__(self, n):
        """
        :type n: int
        """
        self.n = n
        self.arr = [None] * n
        self.ptr = 0

    def insert(self, idKey, value):
        """
        :type idKey: int
        :type value: str
        :rtype: List[str]
        """
        self.arr[idKey - 1] = value
        ret = []
        while self.ptr < self.n and self.arr[self.ptr] is not None:
            ret.append(self.arr[self.ptr])
            self.ptr += 1
        return ret

# Your OrderedStream object will be instantiated and called as such:
# obj = OrderedStream(n)
# param_1 = obj.insert(idKey,value)
def test():
    os = OrderedStream(5);
    ret = os.insert(3, "ccccc"); # Inserts (3, "ccccc"), returns [].
    assert ret == []
    ret = os.insert(1, "aaaaa"); # Inserts (1, "aaaaa"), returns ["aaaaa"].
    assert ret == ["aaaaa"]
    ret = os.insert(2, "bbbbb"); # Inserts (2, "bbbbb"), returns ["bbbbb", "ccccc"].
    assert ret == ["bbbbb", "ccccc"]
    ret = os.insert(5, "eeeee"); # Inserts (5, "eeeee"), returns [].
    assert ret == []
    ret = os.insert(4, "ddddd"); # Inserts (4, "ddddd"), returns ["ddddd", "eeeee"].
    assert ret == ["ddddd", "eeeee"]
