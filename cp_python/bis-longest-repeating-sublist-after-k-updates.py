from collections import Counter
class Solution:
    def solve(self, nums,k):
        n = len(nums)
        if k == 0 and n == 0:
            return 0
        if n <= 1:
            return n
        i = 0
        j = 0
        c = Counter()
        # c[nums[i]] += 1
        # c[nums[j]] += 1
        ans = 0
        while i < n:
            if j < n and  len(c) > 0 and sum(c.values()) - c.most_common(1)[0][1] <= k:
                # print("j: {}".format(j))
                # print(c.most_common(1))
                if j < n :
                    c[nums[j]] += 1
                    ans = max(ans, sum(c.values()))
                j += 1
            else:
                c[nums[i]] -= 1
                i += 1
            ans = max(ans, sum(c.values()))
        # ans = max(ans, sum(c.values()))
        print("ans:{}".format(ans))
        return ans






s = Solution()
nums = [7, 5, 5, 3, 2, 5, 5]
k = 2
assert s.solve(nums, k) == 6, "test1"

nums = [1]
k = 0
assert s.solve(nums, k) == 1, "test1"


nums = [2, 1]
k = 0
assert s.solve(nums, k) == 1, "test5"


nums = [1, 2, 2]
k = 1
assert s.solve(nums, k) == 3, "test6"

nums = [1, 1]
k = 0
assert s.solve(nums, k) == 2, "test7"



nums = [1, 2,3]
k = 0
assert s.solve(nums, k) == 1, "test7"
