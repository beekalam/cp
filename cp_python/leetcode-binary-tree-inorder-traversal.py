# 2021-11-12
# https://leetcode.com/problems/binary-tree-inorder-traversal/submissions/
from typing import Optional
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:

    def recursive(self, root,ans):
        if root is None:
            return
        if root.left:
            self.recursive(root.left,ans)
        ans.append(root.val)
        if root.right:
            self.recursive(root.right,ans)

    # @snippet tree inorder iterative traversal
    def inorderTraversal(self, root: Optional[TreeNode]) -> List[int]:

        # self.recursive(root,ans)
        # return ans

        stack = []
        ans = []
        curr = root
        while curr or stack:
            while curr:
                stack.append(curr)
                curr = curr.left
            curr = stack.pop()
            ans.append(curr.val)
            curr = curr.right
        return ans
