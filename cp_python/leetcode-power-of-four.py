# https://leetcode.com/problems/power-of-four/
import math
class Solution(object):
    def bruteForce(self,n):
        idx = 0
        while True:
            t = pow(4, idx)
            if t == n:
                return True
            if t > n:
                return False
            idx += 1
    def isPowerOfFour(self, n):
        """
        :type n: int
        :rtype: bool
        """
        return self.bruteForce(n)
