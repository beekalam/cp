class Solution:
    def solve(self, s, pairs):
        m = {}
        for a,b in pairs:
            if a in m:
                m[a].add(b)
            else:
                m[a] = set(b)
        i = 0
        j = len(s) - 1
        inmap =0
        while i < j:
            if s[i] != s[j]:
                if s[i] in m and s[j] in m[s[i]]:
                    inmap += 1
                else:
                    return False

            i += 1
            j -= 1
        return True


def test_1():
    ss = "acba"
    pairs = [["b", "c"]]
    s = Solution()
    print(s.solve(ss,pairs))
test_1()
