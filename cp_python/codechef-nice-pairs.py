import string
import random
import time
class Solution:
    def solve(self):
        t = int(input())
        for i in range(t):
            n = input()
            print(self.count_nice_pairs(input()))
    def count_nice_pairs(self, s):
        n = len(s)
        count = 0
        take = min(10,n)
        for i in range(n):
            for j in range(i+1,min(i+11,n)):
                c,d = int(s[i]),int(s[j])
                # print("c:{},d:{}".format(c,d))
                if j - i == abs(d - c):
                    count += 1
        return count

s = Solution()
print (s.count_nice_pairs("123"))
print (s.count_nice_pairs("94241234"))
# s.solve()

def random_str_int():
    ret = []
    l = list(string.digits) * 1000
    for i in range(1,100):
        random.shuffle(l)
        ret.append(''.join(l))
    return ''.join(ret)

# len(random_str_int())
ss = random_str_int()

start = time.time()
print(s.count_nice_pairs(ss))
print(time.time() - start)
