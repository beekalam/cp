# @notsolved: 2021-12-22
from typing import List
import itertools


class Solution:
    def brute_force(self, n: int) -> List[str]:
        lst = list(range(1, n * 2))
        # print(lst)
        res = list(itertools.permutations(list(range(1, n * 2)), r=n))
        return res

    def generateParenthesis(self, n: int) -> List[str]:
        return self.brute_force(n)

        def embed(item):
            mid = len(item) // 2
            return item[0:mid] + '()' + item[mid:]

        def expand(item):
            return '(' + item + ')'

        def extend(item):
            return item[:] + '()'

        def extend2(item):
            return '()' + item[:]

        combs = []

        def generate(pairs):
            if pairs != 0:
                if len(combs) == 0:
                    combs.append('()')
                    generate(pairs - 1)
                else:
                    newCombs = []
                    for item in combs[:]:
                        newCombs.append(embed(item))
                        newCombs.append(expand(item))
                        newCombs.append(extend(item))
                        newCombs.append(extend2(item))
                    # print(newCombs)
                    combs.clear()
                    # for item in newCombs:
                    #     if item not in combs:
                    #         combs.append(item)
                    combs.extend(newCombs)
                    generate(pairs - 1)

        generate(n)
        # print(">................................")
        # print(list(set(combs)))
        return list(set(combs))


def listEqual(expected, current):
    if len(expected) != len(current):
        return False
    for item in expected:
        if current.index(item) == -1:
            return False
    return True


def listDiff(expected, got):
    res = []
    for item in expected:
        if item not in got:
            res.append(item)
    return res


s = Solution()
s.generateParenthesis(3)

# s = Solution()
# res = s.generateParenthesis(3)
# expected = ["((()))", "(()())", "(())()", "()(())", "()()()"]
# assert listEqual(expected, res), "test1"
#
# assert listEqual(['()'], s.generateParenthesis(1)), "test2"
# res = s.generateParenthesis(9)

# a = ["(((())))", "((()()))", "((())())", "((()))()", "(()(()))", "(()()())", "(()())()", "(())(())", "(())()()",
#      "()((()))", "()(()())", "()(())()", "()()(())", "()()()()"]
#
# b = ["()()(())", "(())()()", "((())())", "((()))()", "(()(()))", "()(()())", "(()()())", "()(())()", "((()()))",
#      "()((()))", "()()()()", "(((())))", "(()())()"]
#
# print(len(a))
# print(len(b))
#
# print(listDiff(a, b))
