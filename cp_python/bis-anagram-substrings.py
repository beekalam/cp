class Solution:
    def is_anagram(self,y):
        return self.x == sorted(y)

    def solve(self, s0, s1):
        self.x = sorted(s0)
        m = len(s0)
        n = len(s1)
        ans = 0
        for i in range(n):
            if i + m <= n:
                # print ("checking for: {}".format(s1[i:i+m]) )
                if s1[i] in self.x and  self.is_anagram(s1[i:i+m]):
                    ans += 1
        print("ans: {}".format(ans))
        return ans
    
s = Solution()
assert s.solve("abc","bcabxabc") == 3, "failed test 1"
