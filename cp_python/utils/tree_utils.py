# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def create_tree1(arr):
    def InOrder(root):
        if root and len(arr):
            valueLeft = arr.pop()
            if valueLeft is not None:
                root.left = TreeNode(valueLeft)
            if len(arr):
                valueRight = arr.pop()
                if valueRight is not None:
                    root.right = TreeNode(valueRight)

    root = TreeNode(arr.pop())
    InOrder(root)
    return root


root = create_tree([1, 2, 2, None, 3, None, 3])


def levelOrder(root):
    queue = []
    if not root:
        return
    queue.append(root)
    while len(queue):
        tmp = queue.pop(0)
        print(tmp.val)
        if tmp.left:
            queue.append(tmp.left)
        if tmp.right:
            queue.append(tmp.right)




