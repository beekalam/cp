# https://leetcode.com/problems/all-elements-in-two-binary-search-trees/discuss/829720/Python-O(n1-%2B-n2)-inorder-traversal-explained

# @snippet.tree.inorder-traversal
class InorderBST:

    def __init__(self, root):
        self.stack = []
        self.fill_lefts(root)

    def fill_lefts(self, node):
        if not node:
            return
        while node:
            self.stack.append(node)
            node = node.left

    def inorder(self):
        while self.stack:
            top = self.stack.pop()
            self.fill_lefts(top.right)
            yield top.val

    def nxt(self):
        return self.stack[-1].val if self.stack else None
