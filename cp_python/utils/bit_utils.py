def clear_lowest_bit(x):
    return x & (x - 1)


def extract_lowest_bit(x):
    return x & ~(x - 1)


def set_ith_bit(x, i):
    x |= (1 << i)
    return x


def unset_ith_bit(x, i):
    return x & ~(1 << i)


def invert_ith_bit(x, i):
    return x & (1 << i)


def extract_ith_bit(x, i):
    return (x >> i) & 1


def invert_all_after_the_last_one_bit(x):
    #  inverts all the bits after the last one bit.
    return x | (x - 1)


def set_all_one_to_zero_except_last(x):
    # sets all one bits to zero, except for the last one bit.
    return x & -x


def is_power_of_two(x):
    return x & (x - 1) == 0


def is_power_of_four(num):
    """
Good solution without good explanation,it's easy to find that power of 4 numbers have those 3 common features.First,greater than 0.Second,only have one '1' bit in their binary notation,so we use x&(x-1) to delete the lowest '1',and if then it becomes 0,it prove that there is only one '1' bit.Third,the only '1' bit should be locate at the odd location,for example,16.It's binary is 00010000.So we can use '0x55555555' to check if the '1' bit is in the right place.With this thought we can code it out easily!
    """
    return num > 0 and (num & (num - 1)) == 0 and (num & 0x55555555) != 0


def swap_bits(x, i, j):
    # Extract the i-th and j-th bits, and see if they differ.
    if (x >> i) & 1 != (x >> j) & 1:
        # i-th and j-th bits differ. We will swap them by flipping their values.
        # Select the bits to flip with bit_mask. Since x^1 = 0 when x = 1 and 1
        # when x = 0, we can perform the flip XOR.
        bit_mask = (1 << i) | (1 << j)
        # print(bit_mask)
        # print(bin(bit_mask))
        x ^= bit_mask
    return x


def invert_to_ith_bit(x, i):
    while i >= 0:
        if (x >> i) & 1:
            x = x & ~(1 << i)
        else:
            x |= (1 << i)
        i -= 1
    return x


def flip_bits(x, i):
    return x ^ ((1 << i) - 1)




# print(set_ith_bit(x, 1))
# print(swap_bits(x,0,4))
# print(is_power_of_four(16))
# print(is_power_of_four(8))
xy = 1024
print(bin(xy))
zy = flip_bits(xy,3)
print(bin(zy))
