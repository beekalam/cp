from sys import stdin
import os
import sys


def read_int():
    return int(stdin.readline())


def read_array(typ):
    return list(map(typ, stdin.readline().split()))


def read_matrix(n):
    M = []
    for _ in range(n):
        row = read_array(int)
        assert len(row) == n
        M.append(row)
    return M


def edit_distance(a, b):
    n = len(a)
    m = len(b)
    dp = [[None] * (m + 1) for i in range(n + 1)]

    for i in range(n + 1):
        for j in range(m + 1):
            if i == 0:
                dp[i][j] = j
            elif j == 0:
                dp[i][j] = i
            elif a[i - 1] == b[j - 1]:
                dp[i][j] = dp[i - 1][j - 1]
            else:
                dp[i][j] = 1 + min(dp[i - 1][j], min(dp[i][j - 1], dp[i - 1][j - 1]))
    return dp[n][m]


def longest_common_substring(a, b):
    ans, mi, mj = 0, 0, 0
    for i in range(len(a)):
        for j in range(len(b)):
            if a[i] == b[j]:
                count, m, n = 0, i, j
                while m < len(a) and n < len(b) and a[m] == b[n]:
                    m += 1
                    n += 1
                    count += 1
                if count > ans:
                    ans = count
                    mi = i
                    mj = j
    return mi, mj, ans


class RobinKarp:

    def __init__(self):
        # http://www.wolframalpha.com/input/?i=nearest+prime+number+to+2**56
        # snip{ rabin_karp_roll_hash
        self.PRIME = 72057594037927931  # < 2^{56}
        self.DOMAIN = 128

    def roll_hash(self, old_val, out_digit, in_digit, last_pos):
        val = (old_val - out_digit * last_pos + self.DOMAIN * self.PRIME) % self.PRIME
        val = (val * self.DOMAIN) % self.PRIME
        return (val + in_digit) % self.PRIME

    def matches(self, s, t, i, j, k):
        """tests if s[i:i + k] equals t[j:j + k]"""
        for d in range(k):
            if s[i + d] != t[j + d]:
                return False
        return True

    def rabin_karp_matching(self, s, t):
        """Find a substring by Rabin-Karp

        :param s: the haystack string
        :param t: the needle string

        :returns: index i such that s[i: i + len(t)] == t, or -1
        :complexity: O(len(s) + len(t)) in expected time,
                    and O(len(s) * len(t)) in worst case
        """
        hash_s = 0
        hash_t = 0
        len_s = len(s)
        len_t = len(t)
        last_pos = pow(self.DOMAIN, len_t - 1) % self.PRIME
        if len_s < len_t:
            return -1
        for i in range(len_t):  # preprocessing
            hash_s = (self.DOMAIN * hash_s + ord(s[i])) % self.PRIME
            hash_t = (self.DOMAIN * hash_t + ord(t[i])) % self.PRIME
        for i in range(len_s - len_t + 1):
            if hash_s == hash_t:  # check character by character
                if self.matches(s, t, i, 0, len_t):
                    return i
            if i < len_s - len_t:
                hash_s = self.roll_hash(hash_s, ord(s[i]), ord(s[i + len_t]), last_pos)
        return -1

    def rabin_karp_factor(self, s, t, k):
        """Find a common factor by Rabin-Karp

        :param string s: haystack
        :param string t: needle
        :param int k: factor length
        :returns: (i, j) such that s[i:i + k] == t[j:j + k] or None.
                  In case of tie, lexicographical minimum (i, j) is returned
        :complexity: O(len(s) + len(t)) in expected time,
                    and O(len(s) + len(t) * k) in worst case
        """
        last_pos = pow(self.DOMAIN, k - 1) % self.PRIME
        pos = {}
        assert k > 0
        if len(s) < k or len(t) < k:
            return None
        hash_t = 0
        for j in range(k):  # store hashing values
            hash_t = (self.DOMAIN * hash_t + ord(t[j])) % self.PRIME
        for j in range(len(t) - k + 1):
            if hash_t in pos:
                pos[hash_t].append(j)
            else:
                pos[hash_t] = [j]
            if j < len(t) - k:
                hash_t = self.roll_hash(hash_t, ord(t[j]), ord(t[j + k]), last_pos)
        hash_s = 0
        for i in range(k):  # preprocessing
            hash_s = (self.DOMAIN * hash_s + ord(s[i])) % self.PRIME
        for i in range(len(s) - k + 1):
            if hash_s in pos:  # is this signature in s?
                for j in pos[hash_s]:
                    if self.matches(s, t, i, j, k):
                        return (i, j)
            if i < len(s) - k:
                hash_s = self.roll_hash(hash_s, ord(s[i]), ord(s[i + k]), last_pos)
        return None


def longest_common_subsequence(x, y):
    """Longest common subsequence

    Dynamic programming

    :param x:
    :param y: x, y are lists or strings
    :returns: longest common subsequence in form of a string
    :complexity: `O(|x|*|y|)`
    """
    n = len(x)
    m = len(y)
    #                      -- compute optimal length
    A = [[0 for j in range(m + 1)] for i in range(n + 1)]
    for i in range(n):
        for j in range(m):
            if x[i] == y[j]:
                A[i + 1][j + 1] = A[i][j] + 1
            else:
                A[i + 1][j + 1] = max(A[i][j + 1], A[i + 1][j])
    #                      -- extract solution
    sol = []
    i, j = n, m
    while A[i][j] > 0:
        if A[i][j] == A[i - 1][j]:
            i -= 1
        elif A[i][j] == A[i][j - 1]:
            j -= 1
        else:
            i -= 1
            j -= 1
            sol.append(x[i])
    return ''.join(sol[::-1])  # inverse solution


def is_prime(n):
    """
    Assumes that n is a positive natural number
    """
    # We know 1 is not a prime number
    if n == 1:
        return False

    i = 2
    # This will loop from 2 to int(sqrt(x))
    while i * i <= n:
        # Check if i divides x without leaving a remainder
        if n % i == 0:
            # This means that n has a factor in between 2 and sqrt(n)
            # So it is not a prime number
            return False
        i += 1
    # If we did not find any factor in the above loop,
    # then n is a prime number
    return True


def prime_factors(n):
    factors = []
    if n % 2 == 0:
        factors.append(2)
    for i in range(3, n // 2):
        if i % 2 != 0 and n % i == 0 and is_prime(i):
            factors.append(i)
            print(len(factors))
    return factors

class PrimeCheck:
    def __init__(self):
        self.cache = {}
        self.max_checked = 2

    def is_prime(self,n):
        if n in self.cache:
            return True
        if n < self.max_checked:
            return False

        if n == 1:
            return False
        if n % 2 == 0:
            return False
        i = 2
        while i * i <= n:
            if n % i == 0:
                return False
            i += 1
        max_checked = max(n,self.max_checked)
        self.cache[n] = True
        return True
