class LLNode:
    def __init__(self, val, next=None):
        self.val = val
        self.next = next


# Definition for singly-linked list.
class ListNode(LLNode):
    def __init__(self, val=0, next=None):
        super().__init__(val, next)
        # self.val = val
        # self.next = next


def linkedlist_from_list(lst):
    if lst:
        head = ListNode(lst[0])
        tmp = head
        for i in range(1, len(lst)):
            tmp.next = ListNode(lst[i])
            tmp = tmp.next
        return head


def linkedlist_to_list(head):
    ans = []
    while head:
        ans.append(head.val)
        head = head.next
    return ans
