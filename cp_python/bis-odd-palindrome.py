class Solution:
    def __init__(self):
        self.s = ""
        self.cache = {}


def solve(self, s):
    n = len(s)
    for i in range(n):
        j = i - 1
        k = i + 1
        while j >= 0 and k < n and s[j] == s[k]:
            if (k - j + 1) % 2 == 0:
                return False
            j -= 1
            k += 1
        j = i
        k = i + 1
        while j >= 0 and k < n and s[j] == s[k]:
            if (k - j + 1) % 2 == 0:
                return False
            j -= 1
            k += 1
    return True


s = Solution()
print(s.solve("bab"))
print(s.solve("trart"))
print(s.solve("aa"))
