import unittest


class LogiciansAndBeer:
    def bringBeer(self, responses):
        if len(responses) == 1 and responses == '?':
            return -1

        if len(responses) == 1 and responses == '+':
            return 1

        if len(responses) == 1 and responses == '-':
            return 0

        ans = 0
        has_seen_minus = False
        for i in range(len(responses)):
            last_response = i == len(responses) - 1
            first_response = i == 0

            if has_seen_minus and responses[i] != '-':
                return -1

            if first_response and (responses[i] == '+' or responses[i] == '-'):
                return -1

            if last_response and responses[i] == '+':
                ans += 1
                continue

            if last_response and responses[i] == '?':
                return -1

            if responses[i] == '?':
                ans += 1

            if responses[i] == '-':
                has_seen_minus = True
        return ans


class TestDotaSenate(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = LogiciansAndBeer()

    def test1(self):
        self.assertEqual(3, self.sol.bringBeer("??+"))

    def test2(self):
        self.assertEqual(4, self.sol.bringBeer("????-"))

    def test3(self):
        self.assertEqual(-1, self.sol.bringBeer("-+-+-+"))

    def test4(self):
        self.assertEqual(1, self.sol.bringBeer("?-----"))

    def test5(self):
        self.assertEqual(-1, self.sol.bringBeer("?"))

    def test6(self):
        self.assertEqual(1, self.sol.bringBeer("+"))

    def test7(self):
        self.assertEqual(0, self.sol.bringBeer("-"))

    def test8(self):
        self.assertEqual(-1, self.sol.bringBeer("???"))

    def test9(self):
        self.assertEqual(-1, self.sol.bringBeer("-??"))
