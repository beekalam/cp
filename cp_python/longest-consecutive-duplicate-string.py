import math
from itertools import groupby

pow()
class Solution:
    def solve(self, s):
        return max([len(list(g)) for k, g in groupby(s)]);

    def solve2(self, s):
        n = len(s)
        if n <= 1:
            return n
        tmp = s[0]
        ans = -math.inf
        for i in range(1, n):
            if s[i] == tmp[-1]:
                tmp += s[i]
            else:
                ans = max(ans, len(tmp))
                tmp = s[i]

        ans = max(ans, len(tmp))
        return ans


s = Solution()
print(s.solve("aa"))
# for k, g in groupby("aaabb"):
#     print(k, list(g))
# print(max([len(list(g)) for k, g in groupby("aaabb")]))
