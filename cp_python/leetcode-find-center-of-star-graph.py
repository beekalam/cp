import collections
from typing import List

class Solution:
    def findCenter(self, edges: List[List[int]]) -> int:
        m = collections.defaultdict(int)
        n = len(edges)
        for a,b in edges:
            m[a] += 1
            m[b] += 1
            if m[a] == n:
                return a
            if m[b] == n:
                return b
        return 0

s = Solution()
print(s.findCenter( [[1,2],[2,3],[4,2]]))
