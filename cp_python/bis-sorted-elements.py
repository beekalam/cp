# 2021-11-19
class Solution:
    def solve(self, nums):
        return sum([i==j for i,j in zip(nums,sorted(nums))])
