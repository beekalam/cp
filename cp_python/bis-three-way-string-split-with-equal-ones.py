from collections import Counter
# from itertools import accumulate
import operator



class Solution:
    def solve(self, s):
        # prefix = list(accumulate(s, lambda x, y: x + int(y), initial=0))
        prefix = []
        for i in range(len(s)):
            if i == 0:
                prefix.append(int(s[i]))
            else:
                prefix.append(prefix[i-1] + int(s[i]))

        total = prefix[-1]
        N = len(prefix)
        MOD = 10 ** 9 + 7
        c = Counter(prefix)
        print(c)
        print("before if")
        if total % 3 != 0:  # if not divisible by 3
            return 0

        if total == 0:  # if all zeros, edge case
            return (((N - 3) * (N - 2)) // 2) % MOD

        ans =  (c[total - total // 3] * c[total - 2 * (total // 3)]) % MOD
        return ans


s = Solution()
ss = "11001111"
s.solve(ss)
# assert s.solve(ss) == 3, "test 1 failed"

# ss = "1111"
# assert s.solve(ss) == 0, "test 2 failed"

# ss = "0000"
# assert s.solve(ss) == 3, "test 3 failed"

