from functools import cmp_to_key
class Solution:
    def solve(self, intervals):
        # @snippet: custom sort
        def sort_intervals(a,b):
            if a[0] == b[0]:
                return a[1] - b[1]
            else:
                return a[0] - b[0]
        intervals.sort(key=cmp_to_key(sort_intervals),reverse=True)

        n = len(intervals)
        ans = 0
        # print(intervals)
        for idx,(i,j) in enumerate(intervals):
            k = idx + 1
            while k < n:
                p,q = intervals[k]
                if (i <= p and j >= q) or (p <= i and q >= j):
                    ans += 1
                    k += 1
                else:
                    break

        # print("ans:{}".format(ans))
        return True if ans > 0 else False


s = Solution()
intervals = [
    [1, 3],
    [4, 10],
    [9, 9],
    [4, 8],
]
assert s.solve(intervals) == True, "test1"

intervals = [[0,2], [0,0]]
assert s.solve(intervals) == True, "test2"


intervals = [[1,3],[0,0]]
assert s.solve(intervals) == False, "test3"

intervals = [[0,4], [1,4]]
assert s.solve(intervals) == True, "test4"
