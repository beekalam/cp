class Solution(object):
    def kthDistinct(self, arr, k):
        """
        :type arr: List[str]
        :type k: int
        :rtype: str
        """
        m = {}
        for i in arr:
            if i in m:
                m[i] += 1
            else:
                m[i] = 1
        kth = 0
        for key,v in m.items():
            if v == 1:
                kth += 1
            if kth == k:
                return key
        return ""
s = Solution()
assert  s.kthDistinct(arr = ["a","b","a"], k = 3) == ""
assert  s.kthDistinct(arr = ["d","b","c","b","c","a"], k = 2) == "a"
assert  s.kthDistinct(arr = ["aaa","aa","a"], k = 1) == "aaa"
