class Solution:

    def do_solve(self,s):
        if  s == s[::-1]:
            return 1
        return 0

    def solve(self,s):
        n = len(s)
        ans = 0
        for i in range(n):
            for j in range(n,i,-1):
                ans += self.do_solve(s[i:j])
        return ans


s = Solution()
ss = "bobo"
myassert(ss, s.solve(ss) == 6)

ss = "aa"
myassert(ss, s.solve(ss) == 3)
ss = "cba"

myassert(ss, s.solve(ss) == 3)



def myassert(inp,assertion, message="wrong answer for"):
    if not assertion:
        print(message + " "+ str(inp))
