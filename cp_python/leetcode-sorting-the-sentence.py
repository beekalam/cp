class Solution(object):
    def sortSentence(self, s):
        """
        :type s: str
        :rtype: str
        """
        m={}
        for word in s.split():
            m[int(word[-1])] = word[0:-1]
        return " ".join(m.values())
