# 2021-10-28
import math
import heapq
class Solution:
    def solve(self, A):
        for i in range(len(A)):
            A[i] *= -1
        heapq.heapify(A)
        while len(A) > 1:
            # a,b = heapq.nsmallest(2,A)
            a = -1 * heapq.heappop(A)
            b = -1 * heapq.heappop(A)
            if a != b:
                toadd = -1 * math.floor((a+b)/3)
                heapq.heappush(A,toadd)
            # print(A)
        return A[0] * -1 if len(A) else -1

s = Solution()
# cells = [10,30, 30, 20]
# print(s.solve(cells))

cells = [3,3,3,4]
print(s.solve(cells))
