# https://leetcode.com/contest/weekly-contest-266/problems/vowels-of-all-substrings/
#@readme: not solved
import random
import string
import time

class Solution:
    def countVowels(self, word: str) -> int:
        n = len(word)
        lst = [0] * n
        vowels = set("aeiou")
        lst[0] = 1 if word[0] in vowels else 0
        ans = lst[0]
        for i in range(1, n):
            lst[i] = lst[i - 1]
            if word[i] in vowels:
                lst[i] += 1
            ans += lst[i]
            j = i - 1
            while j >= 0:
                ans += lst[i] - lst[j]
                j -= 1
        return ans


s = Solution()
assert s.countVowels("aba") == 6, "test1"
assert s.countVowels("abc") == 3, "test2"
assert s.countVowels("ltcd") == 0, "test3"
assert s.countVowels("noosabasboosa") == 237, "test4"
assert s.countVowels("a") == 1, "test5"


def id_generator(size=1000):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(size))


mystr = id_generator(100)
start = time.time()
print(s.countVowels(mystr))
end = time.time()
print("duration: ", end - start)
