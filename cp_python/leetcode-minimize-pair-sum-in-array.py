class Solution(object):
    def minPairSum(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        nums.sort()
        m = float('-inf')
        while nums:
            m = max(m, nums.pop() + nums.pop(0) )
        return m

