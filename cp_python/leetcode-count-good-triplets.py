class Solution(object):
    def countGoodTriplets(self, arr, a, b, c):
        """
        :type arr: List[int]
        :type a: int
        :type b: int
        :type c: int
        :rtype: int
        """
        n = len(arr)
        ans = 0
        for i in range(n):
            for j in range(i+1,n):
                for k in range(j+1,n):
                    cond = a >= abs(arr[i] - arr[j])
                    cond = cond and b >= abs(arr[j] - arr[k]) 
                    cond = cond and c >= abs(arr[i] - arr[k])
                    if cond:
                        ans += 1

        # print("ans:{}".format(ans))
        return ans

def test_1():
    s = Solution()
    arr = [3,0,1,1,9,7]
    a = 7
    b = 2
    c = 3
    assert s.countGoodTriplets(arr, a, b, c), 4

def test_2():
    s = Solution()
    arr = [1,1,2,2,3]
    a = 0
    b = 0
    c = 1
    assert s.countGoodTriplets(arr, a, b, c) == 0
