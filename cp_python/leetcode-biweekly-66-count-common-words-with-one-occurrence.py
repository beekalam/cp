# 2021-11-27
import collections
from typing import List


class Solution:
    def countWords(self, words1: List[str], words2: List[str]) -> int:
        c1 = collections.Counter(words1)
        c2 = collections.Counter(words2)
        ans = 0
        for c in c1:
            if c1[c] == 1 and c in c2 and c2[c] == 1:
                ans += 1
        return ans


words1 = ["leetcode", "is", "amazing", "as", "is"]
words2 = ["amazing", "leetcode", "is"]
s = Solution()

assert s.countWords(words1, words2) == 2, "test1"

words1 = ["a", "ab"]
words2 = ["a", "a", "a", "ab"]
assert s.countWords(words1, words2) == 1, "test2"

words1 = ["b", "bb", "bbb"]
words2 = ["a", "aa", "aaa"]
assert s.countWords(words1, words2) == 0, "test3"
