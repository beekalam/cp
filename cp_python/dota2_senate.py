import collections
import unittest


class Solution(object):
    def predictPartyVictory(self, senate):
        """
        :type senate: str
        :rtype: str
        """
        # if len(senate) == 1:
        #     return "Radiant" if senate[0] == 'R' else "Dire"

        if all([senate[0] == i for i in senate]):
            return "Radiant" if senate[0] == 'R' else "Dire"

        senate = list(senate)
        l = len(senate)
        senate_vote = []
        for i in range(0, l - 1):
            if senate[i] == 'X':
                continue
            if senate[i] != senate[i + 1]:
                senate[i + 1] = 'X'
        if senate[0] != 'X' and senate[l - 1] != 'X':
            if senate[l - 1] != senate[0]:
                senate[0] = 'X'
        return self.predictPartyVictory([i for i in senate if i != 'X'])


class Solution_(object):
    def predictPartyVictory(self, senate):
        """
        :type senate: str
        :rtype: str
        """
        queue = collections.deque()
        people, bans = [0, 0], [0, 0]

        for person in senate:
            x = person == 'R'
            people[x] += 1
            queue.append(x)

        while all(people):
            x = queue.popleft()
            if bans[x]:
                bans[x] -= 1
                people[x] -= 1
            else:
                bans[x ^ 1] += 1
                queue.append(x)

        return "Radiant" if people[1] else "Dire"


class TestDotaSenate(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = Solution()

    def test_1(self):
        self.assertEqual("Radiant", self.sol.predictPartyVictory("RD"))

    def test_2(self):
        self.assertEqual("Dire", self.sol.predictPartyVictory("RDD"))

    def test_3(self):
        self.assertEqual("Radiant", self.sol.predictPartyVictory("R"))

    def test_4(self):
        self.assertEqual("Dire", self.sol.predictPartyVictory("D"))

    def test_5(self):
        self.assertEqual("Radiant", self.sol.predictPartyVictory("RDR"))

    def test_6(self):
        self.assertEqual("Radiant", self.sol.predictPartyVictory("RRR"))

    def test_7(self):
        self.assertEqual("Dire", self.sol.predictPartyVictory("DDD"))

    def test_8(self):
        self.assertEqual("Dire", self.sol.predictPartyVictory("DDRRR"))
