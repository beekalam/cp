from collections import Counter
from itertools import groupby


class Solution(object):
    def winnerOfGame(self, colors):
        """
        :type colors: str
        :rtype: bool
        """

        items = []
        for k, grp in groupby(colors):
            items.append([k, len(list(grp))])
        turn = 'a'
        winner = ''
        ai = 0
        bi = 0
        while True and len(items) > 0:
            if turn == 'a':
                found = False
                while ai < len(items):
                    k, v = items[ai]

                    if k == 'A':
                        if v >= 3:
                            items[ai] = ['A', v - 1]
                            ai += 1
                            found = True
                            break
                        elif ai + 1 < len(items) and items[ai + 1][0] == 'A':
                            items[ai + 1] = ['A', v + items[ai + 1][1]]
                            del items[ai]
                        else:
                            ai += 1
                    else:
                        ai += 1
                if not found:
                    return False
                turn = 'b'
            else:
                found = False
                while bi < len(items):
                    k, v = items[bi]
                    if k == 'B':
                        if v >= 3:
                            items[bi] = ('B', v - 1)
                            bi += 1
                            found = True
                            break
                        elif bi + 1 < len(items) and items[bi + 1][0] == 'B':
                            items[bi + 1] = ('B', v + items[bi + 1][1])
                            del items[bi]
                        else:
                            bi += 1
                    else:
                        bi += 1
                if not found:
                    return True
                turn = 'a'


s = Solution()
# colors = "AAABABB"
# assert s.winnerOfGame(colors) == True, "test1"
# colors = "AA"
# assert s.winnerOfGame(colors) == False, "test2"
# colors = "ABBBBBBBAAA"
# assert s.winnerOfGame(colors) == False, "test3"
colors = "AAAAABBB"
print(s.winnerOfGame(colors))
