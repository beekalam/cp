# https://leetcode.com/problems/find-and-replace-pattern/submissions/
from itertools import groupby
from collections import Counter


class Solution(object):
    def signature(self,string):
        return [len(list(grp)) for _,grp in groupby(string)]

    def findAndReplacePattern(self, words, pattern):
        """
        :type words: List[str]
        :type pattern: str
        :rtype: List[str]
        """
        m=dict()
        n = dict()
        ans = []
        for w in words:
            for a,b in zip(w,pattern):
                if a in m and m[a] != b:
                    w= ""
                    break
                else:
                    m[a] = b
                if b in n and n[b] != a:
                    w = ""
                    break
                else:
                    n[b] = a
            if w:
                ans.append(w)
            m.clear()
            n.clear()
        return ans

s = Solution()
words = ["abc","deq","mee","aqq","dkd","ccc"]
pattern = "abb"
print(s.findAndReplacePattern(words,pattern))
words = ["a","b","c"]
pattern = "a"
print(s.findAndReplacePattern(words,pattern))


