# 2021-12-10
import collections
import heapq as hq


class Solution:
    def solve(self, shows, durations, k):
        dic = collections.defaultdict(int)
        for show, duration in zip(shows, durations):
            dic[show] += duration
        items = [(key, value) for key, value in dic.items()]
        items.sort(key=lambda x: x[1], reverse=True)
        ans = 0
        for i in range(k):
            ans += items[i][1]
        return ans


s = Solution()
shows = ["Top Gun", "Aviator", "Top Gun", "Roma", "Logan"]
durations = [5, 3, 5, 13, 4]
k = 2
assert s.solve(shows, durations, k) == 23, "test1"

shows = ["Roma", "Roma", "Logan"]
durations = [4, 1, 7]
k = 1
assert s.solve(shows, durations, k) == 7, "test2"
