# https://binarysearch.com/problems/Add-Linked-Lists
# 2021-11-10
# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, a,b):
        carry = 0
        head = ret = LLNode(0)
        while a or b:
            if a and b:
                s = a.val + b.val + carry
                a = a.next
                b = b.next
            elif a:
                s = a.val + carry
                a = a.next
            elif b:
                s = b.val + carry
                b = b.next
            carry = 0
            if s >= 10:
                s,carry = s - 10,1
            ret.next = LLNode(s)
            ret = ret.next

        if carry:
            ret.next = LLNode(1)
        return head.next
