# 2021-11-19
import collections
class Solution:
    def beautySum(self, s: str) -> int:
        n = len(s)
        ans = 0
        for i in range(n):
            for j in range(i+1,n):
                freq = list(collections.Counter(s[i:j+1]).values())
                freq.sort()
                if len(freq) >= 2:
                    ans += abs(freq[0] - freq[-1])
        return ans
