class Solution:
    def solve(self, nums):
        m = dict(((i, True) for i in nums))
        for k in sorted(m.keys(), reverse=True):
            if k >= 0 and -k in m:
                return k
        return -1


s = Solution()

nums = [-4, 1, 8, -5, 4, -8]
assert s.solve(nums) == 8, "test 1"

nums = [5, 6, 1, -2]
assert s.solve(nums) == -1, "test 2"

nums = []
assert s.solve(nums) == -1, "test 3"

nums = [5, 6, 1, 2]
assert s.solve(nums) == -1, "test 4"

nums = [5, 6, 1, 2, -1]
assert s.solve(nums) == 1, "test 5"

nums = [0]
assert s.solve(nums) == 0, "test 6"
