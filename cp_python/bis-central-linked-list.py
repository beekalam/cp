import math
# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, node):
        tmp = node
        size = 0
        while tmp:
            size += 1
            tmp = tmp.next

        if size == 1:
            return node.val

        t = size//2
        tmp = node
        i = 0
        while i < t:
            i += 1
            tmp = tmp.next
        return tmp.val
