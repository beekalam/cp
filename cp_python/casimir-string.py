def solve(s):
    return s.count('B') == s.count('A') + s.count('C')


n = int(input())
for i in range(n):
    s = input()
    if solve(s):
        print("YES")
    else:
        print("NO")
