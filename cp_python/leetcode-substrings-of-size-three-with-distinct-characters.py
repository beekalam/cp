# 2021-11-09
class Solution:
    def countGoodSubstrings(self, s: str) -> int:
        n = len(s)
        ans = 0
        for i in range(n):
            if i + 2 <= n and len(set(s[i:i+3])) == 3:
                ans += 1
        return ans
