class Solution(object):
    def countNegatives(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        m = len(grid)
        n = len(grid[0])
        ans = 0
        for j in range(n-1,-1,-1):
            i = m-1
            while i>= 0 and grid[i][j] < 0:
                ans += 1
                i -= 1
        return ans

def test_1():
    s = Solution()
    assert s.countNegatives(grid = [[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]]) == 8


def test_2():
    s = Solution()
    assert s.countNegatives(grid = [[3,2],[1,0]]) == 0

def test_3():
    s = Solution()
    assert s.countNegatives( [[1,-1],[-1,-1]]) == 3

def test_4():
    s = Solution()
    assert s.countNegatives( [[-1]]) == 1
