import math

n = int(input())

while n >= 10:
    n = sum([int(i) for i in list(str(n))])

print(n)
