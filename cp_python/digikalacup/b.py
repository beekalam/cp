import unittest


def solve(n, s, f, u, d):
    if f > s and u == 0:
        return "Impossible"
    if s > f and d == 0:
        return "Impossible"
    if f > s and d == 0 and u + s > f:
        return "Impossible"
    if s > f and u == 0 and s - d < f:
        return "Impossible"
    # if f > s and d == 0 and (f - s) % u != 0:
    #     return "Impossible"
    # if s > f and u == 0 and (s - f) % d != 0:
    #     return "Impossible"
    if f > s and (f - s) % u == 0:
        return (f - s) / u
    if s > f and (s - f) % d == 0:
        return (s - f) / d
    if f > s:
        steps = (f - s) // u
        steps += ((f - s) % u) / d


class TestB(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()

    def test_1(self):
        self.assertEqual("Impossible", solve(100, 2, 1, 1, 0))

    def test_2(self):
        self.assertEqual("Impossible", solve(100, 2, 3, 0, 10))

    def test_3(self):
        self.assertEqual("Impossible", solve(4, 3, 4, 5, 0))

    def test_4(self):
        self.assertEqual("Impossible", solve(4, 3, 2, 0, 2))

    def test_5(self):
        self.assertEqual("Impossible", solve(4, 2, 4, 3, 0))

    def test_6(self):
        self.assertEqual("Impossible", solve(4, 2, 1, 0, 3))

    def test_7(self):
        self.assertEqual(1, solve(4, 2, 4, 2, 3))

    def test_8(self):
        self.assertEqual(1, solve(10, 1, 10, 2, 1))
