# 2022-01-01
class Solution:
    def solve(self, n):
        arr = []
        for i in range(1,n+1):
            if i % 3 == 0 or any(j in str(i) for j in ["3","6","9"]):
                arr.append("clap")
            else:
                arr.append(str(i))
        return arr
