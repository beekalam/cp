class Solution:
    def solve(self, matrix):
        m, n = len(matrix), len(matrix[0])
        matrix_ = [
            [matrix[i][j] for i in range(m)]
            for j in range(n)
        ]
        max_rows = [max(row) for row in matrix]
        max_cols = [max(col) for col in matrix_]

        ans = 0
        for i in range(m):
            for j in range(n):
                number = matrix[i][j]
                if (number >= max_rows[i]) and (number >= max_cols[j]):
                    ans = ans + 1
        return ans


class Solution2:
    def solve(self, matrix):
        rowmaxes = list(map(max, matrix))
        print(list(zip(*matrix)))
        # print(list(map(max, zip(*matrix))))
        # colmaxes = list(map(max, zip(*matrix)))
        #
        # ans = 0
        # for r, row in enumerate(matrix):
        #     for c, v in enumerate(row):
        #         if rowmaxes[r] == colmaxes[c] == v:
        #             ans += 1
        # return ans


matrix = [[1, 3, 2], [6, 6, 5], [1, 5, 7], [5, 6, 7]]
s = Solution2()
s.solve(matrix)
