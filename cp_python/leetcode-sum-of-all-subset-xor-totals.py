class Solution(object):
    def subsetXORSum(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        ans = 0
        n = len(nums)
        total = 0
        for i in range(n):
            x = nums[i]
            ans += x
            for j in range(i + 1, n):
                x = x ^ nums[j]
                ans += x

        return ans


s = Solution()
# print(s.subsetXORSum([1,3]))
print(s.subsetXORSum([5, 1, 6]))  # 28
