import math
# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution(object):
    def isPalindrome1(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        tmp = head
        tail = ListNode(tmp.val)
        tmp = tmp.next
        while tmp:
            node = ListNode(tmp.val,tail)
            tail = node
            tmp = tmp.next
        while head and tail:
            if tail.val != head.val:
                return False
            tail =tail.next
            head =head.next
        return True
