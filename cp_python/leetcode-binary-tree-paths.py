# 2021-11-12
import collections
from typing import List
from typing import Optional
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def binaryTreePaths(self, root: Optional[TreeNode]) -> List[str]:
        parents = dict()
        q = collections.deque()
        q.append(root)
        paths = []
        while q:
            r = q.popleft()
            if r.left:
                q.append(r.left)
                parents[r.left] = r
            if r.right:
                q.append(r.right)
                parents[r.right] = r
            if r.left is None and r.right is None:
                p = r
                path = []
                while p:
                    path.append(str(p.val))
                    p = parents.get(p,None)
                paths.append("->".join(reversed(path)))
        return paths
