import itertools


class Solution:
    def solve(self, typed, target):
        # if len(target) > len(typed):
        #     return False
        typed = [(k, list(g)) for k, g in itertools.groupby(typed)]
        target = [(k, list(g)) for k, g in itertools.groupby(target)]
        if len(typed) != len(target):
            return False
        for i in range(len(typed)):
            typed_c, typed_count = typed[i]
            target_c, target_count = target[i]
            if typed_c != target_c or typed_count < target_count:
                return False
        return True





s = Solution()
print(s.solve(typed="aaabcccc", target="abc"))
# print(s.solve(typed="acc", target="aac"))
