# @notsolved: 2021-11-13
n, k, a = list(map(int, input().split()))

for i in range(0, k):
    if a == n + 1:
        a = 1
    else:
        a += 1
if a == n + 1:
    a = n
print(a)
