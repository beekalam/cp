import string
from functools import lru_cache


class Solution:
    def minTimeToType(self, word: str) -> int:
        sl = string.ascii_lowercase

        @lru_cache(maxsize=None)
        def distance(a, b):
            forward = abs(sl.index(pos) - sl.index(c)) + 1
            ia = sl.index(a)
            backward1, backward2 = 0, 0
            while sl[ia] != b:
                if ia == -1:
                    ia = len(sl) - 1
                else:
                    ia -= 1
                    backward1 += 1
            ib = sl.index(b)
            while sl[ib] != a:
                if ib == -1:
                    ib = len(sl) - 1
                else:
                    ib -= 1
                    backward2 += 1
            # print(f"forward: {forward}, backward1: {backward1}, backward2: {backward2}")
            return min(forward, backward1, backward2)

        pos, ans = 'a', 0
        for c in word:
            if pos != c:
                ans += distance(pos, c)
            ans += 1
            pos = c
        # print(f"ans: {ans}")
        return ans


s = Solution()
assert s.minTimeToType('bza') == 7, "test1"
assert s.minTimeToType('pdy') == 31, "test2"
