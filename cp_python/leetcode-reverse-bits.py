class Solution:
    # @param n, an integer
    # @return an integer
    def reverseBits(self, n):
        j = 31
        for i in range(16):
            # extract i-th and j-th bits
            if (n >> i) & 1 != (n >> j) & 1:
                bit_mask = (1 << i) | (1 << j)
                n ^= bit_mask
            j -= 1
        return n


s = Solution()
assert s.reverseBits(43261596) == 964176192
print(s.reverseBits(43261596))
