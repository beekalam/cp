class Solution:
    def solve(self, s0, s1):
        if len(s0) <= len(s1) or s0 == s1:
            return False
        if len(s0) == 1 and len(s1) == 0:
            return True

        # for i in range(len(s0)):
        #     if s0[0:i] + s0[i+1:] == s1:
        #         return True
        i = j = diff = 0
        while i < len(s0):
            if j < len(s1) and s0[i] == s1[j]:
                i += 1
                j += 1
            else:
                i += 1
                diff += 1
        # print("diff: {}".format(diff))
        return diff <= 1

    def solve2(self,s0,s1):
        if len(s0) != len(s1) + 1:
            return False
        # s0 = iter(s0)
        # s1 = iter(s1)
        # return all(x in s0 for x in s1)
        return all(x in s0 for x in s1)



s = Solution()
assert s.solve2("aaaa", "aaa") == True, "wrong"
assert s.solve2("j", "k") == False, "wrong"
