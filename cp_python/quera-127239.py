def distance(x, y, z):
    return (abs(x - 0) + abs(y - 0) + abs(z - 0)) / 2


steps = "ABC"
x, y, z = 0, 0, 0
for direction in steps:
    if direction == "n":
        y += 1
        z -= 1
    elif direction == "ne":
        x += 1
        z -= 1
    elif direction == "se":
        x += 1
        y -= 1
    elif direction == "s":
        y -= 1
        z += 1
    elif direction == "sw":
        x -= 1
        z += 1
    elif direction == "nw":
        y += 1
        z -= 1
