# 2022-01-09
import collections
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
def printTree(q):
    for i in range(len(q)):
        print(q[i].val, end=' ')
class Solution:
    def levelOrderBottom(self, root: Optional[TreeNode]) -> List[List[int]]:
        res = []
        q = collections.deque()
        q.append(root)
        while q:
            tmp = []
            level = []
            # printTree(q)
            # print()
            while q:
                r = q.popleft()
                if r:
                    level.append(r.val)
                    if r.left:
                        tmp.append(r.left)
                    if r.right:
                        tmp.append(r.right)

            if level:
                res.insert(0,level)
            if tmp:
                for node in tmp:
                    q.append(node)
        return res
