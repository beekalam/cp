import sys
import collections
from functools import cmp_to_key

N, M = map(int, input().split())
S = [input() for i in range(2 * N)]
rank = [[0, i] for i in range(2 * N)]


# rank[i] = [x,y]  -> the person who ranks $i$-th has won -x times and has ID y

def judge(a, b):
    # -1 if draw, 0 if the former wins, or 1 if the latter wins
    if a == b: return -1
    if a == 'G' and b == 'P': return 1
    if a == 'C' and b == 'G': return 1
    if a == 'P' and b == 'C': return 1
    return 0


for j in range(M):
    for i in range(N):
        player1 = rank[2 * i][1]
        player2 = rank[2 * i + 1][1]
        result = judge(S[player1][j], S[player2][j])
        if result != -1:
            rank[2 * i + result][0] -= 1
    rank.sort()

for _, i in rank: print(i + 1)

# def cmp_tupples(a, b):
#     if a[1] == b[1]:
#         return b[0] - a[0]
#     else:
#         return a[1] - b[1]


# def new_ranks(mp):
#     ret = sorted(list(mp.items()), key=cmp_to_key(cmp_tupples), reverse=True)
#     print(ret)
#     ans = []
#     for (idx, value) in ret:
#         ans.append(idx - 1)
#     print("new_ransk: ", ans)
#     return ans


# # a = [(1, 0), (2, 1), (3, 1), (4, 0)]
# # ret = print(sorted(a, key=cmp_to_key(cmp_tupples), reverse=True))
# # print(ret)
# # sys.exit(0)


# def col(j, rounds):
#     ans = []
#     for i in range(len(rounds)):
#         ans.append(rounds[i][j])
#     return ans


# def who_wins(pa_id, pa_hand, pb_id, pb_hand):
#     wins = [('G', 'C'), ('C', 'P'), ('P', 'G')]
#     if pa_hand == pb_hand:
#         return 0
#     if (pa_hand, pb_hand) in wins:
#         return pa_id
#     if (pb_hand, pa_hand) in wins:
#         return pb_id


# n, m = map(int, input().split())
# rounds = []
# for i in range(2 * n):
#     rounds.append(list(input()))
# print(rounds)
# players = list(range(2 * n))
# mp = {}
# for i in players:
#     mp[i + 1] = 0
# print(mp)
# for j in range(m):
#     round = col(j, rounds)
#     # print("round:")
#     # print(round)
#     # print("ranks:")
#     # print(players)
#     for i in range(0, 2 * n, 2):
#         winner = who_wins(players[i] + 1, round[i],
#                           players[i + 1] + 1, round[i + 1])
#         print("winner: {}".format(winner))
#         if winner:
#             mp[winner] = mp[winner] + 1
#         # print(mp)
#     players = new_ranks(mp)
