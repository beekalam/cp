class Solution(object):
    def replaceElements(self, arr):
        """
        :type arr: List[int]
        :rtype: List[int]
        """
        n = len(arr)
        m = arr[-1]
        for i in range(n-1,-1,-1):
            tmp = max(m, arr[i])
            arr[i] = m
            m = tmp
        arr[-1] = -1
        return arr

s = Solution()
print(s.replaceElements([17,18,5,4,6,1]))
