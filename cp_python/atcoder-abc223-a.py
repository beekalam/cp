i = int(input().strip())
if i < 100:
    print("No")
elif i % 100 != 0:
    print("No")
else:
    print("Yes")
