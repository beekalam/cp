class my_CustomSet:
    def __init__(self):
        self.items = {}

    def add(self, val):
        if not self.exists(val):
            self.items[val] = 1

    def exists(self, val):
        return val in self.items

    def remove(self, val):
        if self.exists(val):
            del self.items[val]
class ListNode:
    def __init__(self, val):
        self.val = val
        self.next = None

class CustomSet:
    def __init__(self):
        self.mod = 2069  # any large prime number
        self.lst = [None] * self.mod

    def add(self, val):
        hashkey = val % self.mod
        if not self.lst[hashkey]:
            self.lst[hashkey] = ListNode(val)
        else:
            curr = self.lst[hashkey]
            while curr.next:
                if curr.val == val:
                    return
                curr = curr.next
            if curr.val == val:
                return
            curr.next = ListNode(val)

    def exists(self, val):
        hashkey = val % self.mod
        if self.lst[hashkey]:
            curr = self.lst[hashkey]
            while curr:
                if curr.val == val:
                    return True
                curr = curr.next
        return False

    def remove(self, val):
        hashkey = val % self.mod
        if self.lst[hashkey]:
            curr = self.lst[hashkey]
            if curr.val == val:
                self.lst[hashkey] = curr.next
                return
            while curr.next:
                if curr.next.val == val:
                    curr.next = curr.next.next
                    return
                curr = curr.next
s = CustomSet()
print(s.exists(1))
print(s.add(1))
print(s.exists(1))
print(s.remove(1))
print(s.exists(1))
