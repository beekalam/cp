# 2021-11-26
# 2021-12-28
import collections
#https://github.com/samgh/6-Weeks-to-Interview-Ready/blob/master/module_1/python/length_of_longest_substring.py
"""
Solution #2: Using a sliding window tracking previous indices
This is similar to the previous solution except that we track the index
of the previous occurence of each character. This allows us to quickly
update i to the right value rather than having to increment
Time Complexity: O(n)
Space Complexity: O(1)
"""
def length_of_longest_substring_improved(s: str)->int:
    # Index of the previous occurence of character
    index = {}

    max_length = 0;

    # i is the start of our substring and j is the end
    # We're using a sliding window here. Expand j out as far as possible
    # until there are duplicate characters, then move i to 1 plus the
    # previous occurrence of that character
    i = 0
    for j in range(len(s)):
        # If the current character previously occurred after i's position
        # update i
        if s[j] in index:
            i = max(index[s[j]], i)

        max_length = max(max_length, j-i+1)
        index[s[j]] = j+1

    return max_length

import collections


class Solution:
    def sol1(self, s:str) -> int:
        q = collections.deque()
        mx = 0
        for c in s:
            mx = max(mx, len(q))
            while c in q:
                q.popleft()
            q.append(c)
        mx = max(mx, len(q))

        return mx

    def lengthOfLongestSubstring(self, s: str) -> int:
        counter = collections.Counter()
        ws, we, mx = 0,0,0
        n = len(s)
        while we < n:
            c = s[we]
            counter.update(c)
            while ws < n and counter[c] >= 2:
                counter.subtract(s[ws])
                if counter[s[ws]] == 0:
                    del counter[s[ws]]
                ws += 1
            mx = max(mx, we - ws + 1)
            we += 1
        return mx

s = Solution()
assert s.lengthOfLongestSubstring("bbbb") == 1, "test1"
assert s.lengthOfLongestSubstring(" ") == 1, "test2"
assert s.lengthOfLongestSubstring("dvdf") == 3, "test3"
