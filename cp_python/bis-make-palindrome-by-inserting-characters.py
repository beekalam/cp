from cp_utils import edit_distance, longest_common_subsequence


class Solution:
    def solve(self, s):
        lcs = longest_common_subsequence(s, "".join(reversed(s)))
        return len(s) - len(lcs)


s = Solution()
assert s.solve("radr") == 1, "test1"
assert s.solve("rasdr") == 2, "test2"
assert s.solve("rdr") == 0, "test3"
assert s.solve("r") == 0, "test4"
assert s.solve("") == 0, "test5"
assert s.solve("ab") == 1, "test6"
assert s.solve("abc") == 2, "test7"
assert s.solve("crv") == 2, "test8"
assert s.solve("jqzjpu") == 3, "test9"
