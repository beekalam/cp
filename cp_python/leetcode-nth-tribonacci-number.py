class Solution(object):
    def tribonacci(self, n):
        """
        :type n: int
        :rtype: int
        """
        t = [0,1,1]
        if  len(t) > n:
            return t[n]
        a,b,c = t
        idx =2
        while idx != n:
            a,b,c=b,c,a+b+c
            idx += 1
        return c
