# 2021-11-21
class Solution:
    def solve(self, s):
        n, step = len(s), 1
        while step < n:
            first = int(s[0:step])
            # print("first: ", first)
            j = len(str(first))
            next = str(first - 1)
            found = True
            while j < n:
                # print(f"s[{j}:]: ", s[j:])
                if s[j:].startswith(next):
                    j += len(next)
                    next = str(int(next) - 1)
                    # print("next: ", next)
                else:
                    found = False
                    break
            if found:
                # print("ans ", True)
                return True
            step += 1
        return False


s = Solution()
assert s.solve("100999897") == True, "test1"
assert s.solve("51") == False, "test2"
