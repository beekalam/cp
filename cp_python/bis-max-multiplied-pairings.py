class Solution:
    def solve(self, nums, multipliers):
        nums.sort()
        multipliers.sort(reverse=True)
        # print(nums)
        # print(multipliers)
        i, q = 0, 0
        p, j = len(nums) - 1, len(multipliers) - 1
        ans = 0
        while q <= j and i <= p:
            a = nums[p] * multipliers[q]
            b = nums[i] * multipliers[j]
            # print("a:{}".format(a))
            # print("b:{}".format(b))
            if a > b:
                ans += a
                p -= 1
                q += 1
            else:
                ans += b
                i += 1
                j -= 1
        return ans


s = Solution()
nums = [-5, 3, 2]
multipliers = [-3, 1]
print(s.solve(nums, multipliers))
