from collections import deque
import itertools
d = deque('abcdefg')
print(d[0])
print(d[-1])
print(d.pop())
print(len(d))
print(d)
print(d.rotate(-1))
print(d)
nums = list(range(4))
for i in itertools.permutations(nums):
    print(i)
