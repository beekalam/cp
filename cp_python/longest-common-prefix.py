from functools import cmp_to_key


def cmp_length_str(a, b):
    return len(a) - len(b)


class Solution:
    def solve(self, words):
        prefix = ""
        j = 0
        smallest_word = sorted(words, key=cmp_to_key(cmp_length_str))[0]
        for i in range(0, len(smallest_word)):
            c = smallest_word[i]
            if all(word[i] == c for word in words):
                prefix += c
        return prefix




s = Solution()
# print(s.solve(["anthony", "ant", "antigravity"]))
# print(s.solve(["anthony", "znt", "antigravity"]))
print(s.solve(["epxwienuiyxxslsczaxesk", "epxwienuiyxxslseajcqpi"]))
