x, y = map(int, input().split(' '))

ans = 'No'
if x < y and x + 3 > y:
    ans = 'Yes'
if y < x and y + 3 > x:
    ans = 'Yes'

print(ans)
