# https://binarysearch.com/problems/Linked-List-Deletion
# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, node, target):
        head = LLNode(0)
        tmp = head
        while node:
            while node and node.val == target:
                node = node.next
            if node is not None:
                tmp.next = node
                tmp = tmp.next

            if node is not None:
                node = node.next
            tmp.next = None
        return head.next
