# 2021-11-26
class Solution:
    def solve(self, n):
        while n >= 10:
            n = sum([int(i) for i in str(n)])
        return n
s = Solution()
assert s.solve(8835) == 6, "test1"
