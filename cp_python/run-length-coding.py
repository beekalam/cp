from itertools import groupby


class Solution:
    def solve(self, s):
        if len(s) == 0:
            return ""
        # if len(s) == 1:
        #     return "1{}".format(s[0])

        i = count = 1
        prev_letter = s[0]
        ans = ""
        while i < len(s):
            if s[i] == prev_letter:
                count += 1
            else:
                ans += "{}{}".format(count, prev_letter)
                prev_letter = s[i]
                count = 1
            i += 1

        ans += "{}{}".format(count, prev_letter)
        return ans

    def solve2(self, s):
        ans = []
        for k, grp in groupby(s):
            print(k)
            print(list(grp))
            length = len(list(grp))
            ans.append(str(length))
            ans.append(k)
        return "".join(ans)


s = Solution()
# assert s.solve2("a") == "1a", "wrong"
# assert s.solve2("aa") == "2a", "wrong"
# assert s.solve2("abcde") == "1a1b1c1d1e", "wrong"
assert s.solve2("aaabb") == "3a2b", "wrong"
