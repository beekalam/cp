from functools import lru_cache
import string

@lru_cache(maxsize=None)
def shift(c,x):
    if x == 0:
        return c
    idx = string.ascii_lowercase.find(c)
    return string.ascii_lowercase[idx+x]

class Solution(object):
    def replaceDigits(self, s):
        """
        :type s: str
        :rtype: str
        """
        t = ""
        if len(s) % 2 == 1:
            t = s[-1]
        return "".join([ s[i-1] + shift(s[i-1], int(s[i])) for i in range(1,len(s),2) ]) + t
