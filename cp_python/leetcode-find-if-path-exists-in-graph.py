import collections
from typing import List


class Solution:
    def validPath(self, n: int, edges: List[List[int]], start: int, end: int) -> bool:
        m = collections.defaultdict(list)
        for i, j in edges:
            m[i].append(j)
            m[j].append(i)
        seen = set([start])
        q = collections.deque(m[start])
        ans = 0 if start != end else 1
        while q:
            v = q.popleft()
            if end == v:
                ans += 1

            seen.add(v)
            for i in m[v]:
                if i not in seen:
                    q.append(i)
        return ans


s = Solution()
assert s.validPath(n=3, edges=[[0, 1], [1, 2], [2, 0]], start=0, end=2) == 2, "test1"

assert s.validPath(n=6, edges=[[0, 1], [0, 2], [3, 5], [5, 4], [4, 3]], start=0, end=5) == 0, "test2"
