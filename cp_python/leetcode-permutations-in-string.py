# 2022-01-03
import collections
class Solution:
    def checkInclusion(self, pattern: str, s: str) -> bool:
        ns,np = len(s),len(pattern)
        counter_s,counter_p = collections.Counter(),collections.Counter(pattern)
        we,ws = 0,0
        while we < ns:
            counter_s[s[we]] += 1
            if counter_s == counter_p:
                return True
            if we - ws >= np:
                counter_s[s[ws]] -= 1
                if counter_s[s[ws]]  == 0:
                    del counter_s[s[ws]]
                if counter_s == counter_p:
                    return True
                ws += 1
            we += 1
        if counter_s == counter_p:
            return True
            # print(counter_s)
        return False
