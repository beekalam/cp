# 2021-11-15
import collections
import itertools
from typing import List


class Solution(object):
    def numSpecialEquivGroups(self, words: List[str]) -> int:
        def special_sorted(s):
            a = sorted([c for i, c in enumerate(s) if i % 2 == 0])
            b = sorted([c for i, c in enumerate(s) if i % 2 == 1])
            return "".join([i + j for i, j in itertools.zip_longest(a, b, fillvalue='')])

        m = collections.defaultdict(int)
        for w in words:
            ss = special_sorted(w)
            m[ss] += 1
        ans = len(m)
        # print(ans)
        return ans


s = Solution()
words = ["abcd", "cdab", "cbad", "xyzz", "zzxy", "zzyx"]
assert s.numSpecialEquivGroups(words) == 3, "test1"

words = ["a", "b", "c", "a", "c", "c"]
assert s.numSpecialEquivGroups(words) == 3, "test2"
