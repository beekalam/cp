from collections import defaultdict
class Solution(object):
    def countConsistentStrings(self, allowed, words):
        """
        :type allowed: str
        :type words: List[str]
        :rtype: int
        """
        # this is a comment
        #return sum([ 1 if any([ c in allowed for c in set(w)])  else 0 for w in words ])
        ans = 0
        for i,w in enumerate(words):
            found = True
            for c in set(w):
                if c not in allowed:
                    found = False
                    break
            if found:
                ans += 1

        print("ans: ", ans)
        return ans

s = Solution()

allowed = "ab"
words = ["ad","bd","aaab","baa","badab"]
print(s.countConsistentStrings(allowed, words))
