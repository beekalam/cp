import sys, os

n = int(input())

while n:
    a, b = list(input().split())
    if sorted(a) == sorted(b):
        print("true")
    else:
        print("false")
    n -= 1
