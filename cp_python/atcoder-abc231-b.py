# 2021-12-11
from collections import Counter
n = int(input())
arr = list()
for i in range(n):
    arr.append(input())

cnt = Counter(arr).most_common()
print(cnt[0][0])
