# 2021-10-28

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution(object):
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        tmphead = head
        while head and head.next:
            if head.next.val != head.val:
                head = head.next
            else:
                tmp = head
                while head.next and tmp.val == head.next.val:
                    head = head.next
                if head.val == tmp.val:
                    tmp.next = head.next
                else:
                    tmp.next = head

        return tmphead
