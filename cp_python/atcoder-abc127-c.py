# 2021-12-01
n,m = map(int, input().split())
l,r = [], []
for i in range(m):
	a,b = map(int, input().split())
	l.append(a)
	r.append(b)

mxl = max(l)
mnr = min(r)


if mnr < mxl:
	print(0)
else:
	ans = 0
	mx = max(mxl, mnr)
	mn = min(mxl, mnr)
	print(abs(mn - mx) + 1)
