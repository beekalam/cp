import unittest


class Solution(object):
    def distance(self, a, b):
        if b > a:
            return b - a
        return a - b

    def point_distance(self, point1, point2):
        return self.distance(point1[0], point2[0]) + self.distance(point1[1], point2[1])

    def escapeGhosts(self, ghosts, target):
        """
        :type ghosts: List[List[int]]
        :type target: List[int]
        :rtype: bool
        """
        my_distance = self.point_distance([0, 0], target)

        for ghost in ghosts:
            dist = self.point_distance(target, ghost)
            if my_distance >= dist:
                return False
        return True


class TestEscape(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = Solution()

    def test_1(self):
        ghosts = [[1, 0], [0, 3]]
        target = [0, 1]
        res = self.sol.escapeGhosts(ghosts, target)
        self.assertEqual(True, res)

    def test_2(self):
        ghosts = [[1, 0]]
        target = [2, 0]
        res = self.sol.escapeGhosts(ghosts, target)
        self.assertEqual(False, res)

    def test_3(self):
        ghosts = [[5, 0], [-10, -2], [0, -5], [-2, -2], [-7, 1]]
        target = [7, 7]
        res = self.sol.escapeGhosts(ghosts, target)
        self.assertEqual(False, res)
