import time
import random
import string

class Solution(object):
    def numOfPairs(self, nums, target):
        ans,n = 0,len(nums)
        
        for i in range(n):
            for j in range(n):
                if i!=j and  nums[i] + nums[j] == target:
                    ans += 1
        return ans

s = Solution()
print(s.numOfPairs(nums = ["777","7","77","77"], target = "7777"))
print(s.numOfPairs( nums = ["123","4","12","34"], target = "1234"))
print(s.numOfPairs(nums = ["1","1","1"], target = "11"))



###############################################
def id_generator(size=6, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))
###############################################

nums = []
for i in range(1,100):
    nums.append(id_generator(100))

target = id_generator(100)

start = time.time()

print(s.numOfPairs(nums, target))

end = time.time()

print(end - start)

        
