# 2021-12-01
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def __init__(self):
        self.max_path = 0

    def height(self, root:Optional[TreeNode],size) -> int:
        s1,s2 = 0,0
        if root.right:
            s1 = self.height(root.right,size + 1)
        if root.left:
            s2 = self.height(root.left, size + 1)
        return max(size, s1, s2)
    def diameter(self, root:Optional[TreeNode]) -> int:
        rr = self.height(root.right,1) if root.right else 0
        rl = self.height(root.left,1) if root.left else 0
        self.max_path = max(self.max_path, rr+rl)
        if root.right:
            self.diameter(root.right)
        if root.left:
            self.diameter(root.left)
    def diameterOfBinaryTree(self, root: Optional[TreeNode]) -> int:
        self.diameter(root)
        return self.max_path
