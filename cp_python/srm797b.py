class OrganicChemistry:
    def countHydrogens(self, atoms, X, Y):
        map = {"O": 2, "N": 3, "C": 4}
        points = [map[a] for a in atoms]
        # print(points)
        for idx in range(0, len(X)):
            i, j = X[idx], Y[idx]
            points[i] = points[i] - 1
            points[j] = points[j] - 1

        return sum(points)


o = OrganicChemistry()
print(o.countHydrogens("O", [], []))
print(o.countHydrogens("C", [], []))
print(o.countHydrogens("CCO", [0, 1], [1, 2]))
print(o.countHydrogens("CC", [0, 1], [0, 1]))
print(o.countHydrogens("OO", [1, 0], [1, 0]))
print(o.countHydrogens("OOO", [0, 1, 2], [1, 2, 0]))
print(o.countHydrogens("CCCCCC", [0, 1, 2, 3, 5, 4], [1, 2, 3, 4, 0, 5]))
print(o.countHydrogens("CCN", [0,1], [2,2]))
