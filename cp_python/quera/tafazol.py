def game(number):
    num = str(number)
    a = int(num[0])
    b = int(num[1])
    return max(a, b) - min(a, b)
