import hashlib
import re


class Assert:
    def username(value):
        parts = value.split('_')
        if len(parts) != 2:
            raise Exception('invalid username')
        Assert.alphaonly(value, 'invalid username')

    def alphaonly(value, msg):
        for c in value:
            if c == '_':
                continue
            if not c.isalpha():
                raise Exception(msg)

    def password(value, msg):
        if len(msg) < 8:
            raise Exception(msg)

        if not re.match(".*[A-Z].*", value) or not re.match(".*[0-9].*", value) or not re.match(".*[a-z].*", value):
            raise Exception(msg)

    def nationalcode(value, msg):
        s = str(value)
        last = int(s[-1])
        sum = 0
        for i, c in enumerate(list(reversed(s[:-1]))):
            sum += (i + 2) * int(c)
        # print(sum)
        rem = sum % 11
        # print("rem: ", rem)
        if rem < 2:
            if last != rem:
                raise Exception(msg)
        if last != 11 - rem:
            raise Exception(msg)

    def mobile(value, msg):
        if re.match("^\+989\d{9}", value) or re.match("^09\d{9}$", value):
            return True
        raise Exception(msg)

    def email(value, msg):
        if re.match("^[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-\.]+\.[a-zA-Z]+$", value):
            return True
        raise Exception(msg)


class Site:
    def __init__(self, url_address):
        pass

    def show_users(self):
        pass

    def register(self, user):
        pass

    def login(self, **kwargs):
        pass

    def logout(self, user):
        pass

    def __repr__(self):
        return "Site url:%s\nregister_users:%s\nactive_users:%s" % (self.url, self.register_users, self.active_users)

    def __str__(self):
        return self.url


class Account:
    def __init__(self, username, password, user_id, phone, email):
        Assert.username(username)
        Assert.password(password, 'invalid password')
        Assert.nationalcode(user_id, "invalid code")
        Assert.mobile(phone, 'invalid phone number')
        Assert.email(email, 'invalid email')
        self.username = username
        self.password = hashlib.sha256(password)
        self.user_id = user_id
        self.phone = phone
        self.email = email

    def set_new_password(self, password):
        Assert.password(password, "invalid password")
        self.password = hashlib.sha256(password)

    def username_validation(self, username):
        Assert.username(username)
        self.username = username

    def password_validation(self, password):
        Assert.password(password, "invalid password")

    def id_validation(self, id):
        Assert.nationalcode(id, 'invalid code')

    def phone_validation(self, phone):
        Assert.mobile(phone, 'invalid phone number')

    def email_validation(self, email):
        Assert.email(email, 'invalid email')

    def __repr__(self):
        return self.username

    def __str__(self):
        return self.username


def show_welcome(func):
    pass


def verify_change_password(func):
    pass


@show_welcome
def welcome(user):
    return ("welcome to our site %s" % user)


@verify_change_password
def change_password(user, old_pass, new_pass):
    return ("your password is changed successfully.")


# if __name__ == '__main__':
#     account1 = Account("Ali_Babaei", "5Dj:xKBA",
#                        # "7731689951",
#                        "5139953323",
#                        # "0024848484",
#                        # "0030376459",
#
#                        "09121212121",
#                        "SAliB_SAliB@gmail.com")
