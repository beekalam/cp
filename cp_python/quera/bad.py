import math
from collections import Counter


def title_case(text):
    if len(text) == 0:
        return text
    if len(text) == 1:
        return text.upper()

    return text[0].upper() + text[1:].lower()


def is_bad_word(text):
    l = len(text)
    lb = 0
    for c in text:
        if not c.isalpha():
            lb += 1
    # lb = l - len([a.isalpha() for a in text])
    # print(text)
    # print(lb)
    return lb >= math.ceil(l / 2)


def clean_bad_word(text):
    return "".join(filter(lambda x: x.isalpha(), text))


def process_word(text):
    if is_bad_word(text):
        return ""
    return clean_bad_word(text)


def words_check(text):
    # words = [title_case(process_word(word)) for word in text.split()]
    words = []
    for word in text.split():
        w = title_case(process_word(word))
        if w:
            words.append(w)
    return dict(Counter(words))
