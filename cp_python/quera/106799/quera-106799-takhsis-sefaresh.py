import sys
import time
from functools import cmp_to_key
from sys import stdin
import collections


class LineParser:
    STRING = 'string'
    TUPLE = 'tuple'
    INT = 'int'
    LIST = 'list'

    def __init__(self, line):
        self.line = line

    def parse_line(self, types):
        res = []
        for l, t in zip(self.line, types):
            if t == LineParser.STRING:
                res.append(l)
            elif t == LineParser.INT:
                res.append(int(l))
            elif t == LineParser.TUPLE:
                l = l.replace('(', '').replace(')', '').strip().split(',')
                l = list(map(int, l))
                res.append(tuple(l))
            elif t == LineParser.LIST:
                l = l.replace('(', '').replace(')', '').split(',')
                l = list(map(int, l))
                res.append(l)
        return res


class Parser:
    def __init__(self, handler):
        self.commands = {}
        self.command_rule = {}
        self.handler = handler

    def parse_lines(self):
        for line in stdin:
            line = line.replace(', ', ',').split()
            command = line[0]
            if command in self.command_rule:
                lp = LineParser(line[1:])
                self.commands[command] = lp.parse_line(self.command_rule[command])
            else:
                self.commands[command] = line[1:] if len(line) > 1 else []

    def handle(self):
        for command, input in self.commands.items():
            self.handler.dispatch(command, input)

    def add_parse_rule(self, command, rule):
        self.command_rule[command] = rule

    def get_commands(self):
        return self.commands

    def get_command_rules(self):
        return self.command_rule

class Orders:
    User = collections.namedtuple('User', ('position',
                                           'service_category',
                                           'status',
                                           'order_id',
                                           'credit'))
    Order = collections.namedtuple('Order', ('service_category',
                                             'start_position',
                                             'finish_position',
                                             'status',
                                             'cost',
                                             'created_at',
                                             'id'))

    ORDER_PENDING = 'PENDING'
    ORDER_ARRIVED = 'ORDER_ARRIVED'
    ORDER_PICKUP = 'PICKUP'
    ORDER_DELIVERED = 'DELIVERED'
    DRIVER_BUSY = 'BUSY'
    DRIVER_FREE = 'FREE'
    ORDER_STATUS_ORDER = [
        ORDER_PENDING,
        ORDER_ARRIVED,
        ORDER_PICKUP,
        ORDER_DELIVERED
    ]

    def __init__(self):
        self.company_credit = 0
        self.users = {}
        self.orders = {}
        self.order_id = 0
        self.handle_map = {
            'ADD-DRIVER': self.add_driver,
            'CREATE-ORDER': self.create_order,
            'ASSIGN-NEXT-ORDER': self.assign_next_order,
            'GET-DRIVER': self.get_driver,
            'ORDER-UPDATE': self.order_update,
            'GET-ORDER': self.get_order,
            'GET-ORDER-LIST': self.get_order_list,
            'GET-DRIVER-LIST': self.get_driver_list,
            'GET-NEAR-DRIVER': self.get_near_driver,
            'GET-CNT-ORDER': self.get_cnt_order,
            'GET-NEAREST-PENDING-ORDER': self.get_nearest_pending_order,
            'GET-COMPANY': self.get_company,
            'END': self.end
        }

    def get_near_driver(self, position, count):
        pass

    def get_cnt_order(self, position, distance, start_or_finish):
        pass

    def get_nearest_pending_order(self, position):
        pass

    def get_company(self):
        pass

    def next_order_id(self):
        self.order_id += 1
        return self.order_id

    def end(self):
        print('returning')

    def dispatch(self, command, input):
        if command in self.handle_map:
            self.handle_map[command](*input)

    def add_driver(self, user, position, service_category):
        if user in self.users:
            print("user previously added")
        else:
            self.users[user] = self.User(position=position,
                                         service_category=service_category,
                                         status=self.DRIVER_FREE,
                                         credit=0,
                                         order_id=0)
            print("user added successfully")

    def create_order(self, service_category, start_position, finish_position):
        if start_position == finish_position:
            print("invalid order")
        else:
            id = self.next_order_id()
            o = self.Order(service_category=service_category,
                           start_position=start_position,
                           finish_position=finish_position,
                           status=self.ORDER_PENDING,
                           cost=self.order_cost(service_category, start_position, finish_position),
                           created_at=time.time(),
                           id=id)
            self.orders[id] = o
            print(id)

    def order_cost(self, service_category, start, end):
        k = len(self.get_pending_orders(service_category))
        return (k + self.distance(start, end)) * 100

    def get_pending_orders(self, service_category):
        return [order for order in self.orders.values() if
                order.status == self.ORDER_PENDING and order.service_category == service_category]

    def assign_next_order(self, user):
        if user not in self.users:
            print("invalid driver name")
            return
        if self.users[user].status == self.DRIVER_BUSY:
            print("driver is already busy")
            return

        pending_orders = self.get_pending_orders(self.users[user].service_category)
        if len(pending_orders) == 0:
            print("there is no order right now")
            return

        pending_orders = self.sort_orders(pending_orders, user)
        print(pending_orders)
        order = pending_orders[0]
        print("Selected order: ", order)

        self.users[user].status = self.DRIVER_BUSY
        self.users[user].order_id = order.id
        self.orders[order.id].status = self.ORDER_ARRIVED
        print("{} assigned to {}".format(order.id, user))

    def sort_orders(self, pending_orders, user):

        def sort_orders(aa, bb):
            da = self.distance(aa, user.position)
            db = self.distance(bb, user.position)
            if da == db:
                return db.created_at - da.created_at
            return da - db

        return sorted(pending_orders, key=cmp_to_key(sort_orders))

    def distance(self, a, b):
        return abs(a[0] - b[0]) + abs(a[1] - b[1])

    def get_driver(self, username):
        if username not in self.users:
            print('invalid driver name')
            return
        driver = self.users[username]
        print("{} {} {}".format(driver.status, driver.position, driver.credit))

    def get_driver_list(self, status):
        users = []
        for username, driver in self.users.items():
            if driver.status == status:
                users.append(username)
        if users:
            print(" ".join(users))
        else:
            print("None")

    def get_order(self, order_id):
        if order_id not in self.orders:
            print("invalid order")
            return
        order = self.orders[order_id]
        driver = None
        for username, d in self.users.items():
            if d.order_id == order_id:
                driver = username
        print("{} {} {}".format(order.status, driver, order.cost))

    def get_order_list(self, status):
        orders = []
        for order_id, order in self.orders.items():
            if order.status == status:
                orders.append(order_id)
        if orders:
            print(" ".join(orders))
        else:
            print("None")

    def order_update(self, status, username, order_id):
        if username not in self.users:
            print('invalid driver name')
            return
        driver = self.users[username]
        if driver.order_id != order_id:
            print('wrong order-id')
            return
        if status not in self.ORDER_STATUS_ORDER or self.ORDER_STATUS_ORDER[-1] == self.orders[order_id].status:
            print('invalid status')
            return
        order = self.orders[order_id]
        next_status_index = self.ORDER_STATUS_ORDER.index(order.status) + 1
        next_status = self.ORDER_STATUS_ORDER[next_status_index]
        if next_status != status:
            print("invalid status")
            return

        if status == self.ORDER_PICKUP:
            self.users[username].position = order.start_position
        if status == self.ORDER_DELIVERED:
            self.users[username].position = order.finish_position
            self.users[username].status = self.DRIVER_FREE
            driver_share = (order.cost * 80) / 100
            self.company_credit += order.cost - driver_share
            self.users[username].credit += driver_share
        print("status changed successfully")


order = Orders()
p = Parser(order)
p.add_parse_rule('ADD-DRIVER', [LineParser.STRING, LineParser.TUPLE, LineParser.STRING])
p.add_parse_rule('CREATE-ORDER', [LineParser.STRING, LineParser.TUPLE, LineParser.TUPLE])
p.add_parse_rule('ASSIGN-NEXT-ORDER', [LineParser.STRING])
p.add_parse_rule('GET-DRIVER', [LineParser.STRING])
p.add_parse_rule('ORDER-UPDATE', [LineParser.STRING, LineParser.STRING, LineParser.INT])
p.add_parse_rule('GET-ORDER', [LineParser.INT])
p.add_parse_rule('GET-NEAR-DRIVER', [LineParser.TUPLE, LineParser.INT])
p.add_parse_rule('GET-CNT-ORDER', [LineParser.TUPLE, LineParser.INT, LineParser.STRING])
p.add_parse_rule('GET-NEAREST-PENDING-ORDER', [LineParser.TUPLE])
p.add_parse_rule('GET-COMPANY', [])
p.add_parse_rule('END', [])

p.parse_lines()
p.handle()

print(p.get_commands())
# print(p.get_command_rules())
