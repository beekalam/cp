import os


def extract_file_names(path):
    extensions = {}
    names = {}
    all_files = {}
    for root, dirs, files in os.walk(path):
        for file in files:
            if file in all_files:
                all_files[file] += 1
            else:
                all_files[file] = 1
            # name, extension = file.split('.')
            # if extension not in ignore_extensions:
            #     if extension in extensions:
            #         extensions[extension] += 1
            #     else:
            #         extensions[extension] = 1
            #
            #     if name in names:
            #         names[name] += 1
            #     else:
            #         names[name] = 1

    return names, extensions, all_files


def scores(all_files, salib_format, sajjad_format):
    salib_score = 0
    sajjad_score = 0
    for file, count in all_files.items():
        if file.endswith(sajjad_format):
            sajjad_score += count
        if file.endswith(salib_format):
            salib_score += count
    return salib_score, sajjad_score


def cheat_candidates(all_files, salib_format, sajjad_format):
    candidates = {}
    for file, count in all_files.items():
        if file.endswith(sajjad_format) or file.endswith(salib_format):
            continue
        candidates[file] = count
    return candidates


def combet(salib_format, sajjad_format, path):
    path = os.path.join(os.getcwd(), path)
    names, extensions, all_files = extract_file_names(path)
    salib_score, sajjd_score = scores(all_files, salib_format, sajjad_format)
    if sajjd_score > salib_score:
        return "Win! Normally!"

    for candidate, count in cheat_candidates(all_files, salib_format, sajjad_format).items():
        if count + sajjd_score > salib_score:
            f, ext = candidate.split('.')
            return "Win! you can win if you cheat on '{}'!".format(f)

    return "Lose! you can't win this game!"

# if __name__ == '__main__':
#     names, extensions, all_files = extract_file_names("sample_tests", ['jpg'])
#     print(names, extensions, all_files)
