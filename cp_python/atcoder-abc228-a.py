# @notsolved: 2021-11-20
s, t, x = list(map(int, input().split()))

i = s
arr = [False] * 24
while True:
    if i == 24:
        i = 0
    arr[i] = True
    if i == t:
        break
    i += 1

if arr[x]:
    print("Yes")
else:
    print("No")
