# 2021-11-27
days = {
    "shanbe": 'even',
    "yekshanbe": 'odd',
    "doshanbe": 'even',
    "seshanbe": 'odd',
    "chaharshanbe": 'even',
    "panjshanbe": 'odd',
    "jome": 'tatil'
}

s = input().strip()
if days[s] == 'even':
    print('perspolis')
elif days[s] == 'odd':
    print('bahman')
else:
    print('tatil')
