# 2021-11-23
class Solution:
    def generate_words(self, i, j, dir, current_str):
        if len(current_str) == self.len_word:
            self.words.add(current_str)
            return
        if i >= 0 and i < self.w and j >= 0 and j < self.h:
            if dir == "RIGHT":
                self.generate_words(i + 1, j, dir, current_str + self.board[i][j])
            # self.generate_words(i - 1, j, current_str + self.board[i][j])
            if dir == "DOWN":
                self.generate_words(i, j + 1, dir, current_str + self.board[i][j])
            # self.generate_words(i + 1, j - 1, current_str + self.board[i][j])
            # self.generate_words(i + 1, j + 1, current_str + self.board[i][j])
            # self.generate_words(i - 1, j - 1, current_str + self.board[i][j])

    def solve_recursive(self, board, word):
        self.words = set()
        self.word = word
        self.len_word = len(self.word)
        self.board = board
        self.w = len(board)
        self.h = len(board[0])
        for i in range(self.w):
            for j in range(self.h):
                self.generate_words(i, j, "RIGHT", "")
                self.generate_words(i, j, "DOWN", "")
        return word in self.words

    def solve_iterative(self, board, word):
        self.len_word = len(word)

        self.board = board
        self.board_cols = list(zip(*self.board))
        m, n = len(board), len(board[0])
        for i in range(m):
            for j in range(n):
                if word in "".join(self.board[i][j:]) or word in "".join(self.board_cols[j][i:]):
                    return True

        return False

    def solve(self, board, word):
        return self.solve_iterative(board, word)
        return self.solve_recursive(board, word)


s = Solution()
board = [
    ["H", "E", "L", "L", "O"],
    ["A", "B", "C", "D", "E"]
]
word = "HELLO"
assert s.solve(board, word) == True, "test1"

board = [
    ["x", "z", "d", "x"],
    ["p", "g", "u", "x"],
    ["k", "j", "z", "d"]
]
word = "xgz"
assert s.solve(board, word) == False, "test2"

board = [["x", "z", "d", "x"], ["p", "g", "u", "x"], ["k", "j", "z", "d"]]
word = "dxx"
assert s.solve(board, word) == False, "test3"
