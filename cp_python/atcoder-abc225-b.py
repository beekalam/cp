n = int(input())
m = {}
for _ in range(n-1):
    i, j = list(map(int, input().split()))
    if i in m:
        m[i] += 1
    else:
        m[i] = 1
    if j in m:
        m[j] += 1
    else:
        m[j] = 1

found = False
for i in m.keys():
    if m[i] == n-1:
        found = True
        break
if found:
    print("Yes")
else:
    print("No")
