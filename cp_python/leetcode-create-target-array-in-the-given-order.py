class Solution(object):
    def createTargetArray(self, nums, index):
        """
        :type nums: List[int]
        :type index: List[int]
        :rtype: List[int]
        """
        arr = []
        for n,idx in zip(nums, index):
            arr.append(n)
            j = len(arr) - 1
            while j > idx:
                arr[j],arr[j-1] = arr[j-1],arr[j]
                j -= 1
        return arr

s = Solution()
nums = [0,1,2,3,4]
index = [0,1,2,2,1]
print(s.createTargetArray(nums, index))

nums = [1, 2, 3, 4, 0]
index = [0,1,2,3,0]
print(s.createTargetArray(nums, index))
