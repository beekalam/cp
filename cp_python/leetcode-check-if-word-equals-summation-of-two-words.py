import string
class Solution:
    def isSumEqual(self, firstWord: str, secondWord: str, targetWord: str) -> bool:
        def strVal(s):
            ans = ""
            for c in s:
                ans += str(string.ascii_lowercase.index(c))
            return ans
        first,second,target = list(map(lambda x: int(strVal(x)),[firstWord, secondWord,targetWord]))
        return first + second == target
