t = int(input())
for i in range(t):
    n = int(input())
    rem = 2 ** n
    ans = ['1', '1']
    rem -= 2
    start = 2
    while rem:
        if len(ans) == n + 1:
            break
        if len(ans) == n:
            ans.append(str(rem))
            rem = 0
        else:
            ans.append(str(start))
            rem -= start
        start += 1

    print(" ".join(ans))
