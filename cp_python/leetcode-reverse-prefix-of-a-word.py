class Solution(object):
    def reversePrefix(self, word, ch):
        """
        :type word: str
        :type ch: str
        :rtype: str
        """
        idx = word.find(ch)
        if idx == -1:
            return word
        
        word =list(word)
        word = list(reversed(word[0:idx+1])) + word[idx+1:]
        # i = 0
        # while i < idx:
        #     word[i],word[idx] = word[idx],word[i]
        #     i += 1
        #     idx -= 1
        return "".join(word)
