# @notsolved: 2021-12-18
from typing import List


class Solution:
    def __init__(self):
        self.seen = set()
        self.visited = set()

    def in_bound(self, grid, i, j):
        return i >= 0 and i < len(grid) and j >= 0 and j < len(grid[0])

    def visit(self, grid, i, j):
        if grid[i][j] == "0":
            return
        # if tuple([i,j]) in self.seen:
        # return
        self.seen.add(tuple([i, j]))
        print(self.seen)
        bounds = [(0, -1), (0, 1), (1, 0), (-1, 0)]
        for a, b in bounds:
            ii = i + a
            jj = j + b
            t = tuple([ii, jj])
            if t not in self.visited and self.in_bound(grid, ii, jj):
                if grid[ii][jj] == "1":
                    self.seen.add(t)
                    self.visit(grid, ii, jj)
                self.visited.add(t)

    def numIslands(self, grid: List[List[str]]) -> int:
        ans = 0
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == "1" and tuple([i, j]) not in self.seen:
                    self.visit(grid, i, j)
                    ans += 1
        return ans


s = Solution()
grid = [
    ["1", "1", "1", "1", "0"],
    ["1", "1", "0", "1", "0"],
    ["1", "1", "0", "0", "0"],
    ["0", "0", "0", "0", "0"]
]
assert s.numIslands(grid) == 1, "test1"
