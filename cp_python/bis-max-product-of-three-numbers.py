class Solution:
    def solve(self, nums):
        nums.sort()
        count = 3
        mx = 1
        i = 1
        while count:
            mx = mx * nums[-i]
            i += 1
            count -= 1
        if len(nums) > 3 and nums[0] < 0 and nums[1] < 0 and nums[-1] > 0:
            mx = max(mx, nums[0] * nums[1] * nums[-1])
        return mx
