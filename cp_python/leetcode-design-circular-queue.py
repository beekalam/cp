# @notsolved: 2021-11-12
class MyCircularQueue:

    def __init__(self, k: int):
        self.q = [0] * k
        self.idx = 0
        self.k = k
        self.size = 0
        self.head = 0
        self.tail = -1

    def enQueue(self, value: int) -> bool:
        if self.isFull():
            return False
        else:
            self.tail = (self.tail + 1) % self.k
            self.q[self.tail] = value
            self.size += 1
            return True

    def deQueue(self) -> bool:
        if self.isEmpty():
            return False
        else:
            ret = self.q[self.head]
            self.head = (self.head + 1) % self.k
            self.size -= 1
            return True

    def Front(self) -> int:
        if self.isEmpty():
            return -1
        return self.q[0]

    def Rear(self) -> int:
        if self.isEmpty():
            return -1
        return self.q[self.size - 1]

    def isEmpty(self) -> bool:
        return self.size == 0

    def isFull(self) -> bool:
        return self.size == self.k


q = MyCircularQueue(6)
q.enQueue(6)
q.Rear()
q.Rear()
q.deQueue()
q.enQueue(5)
q.Rear()
q.deQueue()
print(q.Front())

# Your MyCircularQueue object will be instantiated and called as such:
# obj = MyCircularQueue(k)
# param_1 = obj.enQueue(value)
# param_2 = obj.deQueue()
# param_3 = obj.Front()
# param_4 = obj.Rear()
# param_5 = obj.isEmpty()
# param_6 = obj.isFull()
