# 2021-11-22
import math

t = int(input())
for _ in range(t):
    n = input()
    arr = list(map(int, input().split()))
    mx, mn = max(arr), min(arr)
    print(mx - mn)
