class Solution:
    def __init__(self):
        self.invalid_cells = []

    def isValid(self, i, j, matrix):
        rc = len(matrix)
        cc = len(matrix[0])
        # checks current row
        for k in range(cc):
            if k != j and matrix[i][k] == 1:
                return False
        # check column
        for k in range(rc):
            if k != i and matrix[k][j] == 1:
                return False
        # check diagonal
        m, n = i + 1, j + 1
        while m < rc and n < cc:
            if matrix[m][n] == 1:
                return False
            m += 1
            n += 1
        m, n = i - 1, j - 1
        while m >= 0 and n >= 0:
            if matrix[m][n] == 1:
                return False
            m -= 1
            n -= 1
        m, n = i + 1, j - 1
        while m < rc and n >= 0:
            if matrix[m][n] == 1:
                return False
            m = m + 1
            n = n - 1
        m, n = i - 1, j + 1
        while m >= 0 and n < rc:
            if matrix[m][n] == 1:
                return False
            m = m - 1
            n = n + 1
        return True

    def solve(self, matrix):
        for i in range(len(matrix)):
            row_has_queen = False
            for j in range(len(matrix[0])):
                if matrix[i][j] == 1:
                    row_has_queen = True
                    if not self.isValid(i, j, matrix):
                        return False
            if row_has_queen == False:
                return False
        return True


s = Solution()
# matrix = [
#     [1, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 1],
#     [0, 0, 0, 0, 0],
#     [0, 0, 0, 1, 0]
# ]
# print(s.solve(matrix))
# matrix = [
#     [0, 0, 0, 1, 0],
#     [0, 1, 0, 0, 0],
#     [0, 0, 0, 0, 1],
#     [0, 0, 1, 0, 0],
#     [1, 0, 0, 0, 0]
# ]
#
# print(s.solve(matrix))
# matrix = [
#     [0, 0, 1],
#     [0, 1, 0],
#     [1, 0, 0],
# ]
# print(s.solve(matrix))

matrix = [
    [0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0],
    [1, 0, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 1]
]
print(s.solve(matrix))
