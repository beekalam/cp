from typing import List
from BinaryTreeNode import BinaryTreeNode

def postorder_traversal(tree: BinaryTreeNode) -> List[int]:

    result = []

    in_process = [(tree, False)]
    while in_process:
        node, subtrees_traversed = in_process.pop()
        if node:
            if subtrees_traversed:
                result.append(node.data)
            else:
                in_process.append((node, True))
                in_process.append((node.right, False))
                in_process.append((node.left, False))
    return result
