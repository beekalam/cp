import random
def uniform_random(lower_bound, upper_bound):
    number_of_outcomes = upper_bound - lower_bound
    while True:
        result, i = 0, 0
        while (1 << i) < number_of_outcomes:
            # zero_one_random() is the provided random number generator
            result = (result << 1) | zero_one_random()
            i += 1
        if result < number_of_outcomes:
            break
    return result + lower_bound

def zero_one_random():
    return random.randint(0,1)

print(uniform_random(1,5))

