def can_read_end(A):
    furthest_reach_so_far, last_index = 0, len(A) - 1
    i = 0
    arr = []
    while i <= furthest_reach_so_far and furthest_reach_so_far < last_index:
        arr.append(A[i] + i)
        furthest_reach_so_far = max(furthest_reach_so_far, A[i] + i)
        i += 1
    print(arr)
    return furthest_reach_so_far >= last_index


A = [3, 3, 1, 0, 2, 0, 1]
print(can_read_end(A))
