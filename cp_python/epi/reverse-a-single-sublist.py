class ListNode:
    def __init__(self, data=0, next_node=None):
        self.data = data
        self.next = next_node

def reverse_sublist(L, start, finish):
    head = L
    dummy_head = sublist_head = ListNode(0, L)
    for _ in range(1, start):
        sublist_head = sublist_head.next

    # Reverse sublist
    sublist_iter = sublist_head.next
    for _ in range(finish - start):
        temp = sublist_iter.next
        sublist_iter.next, temp.next, sublist_head.next = (temp.next, sublist_head.next, temp)
        print_linked_list(head)

    return dummy_head.next

def print_linked_list(head):
    while head:
        print(" ", head.data,end="")
        head = head.next
    print("")

one = ListNode(1)
two = ListNode(2)
three = ListNode(3)
four = ListNode(4)
five = ListNode(5)
six = ListNode(6)
seven = ListNode(7)
eight = ListNode(8)
one.next = two
two.next = three
three.next = four
four.next = five
five.next = six
six.next = seven
seven.next = eight

print_linked_list(reverse_sublist(one, 2,7))

