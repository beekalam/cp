# reverse bits with cache
class ReverseBits:

    def __init__(self) -> None:
        self.cache = [0]
        for i in range(1, 2 ** 16 + 1):
            self.cache.append(self._reverseBits(i))

    def _reverseBits(self, n):
        j = 15
        for i in range(8):
            # extract i-th and j-th bits
            if (n >> i) & 1 != (n >> j) & 1:
                bit_mask = (1 << i) | (1 << j)
                n ^= bit_mask
            j -= 1
        return n

    def reverse_bits(self, x):
        mask_size = 16
        bit_mask = 0xFFFF
        return (self.cache[x & bit_mask] << (3 * mask_size)
                | self.cache[(x >> mask_size) & bit_mask] <<
                (2 * mask_size) |
                self.cache[(x >> (2 * mask_size)) & bit_mask] << mask_size
                | self.cache[(x >> (3 * mask_size)) & bit_mask])
