import string
import functools
class BaseConvert:
    def __init__(self):
        self.map = {}
        for i,val in enumerate(string.hexdigits[:16]):
            self.map[str(i)] = val

    def convert_to_decimal(self, num,base):
        sign = -1 if num[0] == '-' else 1
        num = num[1:] if sign == -1 else num
        idx = 0
        sum = 0
        for c in reversed(str(num)):
            sum += int(c) * pow(base,idx)
            idx += 1

        return sum * sign

    def convert_to_base(self,num, base):
        sign = -1 if num < 0 else 1
        num = abs(num)
        ans = ""
        while num:
            ans = self.map[str(num % base)] + ans
            num //= base
        ans = "-"+ans if sign == -1 else ans
        return ans

    def convert_from_base_to_base(self, num, from_base, to_base):
        fromb = self.convert_to_decimal(num.lower(),from_base)
        return self.convert_to_base(fromb, to_base)


def convert_base(num_as_string, bl, b2):
    def construct_from_base(num_as_int, base):
        return ('' if num_as_int == 0 else
                construct_from_base(num_as_int // base, base) +
                string.hexdigits[num_as_int % base].upper())
    is_negative = num_as_string[0] == '-'
    num_as_int = functools.reduce(
        lambda x, c: x * bl + string.hexdigits.index(c.lower()),
        num_as_string[is_negative:], 0)
    return ('-' if is_negative else '') + ('0' if num_as_int == 0 else
                                            construct_from_base(num_as_int, b2))
b = BaseConvert()
# print(b.convert_to_decimal(16,7))
print(b.convert_from_base_to_base("-615",7, 13))
print(convert_base("-615",7,13))
# print( (string.digits + string.hexdigits)[:14])


