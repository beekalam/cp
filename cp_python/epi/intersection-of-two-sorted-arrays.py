import bisect


def brute_force(A, B):
    return [a for i, a in enumerate(A) if (i == 0 or a != A[i - 1]) and a in B]


# using binary search to improve the brute force implementation
def intersect_two_sorted_arrays_1(A, B):
    def is_present(k):
        i = bisect.bisect_left(B, k)

        print("i:{},k:{}".format(i, k))
        return i < len(B) and B[i] == k

    return [
        a for i, a in enumerate(A)
        if (i == 0 or a != A[i - 1]) and is_present(a)
    ]


def intersection_two_sorted_arrays(A, B):
    i, j, intersection = 0, 0, []
    while i < len(A) and j < len(B):
        if A[i] == B[j]:
            if i == 0 or A[i] != A[i - 1]:
                intersection.append(A[i])
            i, j = i + 1, j + 1
        elif A[i] < B[j]:
            i += 1
        else:
            j += 1
    return intersection


a = list(range(1, 10))
b = list(range(2, 7))
# print(intersect_two_sorted_arrays_1(a,b))
print(intersection_two_sorted_arrays(a, b))
