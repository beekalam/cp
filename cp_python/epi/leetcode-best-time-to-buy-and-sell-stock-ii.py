class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        min_price,max_profit=float('inf'),0.0
        for price in prices:
            max_profit_today = price - min_price
            max_profit = max(max_profit_today, max_profit)
            min_price =min(min_price, price)

        min_price,max_profit_sum=float('inf'),0.0
        for price in prices:
            if min_price == float('inf'):
                min_price = min(min_price,price)
            if price > min_price:
                max_profit_sum += price - min_price
                min_price = float('inf')

        return max(int(max_profit),int(max_profit_sum))
