from collections import Counter


def palindrome_permutation(s):
    return sum(v % 2 for v in Counter(s).values()) <= 1

print(palindrome_permutation("edified"))
print(palindrome_permutation("sag"))

