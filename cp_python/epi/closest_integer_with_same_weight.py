# the correct approach is to swap the two rightmost consecutive bits that differ.
def closest_int_same_bit_count(x: int) -> int:
    num_unsigned_bits = 64
    for i in range(num_unsigned_bits - 1):
        if (x >> i) & 1 != (x >> (i + 1)) & 1:
            x ^= (1 << i) | (1 << (i + 1))  # Swaps bit-i and bit-(i + 1).
            return x

    # Raise error if all bits of x are 0 or 1.
    raise ValueError('All bits are 0 or 1')


ans = closest_int_same_bit_count(92)
print(ans)
print(bin(ans))
ans = closest_int_same_bit_count(6)
print(ans)
print(bin(ans))
