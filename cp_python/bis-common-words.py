# 2021-11-25
class Solution:
    def solve(self, s0, s1):
        st0 = set(map(lambda x: x.lower(), s0.split()))
        st1 = set(map(lambda x: x.lower() ,s1.split()))
        return len(st0 & st1)

s0 = "Hello world hello oyster"
s1 = "World is Your oyster"
s = Solution()
assert  s.solve(s0,s1) == 2,"test1"
