class Solution(object):
    def missingRolls(self, rolls, mean, n):
        x = (mean * (n + len(rolls)))  - sum(rolls)
        # print("{} % {}: {}".format(x,n, x %n))
        if x % n ==0:
            ret = [x // n] * n
        else:
            ret = [x//n] * n
            rem = x % n
            for i in range(len(ret)):
                if ret[i] > 0 and ret[i] < 6:
                    while ret[i] < 6 and rem > 0:
                        ret[i] += 1
                        rem -= 1
            if rem > 0:
                return []
            
        
        # print(ret)
        for i in ret:
            if i > 6 or i < 1:
                return []
        return ret

s = Solution()

print(s.missingRolls(rolls = [3,2,4,3], mean = 4, n = 2))

print(s.missingRolls(rolls = [1,5,6], mean = 3, n = 4))

print(s.missingRolls(rolls = [1,2,3,4], mean = 6, n = 4))

print(s.missingRolls(rolls = [1], mean = 3, n = 1))

print(s.missingRolls([6,3,4,3,5,3], 1, 6))

print(s.missingRolls([4,5,6,2,3,6,5,4,6,4,5,1,6,3,1,4,5,5,3,2,3,5,3,2,1,5,4,3,5,1,5],4, 40))
