import string
from itertools import *
import operator
import pprint


def make_iterables_to_chain():
    yield [1, 2, 3]
    yield ['a', 'b', 'c']


# for i in chain.from_iterable(make_iterables_to_chain()):
#     print(i, end=' ')
# print()
##########################
# for i in islice(range(1000), 5, 10):
#     print(i, end=' ')

# for i, c in zip(count(ord('a')), range(ord('a'), ord('a') + 1)):
#     print("{}: {}".format(i, c))

print(0 * "m")