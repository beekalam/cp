# 2021-11-26
t = int(input())
for i in range(t):
    n,k = list(map(int, input().split()))
    ans = -1
    if n == 0:
        ans = 0
    elif k > n:
        ans = 1
    elif n % k == 0:
        ans = n // k
    print(ans)
