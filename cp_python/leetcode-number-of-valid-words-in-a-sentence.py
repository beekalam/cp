# https://leetcode.com/problems/number-of-valid-words-in-a-sentence/
import re


class Solution(object):
    def countValidWords(self, sentence):
        """
        :type sentence: str
        :rtype: int
        """
        pattern = "^[a-z]+[-]{,1}[a-z]*[!,.-]{,1}$"
        r = re.compile(pattern)
        pattern2 = "^[a-z]*[!,.]{1}$"
        r2 = re.compile(pattern2)
        ans = 0
        for word in sentence.split():
            is_word = word.lower() == word
            is_word = is_word and (r.match(word) or r2.match(word))
            # print("r.match ", word, " ", r.match(word))
            # print("r2.match ", word, " ", r2.match(word))
            if len(word) >= 2 and (not word[-1].isalpha() and not word[-2].isalpha()):
                is_word = False
            if word.endswith('-'):
                is_word = False
            if is_word:
                # print(word)
                ans += 1
        # print("ans: ", ans)
        return ans


s = Solution()
assert s.countValidWords("cat and  dog") == 3, "test1"
assert s.countValidWords("he bought 2 pencils, 3 erasers, and 1  pencil-sharpener.") == 6, "test2"
assert s.countValidWords("alice and  bob are playing stone-game10") == 5, "test3"
assert s.countValidWords("!") == 1, "test4"
assert s.countValidWords("-") == 0, "test5"
assert s.countValidWords(". ! 7hk  al6 l! aon49esj35la k3 7u2tkh  7i9y5  !jyylhppd et v- h!ogsouv 5") == 4, "test6"
