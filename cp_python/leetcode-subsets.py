# 2021-11-26
import copy
from typing import List

from utils.matrix import print_matrix

import copy


class Solution:
    def __init__(self):
        self.ans = []

    def add_to_ans(self, arr):
        if arr not in self.ans:
            self.ans.append(arr)

    def build_subsets(self, nums):
        if len(nums) > 0:
            t = nums.pop()

            for item in self.ans[:]:
                self.add_to_ans([t] + item)
            self.add_to_ans([t])
            self.build_subsets(nums)

    def solve_recursively1(self, nums):
        if nums:
            j = nums.pop(0)
            ans = copy.copy(self.ans)
            for num in ans:
                num.append(j)
                print(num)
                if num not in self.ans:
                    self.ans.append(num)
            self.solve_recursively(nums)

    def solve_recursively(self, head, tail):
        for i in range(len(tail)):
            h = head[:]
            h.append(tail[i])
            if len(tail) > 1:
                self.ans.append(h)
            self.solve_recursively(h, tail[0:i] + tail[i + 1:])

    def subsets(self, nums: List[int]) -> List[List[int]]:
        # self.build_subsets(nums)
        # self.add_to_ans([])
        # return self.ans

        # self.ans = [[i] for i in nums]
        self.ans = [[], nums]
        self.solve_recursively([], nums)
        print(self.ans)
        return self.ans


s = Solution()
assert s.subsets([1, 2, 3]) == [[], [1], [2], [1, 2], [3], [1, 3], [2, 3], [1, 2, 3]]
