import string

alphabet = string.ascii_letters


class Trie_Node:
    def __init__(self, ch):
        self.isWord = False
        self.c = ch
        self.children = {c: None for c in alphabet}


class Trie:
    def __init__(self):
        self.root = Trie_Node('\0')

    def add_word(self, word):
        curr = self.root
        for c in word:
            if curr.children[c] is None:
                curr.children[c] = Trie_Node(c)
            curr = curr.children[c]
        curr.isWord = True

    def is_word(self, word):
        curr = self.root
        for c in word:
            if curr.children[c] is None:
                return False
            curr = curr.children[c]
        return curr.isWord

    def is_concat(self, word):
        curr = self.root
        n = len(word)
        for i, c in enumerate(word):
            if curr.isWord:
                rest = word[i:]
                if self.is_word(rest) or self.is_concat(rest):
                    return True
            if curr.children[c] is not None:
                curr = curr.children[c]
            else:
                return False
        return False


t = Trie()
wordDict = ["leet", "code"]
wordDict = ["apple", "pen"]
wordDict = ["cats", "dog", "sand", "and", "cat"]
for w in wordDict:
    t.add_word(w)

# print(t.is_concat("applepenapple"))
print(t.is_concat("catsandog"))
