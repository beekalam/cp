from typing import List

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        min_price, max_profit = float('inf'), 0.0
        for price in prices:
            max_profit_today = price - min_price
            max_profit = max(max_profit_today, max_profit)
            min_price = min(min_price, price)
        return int(max_profit)

s = Solution()
print(s.maxProfit([7,1,5,3,6,4]))
