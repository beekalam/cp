# @notsolved: 2022-01-07
from typing import List


class Solution:

    def overlaps(self, a, b):
        return (a[0] <= b[0] and b[0] <= a[1]) or (a[0] <= b[1] and b[1] <= a[1])

    def merge(self, a, b):
        return [min(a[0], b[0]), max(a[1], b[1])]

    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        if len(intervals) == 0:
            return [newInterval]
        res = []
        for i, interval in enumerate(intervals):
            if len(newInterval) == 0:
                if self.overlaps(res[-1], interval):
                    res[-1] = self.merge(res[-1], interval)
                else:
                    res.append(interval)
            elif self.overlaps(interval, newInterval):
                res.append(self.merge(interval, newInterval))
                newInterval = []
            elif interval[0] > newInterval[1]:
                res.append(newInterval)
                res.append(interval)
                newInterval = []
            else:
                res.append(interval)

        if len(newInterval) != 0:
            res.append(newInterval)

        print(res)
        return res


s = Solution()
# assert s.insert(intervals=[[1, 3], [6, 9]], newInterval=[2, 5]) == [[1, 5], [6, 9]], "test1"
# assert s.insert(intervals=[[1, 2], [3, 5], [6, 7], [8, 10], [12, 16]], newInterval=[4, 8]) == [[1, 2], [3, 10],
#                                                                                                [12, 16]], "test2"
# assert s.insert([[1, 5]], [6, 8]) == [[1, 5], [6, 8]], "test3"
assert s.insert([[1, 5]], [0, 6]) == [[0, 6]], "test4"
