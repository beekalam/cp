# 2021-11-19
from typing import List
import collections

class Solution:
    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:
        cnt = collections.Counter(arr)
        mc = cnt.most_common()
        mc.sort(key=lambda x: x[1])
        i = 0
        while k:
            key,value = mc[i]
            if value == 0:
                i += 1
            else:
                k -= 1
                mc[i] = (key,value - 1)
                cnt.subtract([key])
                if cnt[key] == 0:
                    del cnt[key]
        ans =  len(cnt)
        return ans

s = Solution()
assert s.findLeastNumOfUniqueInts([5,5,4],1) == 1, "test1"

arr = [4,3,1,1,3,3,2]
k = 3
assert s.findLeastNumOfUniqueInts(arr, k) == 2, "test1"

