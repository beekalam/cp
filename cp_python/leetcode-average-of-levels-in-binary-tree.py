# 2022-01-09
from typing import Optional
from typing import List

import collections
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def averageOfLevels(self, root: Optional[TreeNode]) -> List[float]:
        avgs = []
        q = collections.deque()
        q.append(root)
        while q:
            size = len(q)
            sum = 0
            for _ in range(size):
                r = q.popleft()
                sum += r.val
                if r.left:
                    q.append(r.left)
                if r.right:
                    q.append(r.right)

            avgs.append(sum / size)
        return avgs
