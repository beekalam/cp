class BuyOneGetOneFree:
    def buy(self, prices):
        if len(prices) == 1:
            return prices[0]

        prices = sorted(prices, reverse=True)

        ans = 0
        n = 2
        for l in [prices[i:i + n] for i in range(0, len(prices), n)]:
            ans += max(l)
        return ans


b = BuyOneGetOneFree()
print(b.buy([47]))
print(b.buy([10, 20]))
print(b.buy([10, 20, 30, 20]))
print(b.buy([5, 7, 13, 2, 9]))
print(b.buy([100, 100, 100, 100, 100, 100]))
