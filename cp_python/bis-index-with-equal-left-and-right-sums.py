class Solution:

    def solve(self, nums):
        if len(nums) == 0 or len(nums) == 1:
            return 0
        s = sum(nums)
        print("s: {}".format(s))
        ls = 0
        rs = 0
        for i in range(len(nums)):
            if i == 0:
                ls = 0
                rs = s - nums[0] 
            elif i == len(nums) - 1:
                ls = s - nums[i]
                rs = 0
            else:
                ls += nums[i-1]
                rs -= nums[i]
            print("left_sum: {}".format(ls))
            print("sum: {}".format(s))
            if rs == ls:
                return i
        return -1


s = Solution()
nums = [2, 3, 4, 0, 5, 2, 2]
assert s.solve(nums) == 3, "failed test 1"

nums = [-1, 0]
assert s.solve(nums) == 0, "failed test 2"

nums = [1, -2, 2]
assert s.solve(nums) == 0, "failed test 3"

nums = [-1, 2]
assert s.solve(nums) == -1, "failed test 4"
