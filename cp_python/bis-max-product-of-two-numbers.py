class Solution:
    def solve1(self, nums):
        if len(nums) == 1:
            return nums[0]
        nums.sort()
        ans = nums[-1] * nums[-2]
        ans = max(ans, nums[0] * nums[1])
        return ans

    def solve(self, nums):
        n = len(nums)
        if n == 1:
            return nums[0]
        greatest = [min(nums[0], nums[1]), max(nums[0], nums[1])]
        smallest = list(reversed(greatest))
        for i in range(2, n):
            n = nums[i]
            if n >= greatest[-1]:
                greatest.append(n)
                greatest.pop(0)
            elif n > greatest[0]:
                greatest[0] = n

            if n <= smallest[-1]:
                smallest.append(n)
                smallest.pop(0)
            elif n < smallest[0]:
                smallest[0] = n

        # print("greatest:{},smallest:{}".format(greatest,smallest))
        ans = max(greatest[0] * greatest[1], smallest[0] * smallest[1])
        # print("ans: {}".format(ans))
        return ans


s = Solution()


def test_1():
    assert s.solve([5, 1, 7]) == 35, "test1"


def test_2():
    assert s.solve([7, 1, 7]) == 49, "test_2"


def test_3():
    assert s.solve([-5, 1, -7]) == 35, "test_3"


def test_4():
    assert s.solve([-5, -7]) == 35, "test_4"


def test_5():
    assert s.solve([5, 7]) == 35, "test_5"


def test_6():
    assert s.solve([5, 7, 8]) == 56, "test_6"


def test_7():
    assert s.solve([1, 0]) == 0, "test_7"


def test_8():
    assert s.solve([-2, 3, 0]) == 0, "test_8"
