# 2021-11-12
# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, node, k):
        first = last = None
        head = node
        idx = 0
        while head:
            last = node if not first else last.next
            if idx == k:
                first = head

            head = head.next
            idx += 1
        first.val,last.val = last.val, first.val
        return node
