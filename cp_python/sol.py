class Solution:

    def do_solve(self, n):
        if n == 0:
            return ""
        if n < 10:
            return self.map[n]
        if n < 40:
            return (n // 10) * "X" + self.do_solve(n % 10)
        if n < 50:
            return self.map[40] + self.do_solve(n - 40)
        if n < 90:
            return self.map[50] + self.do_solve(n - 50)
        if n < 100:
            return self.map[90] + self.do_solve(n - 90)
        if n < 400:
            return (n // 100) * self.map[100] + self.do_solve(n % 100)
        if n < 500:
            return self.map[400] + self.do_solve(n - 400)
        if n < 900:
            return self.map[500] + self.do_solve(n - 500)
        if n < 1000:
            return self.map[900] + self.do_solve(n - 900)
        if n < 1400:
            return (n // 1000) * self.map[1000] + self.do_solve(n % 1000)
        if n < 1500:
            return self.map[1400] + self.do_solve(n - 1400)
        if n < 1900:
            return self.map[1500] + self.do_solve(n - 1500)
        if n < 2000:
            return self.map[1900] + self.do_solve(n - 1900)
        if n < 2400:
            return (n // 2000) * self.map[2000] + self.do_solve(n % 2000)
        if n < 2500:
            return self.map[2400] + self.do_solve(n - 2400)
        if n < 2900:
            return self.map[2500] + self.do_solve(n - 2500)
        if n < 3000:
            return self.map[2900] + self.do_solve(n - 2900)
        if n < 3400:
            return (n // 3000) * self.map[3000] + self.do_solve(n % 3000)
        # if stop >= 1 and n >= self.stop_points[stop - 1] and n <= self.stop_points[stop]:
        #     x = self.stop_points[stop - 1]
        #     return self.map[x] + self.do_solve(n - x, stop - 1)
        # if x % 5 == 0 or x % 9 == 0:
        #     return self.map[x] + self.do_solve(n - x, stop - 1)
        # else:
        #     return (n // x) * self.map[x] + self.do_solve()

        # if n < 100:
        #     s = (n // 10) * "X"
        #     return s + self.do_solve(n % 10)

        return "" + self.do_solve(n)

    def solve(self, n):
        self.stop_points = [40, 50, 90, 100, 500, 900, 1000]
        self.map = {
            1: "I",
            2: "II",
            3: "III",
            4: "IV",
            5: "V",
            6: "VI",
            7: "VII",
            8: "VIII",
            9: "IX",
            10: "X",
            40: "XL",
            50: "L",
            90: "XC",
            100: "C",
            400: "CD",
            500: "D",
            900: "CM",
            1000: "M",
            1400: "MCD",
            1500: "MD",
            1900: "MCM",
            2000: "MM",
            2400: "MMCD",
            2500: "MMD",
            2900: "MMCM",
            3000: "MMM"
        }
        return self.do_solve(n)


s = Solution()

print(s.solve(2865))
