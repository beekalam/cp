# @notsolved:2021-11-26
import copy
from functools import lru_cache


class Solution:
    def __init__(self):
        self.res = []

    @lru_cache(maxsize=None)
    def getPerms(self, s):
        if len(s) == 0:
            return [""]
        if len(s) == 1:
            return [s[0]]
        if len(s) == 2:
            return [s[1] + s[0], s[0] + s[1]]

        res = []
        if len(s) > 2:
            for i in range(len(s)):
                for j in self.getPerms(s[0:i] + s[i + 1:]):
                    res.append(s[i] + j)
        return res

    def getPermutation(self, n: int, k: int) -> str:

        arr = [str(i) for i in range(1, n + 1)]
        s_arr = "".join(sorted(arr))
        res = sorted(self.getPerms(s_arr))
        return res[k - 1]


s = Solution()
assert s.getPermutation(3, 3) == "213", "test1"
assert s.getPermutation(4, 9) == "2314", "test2"
assert s.getPermutation(1, 1) == "1", "test3"
assert s.getPermutation(2, 2) == "21", "test4"
assert s.getPermutation(9, 136371) == "21", "test4"
