class Solution(object):
    def maxDepth(self, s):
        """
        :type s: str
        :rtype: int
        """
        o = 0
        c = 0
        ans = 0
        for c in s:
            if c == '(':
                o += 1
            if c == ')':
                o -= 1
            ans = max(o, ans)
        return ans
