# 2021-12-22
class Solution:
    def solve(self, nums):
        j = -1
        n = len(nums)
        for i in range(len(nums)):
            if nums[i] == 0:
                if j < i:
                    j = i
                while j < n and nums[j] == 0:
                    j += 1
                if j < n and j != i:
                    nums[i], nums[j] = nums[j], nums[i]
        return nums


s = Solution()
s.solve([0, 1, 0, 2, 3]) == [1, 2, 3, 0, 0], "test1"
