# 2021-11-20
from functools import cmp_to_key
from typing import List

def sort_votes(a, b):
    ac, ap = a
    bc, bp = b

    for i in range(len(ap)):
        if ap[i] != bp[i]:
            return ap[i] - bp[i]
    return ord(bc) - ord(ac)


class Solution:
    def rankTeams(self, votes: List[str]) -> str:
        n = len(votes[0])
        dic = {}
        chars = set("".join(votes))
        for c in chars:
            dic[c] = [0] * n
        for v in votes:
            for index, key in enumerate(v):
                dic[key][index] += 1
        pairs = [(k, v) for k, v in dic.items()]
        pairs.sort(key=cmp_to_key(sort_votes), reverse=True)
        ans = [item[0] for item in pairs]
        ans = "".join(ans)
        return ans


s = Solution()
assert s.rankTeams(votes=["ABC", "ACB", "ABC", "ACB", "ACB"]) == "ACB", "test1"
assert s.rankTeams(votes=["WXYZ", "XYZW"]) == "XWYZ", "test2"
assert s.rankTeams(votes=["ZMNAGUEDSJYLBOPHRQICWFXTVK"]) == "ZMNAGUEDSJYLBOPHRQICWFXTVK", "test3"
assert s.rankTeams(votes=["BCA", "CAB", "CBA", "ABC", "ACB", "BAC"]) == "ABC", 'test4'
votes = ["FVSHJIEMNGYPTQOURLWCZKAX", "AITFQORCEHPVJMXGKSLNZWUY", "OTERVXFZUMHNIYSCQAWGPKJL", "VMSERIJYLZNWCPQTOKFUHAXG",
         "VNHOZWKQCEFYPSGLAMXJIUTR", "ANPHQIJMXCWOSKTYGULFVERZ", "RFYUXJEWCKQOMGATHZVILNSP", "SCPYUMQJTVEXKRNLIOWGHAFZ",
         "VIKTSJCEYQGLOMPZWAHFXURN", "SVJICLXKHQZTFWNPYRGMEUAO", "JRCTHYKIGSXPOZLUQAVNEWFM", "NGMSWJITREHFZVQCUKXYAPOL",
         "WUXJOQKGNSYLHEZAFIPMRCVT", "PKYQIOLXFCRGHZNAMJVUTWES", "FERSGNMJVZXWAYLIKCPUQHTO", "HPLRIUQMTSGYJVAXWNOCZEKF",
         "JUVWPTEGCOFYSKXNRMHQALIZ", "MWPIAZCNSLEYRTHFKQXUOVGJ", "EZXLUNFVCMORSIWKTYHJAQPG", "HRQNLTKJFIEGMCSXAZPYOVUW",
         "LOHXVYGWRIJMCPSQENUAKTZF", "XKUTWPRGHOAQFLVYMJSNEIZC", "WTCRQMVKPHOSLGAXZUEFYNJI"]
assert s.rankTeams(votes) == "VWFHSJARNPEMOXLTUKICZGYQ", "test4"
assert s.rankTeams(votes=["M", "M", "M", "M"]) == "M", "test5"
