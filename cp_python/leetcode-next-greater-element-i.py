# 2021-11-10
from typing import List


class Solution:
    def nextGreaterElement(self, nums1: List[int], nums2: List[int]) -> List[int]:
        ans = []
        n = len(nums2)
        m = len(nums1)
        for i in range(m):
            a = float('-inf')
            for j in range(nums2.index(nums1[i]) + 1, n):
                if nums2[j] > nums1[i]:
                    a = nums2[j]
                    break
            a = -1 if a == float('-inf') else a
            ans.append(a)
        return ans


s = Solution()

nums1 = [4, 1, 2]
nums2 = [1, 3, 4, 2]
print(s.nextGreaterElement(nums1, nums2))
