class Solution:
    def solve(self, n, lower, upper):
        sd = min(n - 2, upper - lower)
        si = n - sd
        start = upper - (si - 1)
        ans = []
        # print("si: {}, sd: {} start: {}".format(si, sd, start))
        if sd < si:
            return []
        if lower + si > upper:
            return []
        for i in range(0, si):
            ans.append(start)
            start += 1
        start -= 1
        for i in range(0, sd):
            start -= 1
            ans.append(start)
        # print(ans)
        return ans


s = Solution()

print(s.solve(n=5, lower=2, upper=6))
print(".............................")
print(s.solve(n=5, lower=90, upper=92))
print(".............................")
print(s.solve(n=3, lower=8, upper=11))
print(".............................")
print(s.solve(n=6, lower=3, upper=5))
