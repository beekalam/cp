# 2021-11-28
import collections


class Solution:
    def solve(self, s):
        def compress(string):
            sc = collections.Counter(string)
            duplicate_digits = [n for n in sc if sc[n] >= 2]
            ret = "".join(duplicate_digits)
            ret += "".join([str(sc[n]) for n in duplicate_digits])
            ret += "".join([n for n in sc if n not in duplicate_digits])
            return "".join(sorted(ret))

        while True:
            new_string = compress(s)
            if new_string == s:
                break
            s = new_string
        return new_string

    def solve_cmd(self):
        print(self.solve(input()))


s = Solution()
s.solve_cmd()

# assert s.solve("442254545") == "22345", "test1"
# assert s.solve("2223") == "223", "test2"
# assert s.solve("4321") == "1234", "test3"
