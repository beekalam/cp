# 2021-11-28
from functools import cmp_to_key


class Solution:
    def solve(self, a):
        asc_cost = 0
        for i, j in zip(a, sorted(a[:])):
            asc_cost += abs(i - j)

        dsc_cost = 0
        for i, j in zip(a, sorted(a[:], reverse=True)):
            dsc_cost += abs(i - j)

        return min(dsc_cost, asc_cost)


s = Solution()
assert s.solve([1, 4, 3]) == 2, "test1"
assert s.solve([7, 3, 2, 5]) == 6, "test2"
assert s.solve([1, 2, 3, 0]) == 4, "test3"
