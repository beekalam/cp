from collections import deque


class MyStack(object):

    def __init__(self):
        self.a = deque()
        self.b = deque()

    def push(self, x):
        """
        :type x: int
        :rtype: None
        """
        if self.empty():
            self.a.appendleft(x)
            return
        if self.a:
            self.a.appendleft(x)
            return
        if self.b:
            self.b.appendleft(x)

    def pop(self):
        """
        :rtype: int
        """
        if not self.empty():
            if self.a:
                while len(self.a) > 1:
                    self.b.appendleft(self.a.pop())
                return self.a.pop()
            else:
                while len(self.b) > 1:
                    self.a.appendleft(self.b.pop())
                return self.b.pop()

    def top(self):
        """
        :rtype: int
        """
        if not self.empty():
            if self.a:
                last = None
                while self.a:
                    last = self.a.pop()
                    self.b.appendleft(last)
                return last
            else:
                last = None
                while self.b:
                    last = self.b.pop()
                    self.a.appendleft(last)
                return last

    def empty(self):
        """
        :rtype: bool
        """
        return len(self.a) == 0 and len(self.b) == 0