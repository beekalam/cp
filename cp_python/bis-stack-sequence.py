# 2021-11-16
class Solution:
    def solve(self, pushes, pops):
        stack = []
        i, j = 0, 0
        n, m = len(pushes), len(pops)
        while i < n or j < m:
            if i < n:
                if not stack:
                    stack.append(pushes[i])
                    i += 1
                elif stack[-1] == pops[j]:
                    stack.pop()
                    j += 1
                else:
                    stack.append(pushes[i])
                    i += 1
            elif i >= n:
                if j < m and stack[-1] == pops[j]:
                    stack.pop()
                    j += 1
                else:
                    return False
        return True


s = Solution()
assert s.solve([0, 1, 4, 6, 8], [1, 0, 8, 6, 4]) == True, "test1"
assert s.solve([1, 2, 3, 4], [4, 1, 2, 3]) == False, "test2"
