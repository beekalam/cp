t = int(input())
for i in range(t):
    x,y = list(map(int, input().split()))
    s = input()
    ans = x * s.count('1')
    ls,cs = 0,0
    for c in s:
        if c == '0':
            ls = max(ls,cs)
            cs = 0
        else:
            cs += 1
    ls = max(ls,cs)
    ans += ls * y
    print(ans)
