# 2021-11-11
# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
from utils.LinkedList import linkedlist_from_list, linkedlist_to_list, LLNode


class Solution:
    def solve(self, a, b):
        head = LLNode(0)
        ret = head
        while a and b:
            if a.val > b.val:
                b = b.next
            elif b.val > a.val:
                a = a.next
            elif a.val == b.val:
                head.next = LLNode(a.val)
                head = head.next
                a = a.next
                b = b.next
        return ret.next


a = linkedlist_from_list([1, 3, 4])
b = linkedlist_from_list([2, 3, 7, 9])
s = Solution()
ret = s.solve(a, b)
print(linkedlist_to_list(ret))
