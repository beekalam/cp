n,x = list(map(int, input().split()))
l = list(map(int, input().split()))
d = 0
ans = 1
for i in range(len(l)):
    new_d = d + l[i]
    if new_d <= x:
        d = new_d
        ans += 1
    else:
        break
print(ans)
