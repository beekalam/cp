from collections import Counter
from string import ascii_lowercase


class Solution:
    def solve(self, s):
        equality_cost = less_than_cost = greater_than_cost = len(s) // 2
        middle = len(s) // 2
        print(s[0:middle], s[middle:])
        for c, d in zip(s[0:middle], s[middle:]):
            if c == d:
                equality_cost -= 1
            if c < d:
                less_than_cost -= 1
            if c > d:
                greater_than_cost -= 1
        print([equality_cost, greater_than_cost, less_than_cost])
        return min(equality_cost, greater_than_cost, less_than_cost)


s = Solution()
# s.solve("ccbc")
# s.solve("bccbba")  # 1
# s.solve("aaabba")  # 1
print(s.solve("cdac"))  # 1
