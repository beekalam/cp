# class Solution(object):
#     def shuffle(self, nums, n):
#         """
#         :type nums: List[int]
#         :type n: int
#         :rtype: List[int]
#         """
#         mid = len(nums) // 2
#         ans = []
#         for i in range(mid):
#             ans.append(nums[i])
#             ans.append(nums[mid + i])
#
#         return ans
#
#
# s = Solution()
# nums = [2, 5, 2, 3, 4, 7]
# n = 3
# print(s.shuffle(nums, n))

from cp_utils import RobinKarp

s = RobinKarp()
print(s.rabin_karp_factor("mansouri", "soumddd", 3))
