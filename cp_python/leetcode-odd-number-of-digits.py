# 2021-12-21
class Solution:
    def solve(self, nums):
        return sum( len(str(i)) % 2 == 1 for i in nums)
