class Solution(object):
    def solve1(self, m, n, indices):
        mat = [ [0] * n for i in range(m)]
        def incRow(r):
            for i in range(n):
                mat[r][i] += 1
        def incCol(c):
            for i in range(m):
                mat[i][c] += 1
        for (i,j) in indices:
            incRow(i)
            incCol(j)
        print(mat)
        return sum( 1 if mat[i][j] % 2 == 1 else 0
                    for i in range(m)
                    for j in range(n))
    def oddCells(self, m, n, indices):
        """
        :type m: int
        :type n: int
        :type indices: List[List[int]]
        :rtype: int
        """
        # return self.solve1(m,n,indices)

s = Solution()
m = 2
n = 3
indices = [[0,1],[1,1]]
print(s.oddCells(m,n,indices))

m = 2
n = 2
indices = [[1,1],[0,0]]
print(s.oddCells(m,n,indices))
