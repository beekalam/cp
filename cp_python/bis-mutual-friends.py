# 2021-11-14
import collections


class Solution:
    def solve(self, relations):
        dic = collections.defaultdict(set)
        for i, j in relations:
            dic[i].add(j)
        ans = set()
        for k in dic.keys():
            for f in dic[k]:
                if f in dic and k in dic[f]:
                    ans.add(k)
        return sorted(list(ans))


s = Solution()
print(s.solve([[0, 1], [2, 3], [3, 4], [1, 0]]))
