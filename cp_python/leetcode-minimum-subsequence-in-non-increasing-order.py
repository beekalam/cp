# https://leetcode.com/problems/minimum-subsequence-in-non-increasing-order/submissions/
import heapq
import copy
class Solution(object):
    def minSubsequence(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        s = sum(nums)
        ans = []
        heap = []
        heapq.heapify(heap)
        i, j = 0, 0
        max_sum, min_length = float('-inf'), float('inf')
        while i < len(nums):
            heapq.heappush(heap, nums[i])
            while sum(heap) > (s - sum(heap)):
                if len(heap) < min_length:
                    min_length = len(heap)
                    max_sum = sum(heap)
                    ans = copy.copy(heap)
                if len(heap) == min_length and sum(heap) > max_sum:
                    min_length = len(heap)
                    max_sum = sum(heap)
                    ans = copy.copy(heap)
                poped = heapq.heappop(heap)
            i += 1
        return sorted(ans,reverse=True)


s = Solution()
nums = [4, 3, 10, 9, 8]
assert s.minSubsequence(nums) == [10,9] 
assert s.minSubsequence([6]) == [6]
nums = [4, 4, 7, 6, 7]
assert s.minSubsequence(nums) == [7,7,6]
nums = [7, 4, 2, 8, 1, 7, 7, 10]
assert s.minSubsequence(nums) == [10,8,7]
