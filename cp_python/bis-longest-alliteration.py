# 2021-12-25
class Solution:
    def solve(self, words):
        i = 0;
        n = len(words)
        mx = 0
        while i < n:
            length = 1
            j = i
            while j+1 < n and words[j][0] == words[j+1][0]:
                length += 1
                j += 1
            mx = max(length, mx)
            i += 1
        return mx
