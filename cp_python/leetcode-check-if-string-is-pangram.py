import string
class Solution(object):
    def checkIfPangram(self, sentence):
        """
        :type sentence: str
        :rtype: bool
        """
        return all(sentence.find(c) != -1 for  c in string.ascii_lowercase)
