
class Solution(object):
    def toHex(self, num):
        """
        :type num: int
        :rtype: str
        """
        if num < 0:
            num = num * -1
            complement = 0xFFFFFFFF - num + 1
            return self.toHex(complement)
        ans = ""
        m = {}
        for i in list(range(0, 10)):
            m[i] = str(i)
        for i in range(1,7):
            m[9 + i] = chr(ord('a') + i - 1)


        while num >= 16:
            r = num % 16
            ans = m[r] + ans
            num //=  16
        ans = m[num] + ans
        # print(ans)
        return ans


s = Solution()
print(s.toHex(990))
print(s.toHex(-1))
print(s.toHex(16))

