t = int(input())
for i in range(t):
    n, l, x = list(map(int, input().split()))
    r = n - l
    print(min(r, l) * x)
