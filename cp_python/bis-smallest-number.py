class Solution:
    def solve(self, s):
        nums = ['1', '2', '3']
        for i, c in enumerate(s):
            if c == "?":
                exceptions = []
                if i - 1 >= 0:
                    exceptions.append(s[i - 1])
                if i + 1 < len(s):
                    exceptions.append(s[i + 1])
                choices = sorted(filter(lambda i: i not in exceptions, nums))
                # print(choices)
                # print(choices[0])
                s = s[0:i] + str(choices[0]) + s[i + 1:]
        return s


s = Solution()
print(s.solve("3?2??"))
print(s.solve("???"))
