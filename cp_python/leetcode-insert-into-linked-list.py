# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, head, pos, val):
        if pos == 0:
            node = LLNode(val,next=head)
            return node
        tmp,prev_node = head,head
        c = 0
        while True:
            if c + 1 == pos:
                node = LLNode(val, next=tmp.next)
                tmp.next = node
                return head
            prev_node = tmp
            tmp = tmp.next
            c = c + 1
        return head

