n, q = list(map(int, input().split()))
s = list(input().strip())
for i in range(q):
    t, l, r = list(map(int, input().split()))
    l = l - 1
    r = r - 1
    if t == 2:
        balanced = True
        o = 0
        c = 0
        for i in range(r, l + 1):
            if s[i] == '(':
                o += 1
                if c > 0 and o > c:
                    balanced = False
                    break
            else:
                c += 1
                if c > o:
                    balanced = False
                    break
        if o != c:
            balanced = False
        if balanced == True:
            print("Yes")
        else:
            print("No")

    else:
        s[l], s[r] = s[r], s[l]
