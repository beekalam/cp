class Solution(object):
    def interpret(self, command):
        """
        :type command: str
        :rtype: str
        """
        ans = []
        n = len(command)
        lp = len('()')
        lal = len('(al)')
        for i, c in enumerate(command):
            if c == 'G':
                ans.append('G')
            elif i + lp <= n and command[i:i + lp] == '()':
                ans.append('o')
            elif i + lal <= n and command[i:i + lal] == '(al)':
                ans.append('al')

        return "".join(ans)


s = Solution()
command = "G()(al)"
print(s.interpret(command))
