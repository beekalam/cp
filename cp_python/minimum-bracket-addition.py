class Solution:
    def solve(self, s):
        stack = []
        ans = 0
        for i, c in enumerate(s):
            if len(stack) == 0:
                if c == '(':
                    stack.append(c)
                else:
                    ans += 1
            else:
                if stack[-1] == '(' and c == ')':
                    stack.pop()
                elif stack[-1] == '(' and c == '(':
                    stack.append(c)
            # print("stack: {}, ans: {}, s: {}".format(stack, ans, s[0:i]))
        return ans + len(stack)


sol = Solution()
s = ")))(("
print(sol.solve(s))
s = "(())"
print(sol.solve(s))
