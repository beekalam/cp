from collections import Counter
class Solution:
    def areOccurrencesEqual(self, s: str) -> bool:
        c = Counter(s)
        return all(c[s[0]] == i for i in c.values())
