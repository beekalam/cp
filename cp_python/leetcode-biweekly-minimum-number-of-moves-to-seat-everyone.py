class Solution(object):
    def minMovesToSeat(self, seats, students):
        """
        :type seats: List[int]
        :type students: List[int]
        :rtype: int
        """
        seats.sort()
        students.sort()
        return sum(abs(j - i) for i, j in zip(seats, students))


s = Solution()

assert s.minMovesToSeat(seats=[3, 1, 5], students=[2, 7, 4]) == 4, "test 1"
assert s.minMovesToSeat(seats=[3, 1, 5], students=[2, 7, 4]) == 4, "test 4"
assert s.minMovesToSeat(seats=[4, 1, 5, 9], students=[1, 3, 2, 6]) == 7, "test 2"
assert s.minMovesToSeat(seats=[2, 2, 6, 6], students=[1, 3, 2, 6]) == 4, "test 3"

assert s.minMovesToSeat(seats=[3, 1], students=[2, 7]) == 5, "test 4"
assert s.minMovesToSeat(seats=[1], students=[1]) == 0, "test 5"
assert s.minMovesToSeat(seats=[100], students=[100]) == 0, "test 6"
