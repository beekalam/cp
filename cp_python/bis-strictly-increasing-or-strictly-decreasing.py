# 2022-01-05
class Solution:
    def solve(self, nums):
        n = len(nums)
        if all( nums[i] > nums[i-1] for i in range(1,n) ):
            return True
        if all( nums[i] < nums[i-1] for i in range(1,n) ):
            return True
        return False
