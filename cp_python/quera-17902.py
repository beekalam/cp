# @notsolved 2021-12-21
k = int(input())
r = input()
ans = 0
for i in range(k):
    l = input()
    c = r[i]
    rev = 1
    for j in range(len(l)-1,-1,-1):
        if l[j] == c:
            break
        rev += 1

    ans += min(l.find(c), rev)
print(ans)
