# @notoptimal: 2021-11-13
# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, node):
        ans = []
        h = node
        while h:
            ans.append(h.val)
            h = h.next
        return ans == list(reversed(ans))
