import itertools
import string
import time


class Solution_:
    def solve(self, s, t):
        if len(s) != len(t):
            return False

        translation = {}
        rtranslation = {}
        for c1, c2 in zip(s, t):
            translation.setdefault(c1, c2)
            rtranslation.setdefault(c2, c1)
            if translation[c1] != c2:
                return False
            if rtranslation[c2] != c1:
                return False
        return True

# s = Solution()
# s.solve("coco","kaka")

class Solution___:
    def brute_force(self, s):
        for i in range(1,len(s)):
            if len(s.replace(s[0:i],"")) == 0:
                return True
        return False

    def solve1(self, s):
        longest_prefix = ""
        for i in range(1, len(s)):
            if s.find(s[:i], i) != -1:
                longest_prefix = s[:i]
            else:
                break
        print("longest_prefix: {}".format(longest_prefix))
        if len(longest_prefix) != 0 and len(s.replace(longest_prefix, "")) == 0:
            return True
        return False

    def solve3(self,s):
        prefix = ""
        for i in range(1,len(s)):
            print("{}".format(s[0:i]))
            print( "-{}".format( s[i:len(s[0:i])] ) )
            if s[0:i] == s[i: len(s[0:i]) + 1]:
                prefix = s[0:i]
            else:
                break

        print(prefix)
    def solve4(self, s):
        prefix = ""
        for i in range(1, len(s)):
            if s[0:i] in s[i:]:
                prefix = s[0:i]
            else:
                break
        # print(prefix)
        return s.replace(prefix,"") == ""

class Solution______:
    def solve(self, s):
        n = len(s)
        for d in range(1, n):
            if n % d == 0 and s[:d] * (n // d) == s:
                print("n: {}, n%d: {}, (n//d): {}".format(n, n % d, n // d))
                return True
        return False

# s = Solution()
# print(s.solve("dogdog"))
# print(s.solve("catdog"))

class Solution_1:
    def solve(self, s):
        r_pos = s.find('R')
        rightmost_b = s.find('B',r_pos + 1)
        leftmost_b = s.find('B', 0, r_pos)
        if -1 in [rightmost_b, leftmost_b]:
            return True
        return False

# ss = "......B....R.............."
# ss = "......B....R............B.."
# s = Solution()
# print(s.solve(ss))

class Solution_2:
    def solve(self, a, b):
        if len(b) > len(a):
            return self.solve(b,a)
        a = ''.join(reversed(list(a)))
        b = ''.join(reversed(list(b)))
        carry = 0
        ans = ""
        for i in range(len(a)):
            sum = int(a[i])  + carry
            if i < len(b):
                sum += int(b[i])
            carry = 0
            if sum >= 10:
                carry = 1
                sum = sum - 10
            ans = str(sum) + ans 
        if carry != 0:
            ans = "1" + ans
        return ans

# s = Solution()
# print(s.solve("12","13"))
# print(s.solve("92","93"))
# print(s.solve("17","3"))
##################################################################
class RunLengthDecodedIterator:
    def __init__(self, s):
        self.s = list(s)
        self._read_next()
        

    def _read_next(self):
        # self.count = int(self.s.pop(0))
        count = ""
        while self.s[0] in string.digits:
            count = count + self.s.pop(0)
        self.count = int(count)
        
        self.char = self.s.pop(0)
        print("count {}, char: {}, len(s): {}".format(self.count, self.char, len(self.s)))

    def next(self):
        if self.count == 0:
            if len(self.s) > 0:
                self._read_next()
            else:
                raise StopIteration
        
        self.count -= 1
        return self.char

        

    def hasnext(self):
        return (self.count > 0) or (len(self.s) > 0)
        

# s = RunLengthDecodedIterator("2a1b")
# s = RunLengthDecodedIterator("2a10b")
# print(s.next())
# print(s.next())
# print(s.next())
# print(s.next())
# print(s.next())
################################################################
class Solution_3:
    def solve(self, s):
        ans = []
        i ,j = 0,0
        while i < len(s):
            if s[i] == '(':
                opening,closing= 1,0
                j = i + 1
                while j < len(s):
                    if s[j] == ')':
                        closing += 1
                    else:
                        opening += 1
                    if opening == closing:
                        ans.append(s[i:j+1])
                        i = j + 1
                    j += 1
        return ans
                    

# ss = "()()(()())"
# s = Solution()
# print( s.solve(ss) )

################################################################

class Solution_4:
    def solve(self, s):
        ss,n = list(s), len(s)
        # break_out = False
        for i in range(n):
            idx = 0
            for j in range(ord('a'),ord('z') + 1):
                c = chr(j)
                if c < s[i] and s.rfind(c) != -1:
                    idx = s.rindex(c)
                    # print("in if idx: {}".format(idx))
                    break
            # for j in range(n-1,i,-1):
            #     if idx != 0 and s[j] < s[i] and s[j] < s[idx]:
            #         idx = j
            #     if idx == 0 and s[j] < s[i]:
            #         idx = j

            if idx:
                # print("swapping")
                ss[i],ss[idx] = ss[idx],ss[i]
                return ''.join(ss)

        return ''.join(ss) 
 
# s = Solution()
# ss ="cbca"
# print(s.solve(ss))

# ss = "srvsobvhgadoc"
# print(s.solve(ss))

# ss = "yxxwvttnnhffedaa"
# print(s.solve(ss)) 

# ss = "bab"
# print(s.solv(ss))



################################################################
class Solution_5:
    def solve2(self, s):
        seen, ans =[], 0
        last_close = 0
        for i,c in enumerate(s):
            if c == "(" and i not in seen:
                # if last_close==0:
                #     start_from=i+1
                # else:
                #     start_from = last_close + 1
                start_from = max(last_close, i+1)
                for j  in range(start_from,len(s)):
                    if s[j] == ")" and j not in seen:
                        last_close = j
                        seen.extend([i,j])
                        ans += 1
                        break
        # print(seen)
        return ans * 2
    
    def solve(self, s):
        o_idx,c_idx,n,ans = 0,0,len(s),0
        while o_idx < n and c_idx <n:
            if c_idx > o_idx and s[o_idx] == "(" and s[c_idx] == ")":
                print("o_idx: {}, c_idx: {}".format(o_idx, c_idx))
                ans += 1
                c_idx += 1
                o_idx += 1
            while o_idx < n:
                if s[o_idx] == '(':
                    break
                o_idx += 1
            while c_idx < n:
                if c_idx > o_idx and s[c_idx] == ")":
                    break
                c_idx += 1
        return ans * 2

# s = Solution()

# ss =  "())(()"
# ss = ")"

# print(s.solve(ss))



def myassert(inp,assertion, message="wrong answer for"):
    if not assertion:
        print(message + " "+ str(inp))
################################################################
class Solution:

    def do_solve(self,s):
        if  s == s[::-1]:
            return 1
        return 0

    def solve(self,s):
        n = len(s)
        ans = 0
        for i in range(n):
            for j in range(n,i,-1):
                ans += self.do_solve(s[i:j])
        return ans


# s = Solution()
# ss = "bobo"
# myassert(ss, s.solve(ss) == 6)

# ss = "aa"
# myassert(ss, s.solve(ss) == 3)
# ss = "cba"

# myassert(ss, s.solve(ss) == 3)

# print(is_palindrome("obo"))

################################################################

