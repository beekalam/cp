# @notsolved: 2022-01-08
class Solution:
    def solve(self, nums):
        if len(nums) == 0:
            return 0
        target = max(nums) + 1
        ans = 0
        n = len(nums)
        for i in sorted(nums):
            print(f"i: {i} n:{n} i+n:", i + n)
            if i + n == target:
                ans += 1
                n -= 1
            # if target - i <= n and i not in seen:
            #     seen.add(i)
            #     ans += 1
        print(ans)
        return ans


s = Solution()
assert s.solve([8, 7, 10, 11]) == 3, "test1"
