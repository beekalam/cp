class Solution:
    def solve(self, a, b):
        n, m = len(a), len(b)
        if m > n:
            return self.solve(b, a)
        carry = 0
        ans = []
        for i in range(1, n+1):
            ca = int(a[n - i])
            cb = int(b[m - i]) if m - i >= 0 else 0
            _sum = ca + cb + carry

            if _sum == 0 or _sum == 1:
                ans.append(_sum)
                carry = 0
            elif _sum == 2:
                ans.append(0)
                carry = 1
            elif _sum == 3:
                ans.append(1)
                carry = 1

        if carry == 1:
            ans.append(1)

        ans.reverse()
        # print(ans)
        return "".join(map(str, ans))


s = Solution()
print(s.solve("1", "1"))
print(s.solve("111", "1"))
