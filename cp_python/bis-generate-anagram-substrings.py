# 2021-12-27
import collections


class Solution:
    def solve(self, s):
        dic = collections.defaultdict(list)
        for i in range(len(s)):
            for j in range(i + 1, len(s) + 1):
                ss = s[i:j]
                dic["".join(sorted(ss))].append(ss)
        ans = []
        for k, v in dic.items():
            # print(v)
            if len(v) >= 2:
                ans.extend(v)

        return sorted(ans)


s = Solution()
assert s.solve("aba") == ["a", "a", "ab", "ba"], "test1"
