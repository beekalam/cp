# 2021-11-20
class Solution:
    def solve(self, prices, k):
        prices.sort()
        ans = 0
        for i in prices:
            if k >= i:
                k -= i
                ans += 1
            else:
                break
        return ans
