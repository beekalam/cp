import unittest

from dailycodingproblem.Problem_004 import Problem_004
from dailycodingproblem.Problem_007 import Problem_007


class TestProblem007(unittest.TestCase):
    def setUp(self) -> None:
        self.sol = Problem_007()

    def test1(self):
        self.assertEqual(3, self.sol.solve('111'))
