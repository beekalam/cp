def exists_helper(board, word, current, i=0, j=0):
    if word == current:
        return True
    if len(current) > len(word):
        return False
    if i < 0 or j < 0 or i >= len(board) or j >= len(board[0]):
        return False
    for m in [0, 1, -1]:
        for n in [0, 1, -1]:
            if exists_helper(board, word, current + board[i][j], i + m, j + n):
                return True
    return False
    # return exists_helper(board, word, current + board[i][j], i + 1, j) or \
    #        exists_helper(board, word, current + board[i][j], i - 1, j) or \
    #        exists_helper(board, word, current + board[i][j], i, j + 1) or \
    #        exists_helper(board, word, current + board[i][j], i, j - 1) or \
    #        exists_helper(board, word, current + board[i][j], i - 1, j - 1) or \
    #        exists_helper(board, word, current + board[i][j], i - 1, j + 1) or \
    #        exists_helper(board, word, current + board[i][j], i + 1, j - 1) or \
    #        exists_helper(board, word, current + board[i][j], i + 1, j + 1)


def exists(board, word):
    for row in board:
        for col in row:
            if col == word[0] and exists_helper(board, word, "", row, col):
                return True


board = [
    ['A', 'B', 'C', 'E'],
    ['S', 'F', 'C', 'S'],
    ['A', 'D', 'E', 'E']
]
print(exists_helper(board, "ABCCE", ""))
print(exists_helper(board, "SEE", ""))
