import threading
import time
from time import sleep


class Scheduler:
    def __init__(self):
        pass

    def delay(self, f, n):
        def sleep_then_call(n):
            sleep(n / 1000)
            f()

        t = threading.Thread(target=sleep_then_call)
        t.start()


def runthis():
    print(time.time())


s = Scheduler()

for i in range(1, 1000):
    s.delay(runthis, i * 1000)
