import unittest

from dailycodingproblem.Problem_004 import Problem_004


class TestProblem004(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = Problem_004()

    def test1(self):
        self.assertEqual(2, self.sol.solve([3, 4, -1, 1]))

    def test2(self):
        self.assertEqual(2, self.sol.solve([1, 3]))

    def test3(self):
        self.assertEqual(3, self.sol.solve([1, 2, 0]))
