def subset_sum_brute_force(nums, k):
    print(nums)
    if k == 0:
        return []
    if not nums and k != 0:
        return None

    nums_copy = nums[:]
    last = nums_copy.pop()

    with_last = subset_sum(nums_copy, k - last)
    without_last = subset_sum(nums_copy, k)
    if with_last is not None:
        return with_last + [last]
    if without_last is not None:
        return without_last


def log(s):
    if isinstance(s, list):
        for row in s:
            print(row)
    else:
        print(s)


def subset_sum(nums, k):
    A = [[None for _ in range(k + 1)] for _ in range(len(nums) + 1)]

    for i in range(len(nums) + 1):
        A[i][0] = []

    for i in range(1, len(nums) + 1):
        for j in range(1, k + 1):
            last = nums[i - 1]
            if last > j:
                A[i][j] = A[i - 1][j]
            else:
                if A[i - 1][j] is not None:
                    A[i][j] = A[i - 1][j]
                elif A[i - 1][j - last] is not None:
                    A[i][j] = A[i - 1][j - last] + [last]
                else:
                    A[i][j] = None

    return A[-1][-1]


s = [12, 1, 61, 5, 9, 2]
k = 24

print(subset_sum(s, k))
