from collections import deque


def max_of_subarrays_brute_force(lst, k):
    for i in range(len(lst) - k + 1):
        print(max(lst[i:i + k]))



def max_of_subarrays(lst, k):
    q = deque()
    for i in range(k):
        # print(q)
        while q and lst[i] >= lst[q[-1]]:
            q.pop()
        q.append(i)
    print(q)
    # Loop invariant: q is a list of indices where their corresponding values are in descending order.
    for i in range(k, len(lst)):
        print(lst[q[0]])
        while q and q[0] <= i - k:
            q.popleft()
        while q and lst[i] >= lst[q[-1]]:
            q.pop()
        q.append(i)
        print(q)
    # print(lst[q[0]])


max_of_subarrays([10, 5, 2, 7, 8, 7], 3)
# max_of_subarrays_brute_force([10, 5, 2, 7, 8, 7], 3)
