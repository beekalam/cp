import unittest
from bisect import bisect_left


class Problem_001:
    def solution_1(self, arr, k):
        """Brute force solution o(N^2)"""
        for i in range(0, len(arr)):
            for j in range(0, len(arr)):
                if i != j and arr[i] + arr[j] == k:
                    return True
        return False

    def solution_2(self, arr, k):
        seen = set()
        for num in arr:
            if k - num in seen:
                return True
            seen.add(num)
        return False

    def two_sum(self, lst, K):
        lst.sort()

        for i in range(len(lst)):
            target = K - lst[i]
            j = self.binary_search(lst, target)

            # Check that binary search found the target and that it's not in the same index
            # as i. If it is in the same index, we can check lst[i + 1] and lst[i - 1] to see
            #  if there's another number that's the same value as lst[i].
            if j == -1:
                continue
            elif j != i:
                return True
            elif j + 1 < len(lst) and lst[j + 1] == target:
                return True
            elif j - 1 >= 0 and lst[j - 1] == target:
                return True
        return False

    def binary_search(self, lst, target):
        lo = 0
        hi = len(lst)
        ind = bisect_left(lst, target, lo, hi)

        if 0 <= ind < hi and lst[ind] == target:
            return ind
        return -1

    def solve(self, arr, k):
        return self.two_sum(arr, k)


class TestProblem001(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = Problem_001()

    def test1(self):
        self.assertTrue(self.sol.solve([10, 15, 3, 7], 17))

    def test2(self):
        self.assertFalse(self.sol.solve([1, 2, 3, 4], 500))

    def test3(self):
        self.assertFalse(self.sol.solve([], 500))
