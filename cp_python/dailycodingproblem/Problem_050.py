class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

    def has_leaves(self):
        return self.right and self.left and self.right.val.isnumeric() and self.left.val.isnumeric()


def sol(root):
    if root.has_leaves():
        return int(eval("{} {} {}".format(root.right.val, root.val, root.left.val)))
    return int(eval("{} {} {}".format(sol(root.right), root.val, sol(root.left))))


r = Node("*", Node("+", Node("3"), Node("2")), Node("+", Node("4"), Node("5")))

print(sol(r))
