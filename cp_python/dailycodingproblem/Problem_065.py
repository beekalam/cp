direction = {
    "R": "D", "D": "L", "L": "U", "U": "R"
}


def traverse(mat, dir, x, y, w, h, count):
    if count <= 0:
        return

    if dir == "R" or dir == "L":
        s = 1 if dir == "R" else -1
        for i in range(w-1):
            print(mat[x][y])
            count -= 1
            y += s
        w -= 1

    if dir == "D" or dir == "U":
        s = 1 if dir == "D" else -1
        for i in range(h-1):
            # print("x, y",x,y)
            print(mat[x][y])
            count -= 1
            x += s
        h -= 1
    traverse(mat, direction[dir], x, y, w, h, count)


mat = [[1, 2, 3, 4, 5],
       [6, 7, 8, 9, 10],
       [11, 12, 13, 14, 15],
       [16, 17, 18, 19, 20]]
w = len(mat[0])
h = len(mat)
print(w, h)
traverse(mat, "R", 0, 0, w, h, w * h)
