from collections import defaultdict

from Node import Node


class Problem_008:

    def solve(self, tree):
        # return self.count_unival_subtrees(tree)
        return self.count_unival_subtrees_2(tree)

    def count_unival_subtrees_2(self, root):
        count, _ = self.helper(root)
        return count

    # Also returns number of unival subtrees, and whether it is itself a unival subtree.
    def helper(self, root):
        if root is None:
            return 0, True

        left_count, is_left_unival = self.helper(root.left)
        right_count, is_right_unival = self.helper(root.right)
        total_count = left_count + right_count

        if is_left_unival and is_right_unival:
            if root.left is not None and root.val != root.left.val:
                return total_count, False
            if root.right is not None and root.val != root.right.val:
                return total_count, False
            return total_count + 1, True
        return total_count, False

    def count_unival_subtrees(self, root):
        if root is None:
            return 0
        left = self.count_unival_subtrees(root.left)
        right = self.count_unival_subtrees(root.right)
        return 1 + left + right if self.is_unival(root) else left + right

    def is_unival(self, root):
        return self.unival_helper(root, root.val)

    def unival_helper(self, root, value):
        if root is None:
            return True
        if root.val == value:
            return self.unival_helper(root.left, value) and self.unival_helper(root.right, value)
        return False

