# dailycodingproblem solutions
def first_missing_positive(nums):
    if not nums:
        return 1
    for i, num in enumerate(nums):
        while i + 1 != nums[i] and 0 < nums[i] <= len(nums):
            v = nums[i]
            nums[i], nums[v - 1] = nums[v - 1], nums[i]
            if nums[i] == nums[v - 1]:
                break
    for i, num in enumerate(nums, 1):
        if num != i:
            return i
    return len(nums) + 1


def first_missing_positive2(nums):
    s = set(nums)
    i = 1
    while i in s:
        i += 1
    return i


class Problem_004:
    def solve(self, arr):
        maximum = None
        sum = 0
        for num in arr:
            if num < 0:
                continue
            if maximum is None:
                maximum = num
            else:
                maximum = max(maximum, num)
            sum += num

        true_sum = (maximum * (maximum + 1)) / 2
        if int(true_sum) == sum:
            return maximum + 1
        else:
            return true_sum - sum
