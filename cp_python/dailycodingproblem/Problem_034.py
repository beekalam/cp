# my solution
def sol(s):
    i = 0
    j = len(s) - 1
    ans = ""
    while i < j:
        if s[j] != s[i]:
            ans += s[j]
            j -= 1
        elif s[i] == s[j]:
            ans += s[i]
            i += 1
            j -= 1
    while i < len(s):
        ans += s[i]
        i += 1
    return ans


def log(s):
    if isinstance(s, list):
        for row in s:
            print(row)
    else:
        print(s)


def make_palindrome(s):
    if len(s) <= 1:
        return s
    table = [['' for i in range(len(s) + 1)] for j in range(len(s) + 1)]

    for i in range(len(s)):
        table[i][1] = s[i]

    for j in range(2, len(s) + 1):
        for i in range(len(s) - j + 1):
            term = s[i:i + j]
            log(term)
            first, last = term[0], term[-1]
            if first == last:
                table[i][j] = first + table[i + 1][j - 2] + last
                log(table)
            else:
                one = first + table[i + 1][j - 1] + first
                two = last + table[i][j - 1] + last
                if len(one) < len(two):
                    table[i][j] = one
                elif len(one) > len(two):
                    table[i][j] = two
                else:
                    table[i][j] = min(one, two)
        log(table)
        log("==============================")

    return table[0][-1]


print(make_palindrome("google"))
# print(sol("goagle"))
# print(sol("race"))
# print(sol("faf"))
