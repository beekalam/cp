class Problem_012:

    def solve(self, n):
        return self.staircase_iterative(n)

    def staircase(self, n):
        if n <= 1:
            return 1
        return self.staircase(n - 1) + self.staircase(n - 2)

    def staircase_iterative(self, n):
        a, b = 1, 2
        for _ in range(n - 1):
            a, b = b, a + b
        return a

    def staircase_general(self, n, X):
        if n < 0:
            return 0
        elif n == 0:
            return 1
        else:
            return sum(self.staircase_general(n - x, X) for x in X)


if __name__ == '__main__':
    p = Problem_012()
    print(p.solve(4))
