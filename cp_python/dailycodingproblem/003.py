import unittest
from bisect import bisect_left

from Node import Node


class Problem_003:

    def serialize(self, root):
        if root is None:
            return '#'
        return '{} {} {}'.format(root.val, self.serialize(root.left), self.serialize(root.right))

    def deserialize(self, data):
        def helper():
            val = next(vals)
            if val == '#':
                return None
            node = Node(int(val))
            node.left = helper()
            node.right = helper()
            return node

        vals = iter(data.split())
        return helper()


class TestProblem003(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = Problem_003()

    def test_can_serialize(self):
        res = self.sol.serialize(Node('root'))
        print(res)
        self.assertEqual('root # #', res)

    def test_can_serialize2(self):
        res = self.sol.serialize(Node('root', Node('a'), Node('b')))
        self.assertEqual('root a # # b # #', res)

    def test_can_serialize3(self):
        res = self.sol.serialize(Node(1, Node(2), Node(3)))
        # print(self.sol.deserialize(res).left.val)
        self.assertEqual(2, self.sol.deserialize(res).left.val)
