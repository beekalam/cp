def power_set(s, indent=0):
    print((indent * "-") + "s:", s)
    if not s:
        return [[]]
    result = power_set(s[1:], indent + 1)
    print((indent * "-") + "result:", result)
    ret = result + [subset + [s[0]] for subset in result]
    print((indent * "-") + "ret:", ret)
    # print((indent * "-"))
    return ret


print(power_set([1, 2, 3]))
