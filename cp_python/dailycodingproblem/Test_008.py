import unittest

from Node import Node
from dailycodingproblem.Problem_008 import Problem_008


class TestProblem008(unittest.TestCase):
    def setUp(self) -> None:
        self.sol = Problem_008()

    def test1(self):
        tree = Node(1)
        self.assertEqual(1, self.sol.solve(tree))

    def test3(self):
        tree = Node(1, Node(0), Node(1))
        self.assertEqual(2, self.sol.solve(tree))

    def test4(self):
        tree = Node(1, Node(0))
        self.assertEqual(1, self.sol.solve(tree))

    def test5(self):
        tree = Node(1, Node(1), Node(1))
        self.assertEqual(3, self.sol.solve(tree))

