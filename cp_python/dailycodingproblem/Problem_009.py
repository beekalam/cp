from collections import defaultdict

from Node import Node


class Problem_009:

    def solve(self, nums):
        print(self.largest_non_adjacent(nums))

    def do_solve(self, nums):
        if len(nums) == 4:
            return max(
                max(nums[0] + nums[2], nums[0] + nums[3]),
                nums[1] + nums[3]
            )
        else:
            return nums[0] + self.do_solve(nums[1:])

    def largest_non_adjacent(self, arr):
        if not arr:
            return 0
        return max(
            self.largest_non_adjacent(arr[1:]),
            arr[0] + self.largest_non_adjacent(arr[2:])
        )

    # non recursive version
    def largest_non_adjacent2(self, arr):
        if len(arr) <= 2:
            return max(0, max(arr))

        cache = [0 for i in arr]
        cache[0] = max(0, arr[0])
        cache[1] = max(cache[0], arr[1])

        for i in range(2, len(arr)):
            num = arr[i]
            cache[i] = max(num + cache[i - 2], cache[i - 1])
        return cache[-1]

    # non-recursive without  array
    def largest_non_adjacent3(self, arr):
        if len(arr) <= 2:
            return max(0, max(arr))

        max_excluding_last = max(0, arr[0])
        max_including_last = max(max_excluding_last, arr[1])

        for num in arr[2:]:
            prev_max_including_last = max_including_last

            max_including_last = max(max_including_last, max_excluding_last + num)
            max_excluding_last = prev_max_including_last

        return max(max_including_last, max_excluding_last)


if __name__ == '__main__':
    p = Problem_009()
    p.solve([2, 4, 6, 2, 5])
    p.solve([5, 1, 1, 5])
