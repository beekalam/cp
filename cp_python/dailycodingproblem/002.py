import unittest
from bisect import bisect_left


class Problem_002:

    def solution_1(self, arr):
        mul = 1
        for i in arr:
            mul = mul * i

        ans = []
        for i in arr:
            ans.append(mul / i)

        return ans

    def solution_ans(self, nums):
        # Generate prefix products
        prefix_products = []
        for num in nums:
            if prefix_products:
                prefix_products.append(prefix_products[-1] * num)
            else:
                prefix_products.append(num)

        print(prefix_products)
        # Generate suffix products
        suffix_products = []
        for num in reversed(nums):
            if suffix_products:
                suffix_products.append(suffix_products[-1] * num)
            else:
                suffix_products.append(num)
        suffix_products = list(reversed(suffix_products))
        print(suffix_products)

        # Generate result
        result = []
        for i in range(len(nums)):
            if i == 0:
                result.append(suffix_products[i + 1])
            elif i == len(nums) - 1:
                result.append(prefix_products[i - 1])
            else:
                result.append(prefix_products[i - 1] * suffix_products[i + 1])
        return result

    def solve(self, arr):
        return self.solution_ans(arr)


class TestProblem001(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = Problem_002()

    def test1(self):
        self.assertEqual(
            [120, 60, 40, 30, 24],
            self.sol.solve([1, 2, 3, 4, 5])
        )

    def test2(self):
        self.assertEqual(
            [2, 3, 6],
            self.sol.solve([3, 2, 1])
        )
