def permute1(nums):
    if (len(nums) == 1):
        return [nums]

    output = []
    print("nums: ", nums)
    for l in permute(nums[1:]):
        print("L: ", l)
        for idx in range(len(nums)):
            output.append(l[:idx] + [nums[0]] + l[idx:])
    return output


def permute(nums):
    def helper(nums, index, output):
        if index == len(nums) - 1:
            output.append(nums.copy())
        print("===========")
        # print("nums: ", nums)
        print("index: ", index)
        for i in range(index, len(nums)):
            nums[index], nums[i] = nums[i], nums[index]
            # print("nums: ", nums)
            helper(nums, index + 1, output)
            nums[index], nums[i] = nums[i], nums[index]
            print("nums: ", nums)
        print("+++++++++++++++")
    output = []
    helper(nums, 0, output)
    return output


print(permute([1, 2, 3]))
