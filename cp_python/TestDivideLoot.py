import unittest

from DivideTheLoot import DivideLoot


class TestDivideLoot(unittest.TestCase):

    def setUp(self) -> None:
        self.divide_the_loot = DivideLoot()
        self.true_res = "possible"
        self.false_res = "impossible"

    def test_true_for_one_item(self):
        res = self.divide_the_loot.verify(1, [45])
        self.assertEquals(self.true_res, res)

    def test_all_equal(self):
        res = self.divide_the_loot.verify(3, [1, 1, 1])
        self.assertEquals(self.true_res, res)

    def test__(self):
        res = self.divide_the_loot.verify(3, [10, 8, 10, 1, 1])
        self.assertEquals(self.false_res, res)

    def test__2(self):
        res = self.divide_the_loot.verify(2, [10, 10, 1, 1])
        self.assertEquals(self.true_res, res)

    def test__3(self):
        res = self.divide_the_loot.verify(3, [3, 9, 10, 7, 1])
        self.assertEquals(self.true_res, res)

    def test__4(self):
        res = self.divide_the_loot.verify(6, [1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1])
        self.assertEquals(self.true_res, res)

    def test__5(self):
        res = self.divide_the_loot.verify(2, [40, 1, 42])
        self.assertEquals(self.false_res, res)

    def test__6(self):
        res = self.divide_the_loot.verify(2, [2, 1, 1])
        self.assertEquals(self.true_res, res)

    def test__7(self):
        res = self.divide_the_loot.verify(2, [2, 2])
        self.assertEquals(self.true_res, res)

    def test__8(self):
        res = self.divide_the_loot.verify(2, [3, 2, 1])
        self.assertEquals(self.true_res, res)

    def test__8(self):
        res = self.divide_the_loot.verify(2, [3, 2])
        self.assertEquals(self.false_res, res)
