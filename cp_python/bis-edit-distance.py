def longest_common_substring(a, b):
    ans, mi, mj = 0, 0, 0
    for i in range(len(a)):
        for j in range(len(b)):
            if a[i] == b[j]:
                count, m, n = 0, i, j
                while m < len(a) and n < len(b) and a[m] == b[n]:
                    m += 1
                    n += 1
                    count += 1
                if count > ans:
                    ans = count
                    mi = i
                    mj = j
    return mi, mj, ans


class Solution:
    def solve1(self, a, b):
        _, _, m = longest_common_substring(a, b)
        if m == 0:
            return max(len(a), len(b))
        else:
            return max(len(a) - m, len(b) - m)

    def solve(self, a, b):
        n = len(a)
        m = len(b)
        dp = [[None] * (m + 1) for i in range(n + 1)]

        for i in range(n + 1):
            for j in range(m + 1):
                if i == 0:
                    dp[i][j] = j
                elif j == 0:
                    dp[i][j] = i
                elif a[i - 1] == b[j - 1]:
                    dp[i][j] = dp[i - 1][j - 1]
                else:
                    dp[i][j] = 1 + min(dp[i - 1][j], min(dp[i][j - 1], dp[i - 1][j - 1]))
        # print(dp)
        return dp[n][m]


a, b = "daycare", "dycare"
mi, mj, m = longest_common_substring(a, b)
print("mi: {}, mj: {}, m: {}".format(mi, mj, m))
print(a[mi:mi + m])
print(b[mj:mj + m])
