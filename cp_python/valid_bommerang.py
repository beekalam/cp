import collections
import unittest


class Solution(object):
    def isBoomerang(self, points):
        """
        :type points: List[List[int]]
        :rtype: bool
        """
        x1, x2, x3, y1, y2, y3 = points[0][0], points[1][0], points[2][0], points[0][1], points[1][1], points[2][1]

        if (y3 - y2) * (x2 - x1) == (y2 - y1) * (x3 - x2):
            return False
        return True


class TestIsBommerang(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = Solution()

    def test_1(self):
        self.assertEqual(True, self.sol.isBoomerang([[1, 1], [2, 3], [3, 2]]))

    def test_2(self):
        self.assertEqual(False, self.sol.isBoomerang([[1, 1], [2, 2], [3, 3]]))

    def test_3(self):
        self.assertEqual(True, self.sol.isBoomerang([[0, 0], [0, 2], [2, 1]]))
    #
    # def test_4(self):
    #     self.assertEqual("Dire", self.sol.predictPartyVictory("D"))
    #
    # def test_5(self):
    #     self.assertEqual("Radiant", self.sol.predictPartyVictory("RDR"))
    #
    # def test_6(self):
    #     self.assertEqual("Radiant", self.sol.predictPartyVictory("RRR"))
    #
    # def test_7(self):
    #     self.assertEqual("Dire", self.sol.predictPartyVictory("DDD"))
    #
    # def test_8(self):
    #     self.assertEqual("Dire", self.sol.predictPartyVictory("DDRRR"))
