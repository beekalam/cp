import collections


n,m = list(map(int, input().split()))
dic = collections.defaultdict(int)
for i in range(m):
    a,b = list(map(int, input().split()))
    dic[a] += 1
    dic[b] += 1

if all([v <= 2 for _,v in dic.items()]):
    print("Yes")
else:
    print("No")
