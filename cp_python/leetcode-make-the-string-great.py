# 2021-11-11
class Solution:
    def makeGood(self, s: str) -> str:
        def is_lower(a):
            return ord('z') >= ord(a) >= ord('a')

        def is_upper(a):
            return ord('Z') >= ord(a) >= ord('A')

        stack = []
        for c in s:
            if stack:
                a,b = stack[-1],c
                if a.lower() == b.lower() and ( (is_lower(a) and is_upper(b)) or (is_upper(a) and is_lower(b))):
                    stack.pop()
                else:
                    stack.append(c)
            else:
                stack.append(c)
        ans = "".join(stack)
        return ans

s = Solution()
assert s.makeGood("kkdsFuqUfSDKK") == "kkdsFuqUfSDKK","test1"
