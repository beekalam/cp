class Solution(object):
    def construct2DArray(self, original, m, n):
        if m * n != len(original):
            return []
        ans = []
        idx = 0
        for i in range(1,m+1):
            to = idx + n 
            ans.append(original[idx:to])
            idx = to
        return ans
            


s = Solution()
print(s.construct2DArray([3],1,2))
print(s.construct2DArray([1,2],1,1))
print(s.construct2DArray([1,2,3], 1, 3))
print(s.construct2DArray([1,2,3,4], 2, 2))
