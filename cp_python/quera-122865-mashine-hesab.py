from functools import cmp_to_key


class Solution:
    def parse_numbers(self, numbers):
        number = ""
        sign = ""
        ret = []
        neg_count = 0
        pos_count = 0
        for i, c in enumerate(numbers):
            if c in ['-', '+']:
                if number != "":
                    ret.append(sign + number)
                    if sign == '+':
                        pos_count += 1
                    else:
                        neg_count += 1
                number = ""
                sign = c
            else:
                number += c

        if number != "":
            ret.append(sign + number)
            if sign == '+':
                pos_count += 1
            else:
                neg_count += 1
        return ([int(i) for i in ret], pos_count, neg_count)

    def sort(self, numbers):
        def sort(a, b):
            if a < 0 and b < 0:
                return abs(a) - abs(b)
            return a - b

        numbers.sort(key=cmp_to_key(sort))

    def evaluate(self, expr):
        return eval("".join([str(i) if i < 0 else "+" + str(i) for i in expr]))

    def eval_by_first_rul(self, numbers):
        self.sort(numbers)
        expr = []
        while numbers:
            expr.append(numbers.pop())
            if numbers:
                expr.append(numbers.pop(0))
        return expr

    def solve(self, numbers):
        numbers, pos_count, neg_count = self.parse_numbers(numbers)
        # print(pos_count, neg_count)
        if neg_count == pos_count:
            expr = self.eval_by_first_rul(numbers)
            return self.build_ans(expr)
        elif pos_count > neg_count:
            expr = []
            self.sort(numbers)
            while numbers:
                if len(numbers) >= 2 and numbers[0] < 0:
                    expr.append(numbers.pop())
                    expr.append(numbers.pop(0))
                else:
                    break

            res = self.evaluate(expr)
            if res > numbers[-1]:
                while numbers:
                    expr.append(numbers.pop())
            else:
                numbers[-1] = -1 * numbers[-1]
                while numbers:
                    expr.append(numbers.pop())

            return self.build_ans(expr)
        else:
            expr = []
            self.sort(numbers)
            # print(numbers)
            while numbers:
                if len(numbers) >= 2 and numbers[-1] >= 0:
                    expr.append(numbers.pop())
                    expr.append(numbers.pop(0))
                else:
                    break

            res = self.evaluate(expr)
            # print(numbers)
            # print(res)
            if res > numbers[-1]:
                numbers[-1] = -1 * numbers[-1]
                expr.append(numbers.pop())
                while numbers:
                    expr.append(numbers.pop(0))
            else:
                while numbers:
                    expr.append(numbers.pop(0))
            return self.build_ans(expr)

    def build_ans(self, expr):
        expr_str = "".join([str(i) if i < 0 else "+" + str(i) for i in expr])
        return "{}={}".format(expr_str, eval(expr_str))

    def solve_cmd(self):
        line = input().strip()
        print(self.solve(line))


s = Solution()
s.solve_cmd()
# assert s.solve("+6+66+666-6666-66666-66") == -72660, "test1"
# assert s.solve("+9+7+8+11-1") == eval("+9+7+8+11-1"), "test1"
# assert s.solve("+1-12-17-19-20") == eval("+1-12+20-17-19"), "test3"
