n = int(input())

nums = [int(i) for i in input().split(" ")]
res = []
for i in range(0, len(nums), 2):
    m, n = nums[i], nums[i + 1]
    if m > n:
        res.append([m, i])
    else:
        res.append([n, i + 1])

while len(res) > 2:
    m, n = res.pop(0), res.pop(0)
    if m[0] > n[0]:
        res.append(m)
    else:
        res.append(n)

m, n = res.pop(0), res.pop(0)

if m[0] > n[0]:
    print(n[1] + 1)
else:
    print(m[1] + 1)
