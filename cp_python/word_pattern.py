import unittest
from collections import Counter


class Solution(object):
    def wordPattern(self, pattern, string):
        """
        :type pattern: str
        :type string: str
        :rtype: bool
        """
        dic = dict()
        string = string.split()
        pattern = list(pattern)
        if len(pattern) != len(string):
            return False
        for i in range(len(string)):
            p = pattern[i]
            s = string[i]
            if s in dic:
                if dic[s] != p:
                    return False
            else:
                if p in dic.values():
                    return False
                dic[s] = p
            # print(dic)

        return True


class TestWordPattern(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = Solution()

    def test_1(self):
        self.assertEqual(True, self.sol.wordPattern("abba", "dog cat cat dog"))

    def test_2(self):
        self.assertEqual(False, self.sol.wordPattern("abba", "dog cat cat fish"))

    def test_3(self):
        self.assertEqual(False, self.sol.wordPattern("aaaa", "dog cat cat dog"))

    def test_4(self):
        self.assertEqual(False, self.sol.wordPattern("abba", "dog dog dog dog"))
