class Solution:
    def solve(self, n):
        length = len(str(n))
        s = 0
        tmp = n
        while tmp:
            s += (tmp % 10) ** length
            tmp = tmp // 10
        return s == n
