class Solution:
    def solve(self, nums):
        n = len(nums)
        arr = []
        for i in range(1, n):
            if nums[i] > nums[i - 1]:
                if len(arr) == 0:
                    arr.append(1)
                elif arr[-1] != 1:
                    arr.append(1)
            elif nums[i] < nums[i - 1]:
                if len(arr) == 0:
                    arr.append(-1)
                elif arr[-1] != -1:
                    arr.append(-1)
            else:
                return False
                # if len(arr) == 0:
                #     return False
                # elif arr[-1] != 0:
                #     arr.append(0)
            if len(arr) > 0 and arr[0] != 1:
                return False
        if arr[0] != 1:
            return False
        return True


s = Solution()
nums = [1, 2, 5, 7, 4, 1, 6, 8, 3, 2]
# print(s.solve(nums))

nums = [2, 1, 0]
assert s.solve(nums) == False

nums = [0, 2, 2]
assert s.solve(nums) == False
