import itertools



class Solution(object):

    def isValid(self, matrix):
        rc = len(matrix)
        cc = len(matrix[0])
        for i in range(rc):
            for j in range(cc):
                if matrix[i][j] == 1:
                    # in current row
                    for n in range(cc):
                        if n != j and matrix[i][n] == 1:
                            return False
                    # current column
                    for m in range(rc):
                        if m != i and matrix[m][j] == 1:
                            return False
                    # diagonals
                    a, b = i - 1, j + 1
                    while a >= 0 and b < cc:
                        if matrix[a][b] == 1:
                            return False
                        a, b = a - 1, b + 1

                    a, b = i + 1, j + 1
                    while a < rc and b < cc:
                        if matrix[a][b] == 1:
                            return False
                        a, b = a + 1, b + 1

                    a, b = i - 1, j - 1
                    while a >= 0 and b >= 0:
                        if matrix[a][b] == 1:
                            return False
                        a, b = a - 1, b - 1

                    a, b = i + 1, j - 1
                    while a < rc and b >= 0:
                        if matrix[a][b] == 1:
                            return False
                        a, b = a + 1, b - 1
        return True

    def solveNQueens(self, n):
        """
        :type n: int
        :rtype: List[List[str]]
        """
        ans = []
        idx = 0
        for lst in itertools.permutations(list(range(n))):
            board = []
            for num in lst:
                row = [0] * n
                row[num] = 1
                board.append(row)
            if self.isValid(board):
                a = ["".join(map(str, row)).replace('1', 'Q').replace('0', '.')
                     for row in board]
                ans.append(a)
            print(idx)
            idx += 1
        return ans
        # print_matrix(self.isValid(board))
        # if self.is_valid(board):
        #     ans += 1


s = Solution()
ans = s.solveNQueens(9)
print(ans)
