# 2021-12-08
class Solution:
    def addDigits(self, num: int) -> int:
        if num < 10:
            return num
        sm = sum([int(i) for i in str(num)])
        while sm > 9:
            sm = sum([int(i) for i in str(sm)])
        return sm
