def right_shift(a):
    ret = [a[-1]]
    ret.extend(a[0:-1])
    return ret


def left_shift(b):
    ret = b[1:]
    ret.extend([b[0]])
    return ret


s = input().strip()
a = list(s)
b = list(s)
n = len(s)
if n == 1:
    print(s)
    print(s)
else:
    _max = s
    _min = s
    for i in range(n):
        a = right_shift(a)
        b = left_shift(b)
        aa = "".join(a)
        bb = "".join(b)
        _max = max(_max, aa, bb)
        _min = min(_min, aa, bb)

    print(_min)
    print(_max)
