# 2022-01-06
from collections import Counter


class Solution:

    def solve(self, s, p):
        if len(s) < len(p):
            return -1
        ws, we, matched = 0, 0, 0
        mn = len(s) + 1
        freq = Counter(p)
        for we in range(len(s)):
            c = s[we]
            if c in freq:
                freq[c] -= 1
                if freq[c] >= 0:
                    matched += 1

            # shrink the window if we can
            while matched == len(p):

                mn = min(mn, we - ws + 1)

                lc = s[ws]
                ws += 1
                if lc in freq:
                    if freq[lc] == 0:
                        matched -= 1
                    freq[lc] += 1
        # print(mn)
        return mn if mn != len(s) + 1 else -1


s = Solution()
a = "qthequickbrownfox"
b = "qown"
assert s.solve(a, b) == 10, "test1"
a = ""
b = "a"
assert s.solve(a, b) == -1, "test1"
assert s.solve("bb", "aa") == -1, "test2"
