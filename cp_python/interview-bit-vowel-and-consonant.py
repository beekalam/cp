import string
class Solution:
    # @param A : string
    # @return an integer
    def solve(self, A):
        ans = 0
        n = len(A)
        vowels = "aeiou"
        for i in range(n):
            if A[i] in vowels:
                j = 1 
                while i+j < n:
                    if A[i+j] not in vowels:
                        ans += 1
                    # print(A[i:i+j])
                    # print(i+j)
                    j += 1
            else:
                j = 1 
                while i+j < n:
                    if  A[i+j] in vowels:
                        ans += 1
                    print(A[i:i+j])
                    j += 1
        return ans % (10**9 + 7)

s = Solution()
# print(s.solve("aba"))

ss = "inttnikjmjbemrberk"
print(s.solve(ss))
