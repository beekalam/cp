# 2021-11-09
class Solution:
    def canBeTypedWords(self, text: str, brokenLetters: str) -> int:
        b = set(brokenLetters)
        ans = 0
        for word in text.split():
            if any(c in b for c in word):
                ans += 0
            else:
                ans += 1
        return ans
