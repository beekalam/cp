# 2021-11-15
import collections
class Solution:
    def countLargestGroup(self, n: int) -> int:
        dic = collections.defaultdict(int)
        for i in range(1, n+1):
            s = sum(int(c) for c in str(i))
            dic[s] += 1
        mx = max(dic.values())
        return sum (v == mx for v in dic.values())


s = Solution()
assert s.countLargestGroup(13) == 4,"test1"
