t = int(input())
for i in range(t):
    n,x,p = list(map(int, input().split()))
    y = n - x
    x = x * 3
    y = y * -1
    if x+y >= p:
        print("PASS")
    else:
        print("FALSE")
