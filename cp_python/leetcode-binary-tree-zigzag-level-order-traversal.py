# 2022-01-09
import collections
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def zigzagLevelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        if root is None:
            return []
        q = collections.deque()
        level = 1
        q.append(root)
        ans = []
        while q:
            current_level = []
            size = len(q)
            for _ in range(size):
                r = q.popleft()
                if r :
                    if level % 2 == 1:
                        current_level.append(r.val)
                    else:
                        current_level.insert(0, r.val)
                    if r.left:
                        q.append(r.left)
                    if r.right:
                        q.append(r.right)
            level += 1

            ans.append(current_level)
        return ans
