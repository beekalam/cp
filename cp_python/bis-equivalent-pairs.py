# 2021-12-18
import collections
class Solution:
    def solve(self, nums):
        c = collections.Counter()
        ans = 0
        for n in nums:
            if n in c:
                ans += c[n]
            c.update([n])
        return ans
