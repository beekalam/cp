# 2021-12-05
from collections import Counter


class Solution:
    def solve(self, s, t):
        sc = Counter(s)
        tc = Counter(t)
        for i in range(ord('a'), ord('z') + 1):
            c = chr(i)
            if c in tc and c in sc:
                mn = min(sc[c], tc[c])
                sc[c] = sc[c] - mn
                tc[c] = tc[c] - mn
        ans = max(sum(sc.values()), sum(tc.values()))
        return ans


s = Solution()
assert s.solve("ehyoe", "hello") == 2, "test1"
