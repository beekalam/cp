# 2021-12-14
class Solution:
    def solve(self, nums, k):
        lhs,rhs = 0,len(nums)-1
        while lhs < rhs:
            s = nums[lhs] + nums[rhs]
            if s == k:
                return True
            if s < k:
                lhs += 1
            else:
                rhs -= 1


        return False

    def sol1(self, nums, k):
        seen = set()
        for i in nums:
            if k - i in seen:
                return True
            seen.add(i)
        return False
