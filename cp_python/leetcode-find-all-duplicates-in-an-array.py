# 2021-11-15
import collections


class Solution(object):
    def findDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        freq = collections.Counter(nums)
        return [k for k in freq if freq[k] == 2]



nums = [4, 3, 2, 7, 8, 2, 3, 1]
s = Solution()
print(s.findDuplicates(nums))
