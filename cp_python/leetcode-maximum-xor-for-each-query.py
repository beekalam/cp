#https://leetcode.com/problems/maximum-xor-for-each-query/submissions/
from typing import List
import functools


def flip_bits(x, i):
    return x ^ ((1 << i) - 1)


class Solution:
    def getMaximumXor(self, nums: List[int], maximumBit: int) -> List[int]:
        n = len(nums)
        ans = []
        xored = functools.reduce(lambda x, c: x ^ c, nums[1:], nums[0])
        for _ in range(n):
            inverted = flip_bits(xored, maximumBit)
            ans.append(inverted)
            last = nums.pop()
            xored ^= last
        return ans


s = Solution()
assert s.getMaximumXor(nums=[0, 1, 1, 3], maximumBit=2) == [0, 3, 2, 3], "test1"
assert s.getMaximumXor(nums=[2, 3, 4, 7], maximumBit=3) == [5, 2, 6, 5], "test2"
