# @notsolved: 2021-11-29
import collections
import datetime
from functools import cmp_to_key


class Dates:
    Date = collections.namedtuple('Date', ('day', 'start', 'end'))
    Exceptions = collections.namedtuple('exceptions', ('device', 'from_date', 'to_date', 'status', 'id'))
    days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    DEVICE_PRIORITY = ["Tenant", "Store", "Station"]

    def __init__(self):
        self.dates = []
        self.exceptions = []

    def add_date(self, day, start, end):
        h, m = list(map(int, start.split(":")))
        start = datetime.time(h, m)
        h, m = list(map(int, end.split(":")))
        end = datetime.time(h, m)
        day = self.days.index(day)

        self.dates.append(self.Date(day, start, end))
        return self

    def add_exception(self, device, from_date, to_date, status):
        format = "%Y-%m-%dT%H:%M"
        from_date = datetime.datetime.strptime(from_date, format)
        # print("from_date: ", self.days[from_date.weekday()])
        to_date = datetime.datetime.strptime(to_date, format)
        next_id = len(self.exceptions) + 1
        self.exceptions.append(self.Exceptions(device, from_date, to_date, status, next_id))
        return self

    def filter_dates(self, weekday):
        ret = []
        for d in self.dates:
            if d.day == weekday:
                ret.append(d)
        return ret

    def sort_by_priority(self, exceptions):
        def sort(a, b):
            if a.device == b.device:
                return a.id - b.id
            return self.DEVICE_PRIORITY.index(b.device) - self.DEVICE_PRIORITY.index(a.device)

        exceptions.sort(key=cmp_to_key(sort))
        return exceptions

    def date_overlaps(self, query, start, end):
        qts = query.timestamp()
        sts = start.timestamp()
        ets = end.timestamp()
        if qts >= sts and qts <= ets:
            return True
        return self.time_overlaps(query, start, end)

    def time_overlaps(self, query, start, end):
        ans = query.hour >= start.hour and query.hour <= end.hour
        if query.hour == start.hour:
            ans = ans and query.minute >= start.minute
        if query.hour == end.hour:
            ans = ans and end.minute >= query.minute
        return ans

    def query(self, query):
        format = "%Y-%m-%dT%H:%M"
        query = datetime.datetime.strptime(query, format)
        # print("query weekday: ", self.days[query.weekday()])
        ans = "false"
        for d in self.filter_dates(query.weekday()):
            if self.time_overlaps(query, d.start, d.end):
                ans = "true"
        for ex in (self.sort_by_priority(self.exceptions)):  # reversed(self.filter_exceptions(query.month, query.day)):
            if self.date_overlaps(query, ex.from_date, ex.to_date):
                if ex.status == 'closed':
                    ans = "false"
                else:
                    ans = "true"
        return ans


def solve():
    n, m, q = list(map(int, input().split()))
    dates = Dates()
    for _ in range(n):
        day, start, end = input().split()
        dates.add_date(day, start, end)
    for _ in range(m):
        device, from_date, to_date, status = input().split()
        dates.add_exception(device, from_date, to_date, status)
    queries = []
    for _ in range(q):
        queries.append(input())

    for query in queries:
        print(dates.query(query))


solve()
#
# dates = Dates().add_date("Monday", "8:00", "12:00") \
#     .add_exception("Tenant", "2021-10-25T09:00", "2021-10-25T10:00", "closed")
# assert dates.query("2021-11-29T09:00") == "false", "test1"
#
# dates = Dates().add_date("Monday", "8:05", "12:00")
# assert dates.query("2021-11-29T08:01") == "false", "test2"
#
# dates = Dates().add_date("Monday", "8:00", "12:08")
# assert dates.query("2021-11-29T12:09") == "false", "test1"
#
# dates = Dates().add_date("Monday", "8:00", "12:08")
# assert dates.query("2021-11-29T12:08") == "true", "test1"
#
# dates = Dates().add_date("Monday", "8:00", "12:08")
# assert dates.query("2021-11-29T12:07") == "true", "test1"
#
# dates = Dates().add_exception("Tenant", "2021-10-25T09:00", "2021-10-25T10:00", "closed") \
#     .add_exception("Store", "2021-10-25T09:00", "2021-10-25T10:00", "closed") \
#     .add_exception("Station", "2021-10-25T09:00", "2021-10-25T10:00", "closed")
#
# # print_matrix([(d.id, d.device) for d in dates.sort_by_priority(dates.exceptions)])
# assert dates.sort_by_priority(dates.exceptions)[0].device == "Station", "test9"
# assert dates.sort_by_priority(dates.exceptions)[1].device == "Store", "test 10"
# assert dates.sort_by_priority(dates.exceptions)[2].device == "Tenant", "test 11"
#
# dates = Dates().add_exception("Tenant", "2021-10-25T09:00", "2021-10-25T10:00", "closed") \
#     .add_exception("Tenant", "2021-10-25T09:00", "2021-10-25T10:00", "closed") \
#     .add_exception("Tenant", "2021-10-25T09:00", "2021-10-25T10:00", "closed")
# # print_matrix([d.id for d in dates.sort_by_priority(dates.exceptions)])
#
# dates = Dates().add_date("Wednesday", "8:00", "12:00") \
#     .add_date("Wednesday", "6:30", "14:30") \
#     .add_exception("Tenant", "2020-05-20T09:00", "2020-05-20T11:00", "closed")
# assert dates.query("2020-06-10T00:00") == "true", "test1"
#
# dates = Dates().add_date("Wednesday", "8:00", "12:00") \
#     .add_exception("Store", "2020-06-01T08:00", "2020-06-20T20:00", "open")
# assert dates.query("2020-06-10T00:00") == "true", "test1"
