import string
class Solution:
    def solve(self, s):
        n = len(s)
        if n == 0:
            return ""
        arr = []
        i = 0
        while i < n:
            num = ""
            while i < n and s[i] in string.digits:
                num += s[i]
                i += 1
            if len(num) != 0:
                arr.append(s[i] * int(num))
                i += 1
            else:
                arr.append(s[i])
                i += 1

        return "".join(arr)

s = Solution()
ss = "4a3b2c1d2a"
assert s.solve(ss) == "aaaabbbccdaa","test1"
assert s.solve("") == "","test2"
assert s.solve("10a") == "aaaaaaaaaa","test3"
assert s.solve("1a") == "a","test4"
assert s.solve("2a") == "aa","test5"
assert s.solve("2a2b") == "aabb","test6"
