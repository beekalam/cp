# 2021-12-21
n = int(input())
row = '*' * n
if n == 1:
    print("*")
if n== 2:
    print(row)
    print(row)
else:
    mid = '*' + ' ' * (n-2) + '*'
    print(row)
    for i in range(n-2):
        print(mid)
    print(row)
