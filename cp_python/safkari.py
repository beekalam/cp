n, a, b = [int(i) for i in input().split()]
cols = [int(i) for i in input().split()]

# print("n:{} a:{} b:{} \n cols: {}".format(n, a, b, cols))
height = cols[0]
cost = 0
blocks = 0
i = 1
while i < len(cols):
    if cols[i] < height:
        cost += a
        cols[i] = height
    elif cols[i] > height:
        cost += b
        blocks = cols[i] - height
        cols[i] = height
        for j in range(i + 1, len(cols)):
            if cols[j] > height:
                blocks = blocks + (cols[j] - height)
                cols[j] = height
            if cols[j] < height and blocks > 0:
                diff = height - cols[j]
                if diff <= blocks:
                    cols[j] = height
                    blocks = blocks - diff
                else:
                    cols[j] = cols[j] + blocks
                    blocks = 0
        if blocks > 0:
            cols.append(blocks)
            blocks = 0
    i = i + 1
    # print(cost)
    # print(cols)
    # print("================================")
print(cost)
