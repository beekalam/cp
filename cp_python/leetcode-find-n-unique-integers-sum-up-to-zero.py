class Solution(object):
    def sumZero(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        if n == 1:
            return [0]

        ans = []
        if n % 2 == 1:
            ans.append(0)

        idx = 1
        for i in range(0, n-1, 2):
            ans.append(idx)
            ans.append(-1 * idx)
            idx += 1

        return ans
s = Solution()
print(s.sumZero(5))
