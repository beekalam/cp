# 2021-12-28
class Solution:
    def solve(self, contacts):
        st = set()
        ans = 0
        for line in contacts:
            found = False
            for email in line:
                if email in st:
                    found = True
            if not found:
                ans += 1
            for email in line:
                st.add(email)
        return ans

s = Solution()
contacts = [
    ["elon@tesla.com", "elon@paypal.com"],
    ["elon@tesla.com", "elon@spacex.com"],
    ["tim@apple.com"]
]
assert s.solve(contacts) == 2, "test1"

