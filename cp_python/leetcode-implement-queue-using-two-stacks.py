class MyQueue(object):

    def __init__(self):
        self.head = []
        self.tail = []

    def push(self, x):
        """
        :type x: int
        :rtype: None
        """
        self.head.append(x)

    def _populateTailMaybe(self):
        if len(self.tail) == 0 and len(self.head) > 0:
            while len(self.head):
                self.tail.append(self.head.pop())
    def pop(self):
        """
        :rtype: int
        """
        self._populateTailMaybe()
        if len(self.tail) > 0:
            return self.tail.pop()
        

    def peek(self):
        """
        :rtype: int
        """
        self._populateTailMaybe()
        if len(self.tail) > 0:
            return self.tail[-1]
        

    def empty(self):
        """
        :rtype: bool
        """
        return len(self.head) == 0 and len(self.tail) == 0
        


# Your MyQueue object will be instantiated and called as such:
# obj = MyQueue()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.peek()
# param_4 = obj.empty()
