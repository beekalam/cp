# 2022-01-09
class Solution:
    def solve(self, orders):
        m = dict()
        for order in orders:
            t, n = order[0], order[1:]
            delivery_key = 'D' + str(n)
            order_key = 'P' + str(n)
            if t == 'P' and order in m:
                return False
            elif t == 'P' and order in m and m[order] is not None:
                return False
            elif t == 'D' and order_key not in m:
                return False
            elif t == 'D' and order_key in m and m[order_key] == order:
                return False
            if t == 'P':
                m[order] = None
            else:
                m['P' + str(n)] = order

        for order, delivery in m.items():
            if order[0] == 'P' and delivery is None:
                return False
        return True


s = Solution()
assert s.solve(orders=["P1", "P2", "D2", "D1"]) == True, "test1"
