# 2021-11-30
import collections

n, x = list(map(int, input().split()))
lst = list(map(int, input().split()))
m = {}

for i, v in enumerate(lst, start=1):
    m[i] = v

seen = set()

while x not in seen:
    seen.add(x)
    if x in m:
        x = m[x]

print(len(seen))
