#https://leetcode.com/problems/make-two-arrays-equal-by-reversing-sub-arrays/submissions/
class Solution(object):
    def canBeEqual(self, target, arr):
        """
        :type target: List[int]
        :type arr: List[int]
        :rtype: bool
        """
        return sorted(arr) == sorted(target)
