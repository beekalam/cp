# 2021-11-27
def has_carry(a, b):
    a = list(reversed(a))
    b = list(reversed(b))
    for i in range(min(len(a), len(b))):
        if int(a[i]) + int(b[i]) > 9:
            return True
    return False


a, b = input().split()
if has_carry(a, b):
    print("Hard")
else:
    print("Easy")
