# https://leetcode.com/problems/sum-of-left-leaves/
from collections import deque
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution(object):
    def __init__(self):
        self.left_sum = 0

    def leftSum(self, root):
        if root is None:
            return
        if root.left and root.left.left == None and root.left.right == None:
            self.left_sum += root.left.val
        self.leftSum(root.left)
        self.leftSum(root.right)

    def sumOfLeftLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        # recursive approach
        #self.leftSum(root)
        #return self.left_sum

        # iterative approach
        def is_leaf(node):
            return node and node.left is None and node.right is None
        q = deque()
        q.append(('', root))
        ans = 0
        while q:
            t,n = q.popleft()
            if t == 'L' and is_leaf(n):
                ans += n.val
            if n.right:
                q.append(('R',n.right))
            if n.left:
                q.append(('L', n.left))
        return ans
