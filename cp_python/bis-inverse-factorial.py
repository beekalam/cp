# 2021-11-24
class Solution:
    def solve(self, a):
        idx = 2
        while a != 1:
            if a % idx != 0:
                return -1
            a = a // idx
            idx += 1
        return idx - 1
