# 2021-12-10
class Solution:
    def hammingWeight(self, n: int) -> int:
        def count_bits(x):
            c = 0
            while x:
                c += x & 1
                x >>= 1
            return c
        return count_bits(n)
