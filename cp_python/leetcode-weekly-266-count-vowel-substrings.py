class Solution:
    def countVowelSubstrings(self, word: str) -> int:
        vowels = set('aeiou')
        ans = 0
        for i in range(len(word)):
            j = i
            ss = ""
            while j < len(word) and word[j] in vowels:
                ss += word[j]
                if set(ss) == vowels:
                    ans += 1
                j += 1
        return ans

s = Solution()
print(s.countVowelSubstrings('a'))
