# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution(object):
    def __init__(self):
        self.balanced = True
    def height(self, root):
        if root is None:
            return 0
        
        left_height = 1 + self.height(root.left)
        right_height = 1 + self.height(root.right)
        if abs(left_height - right_height) > 1:
            self.balanced = False
        return max(left_height, right_height)

    def isBalanced(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        self.height(root)
        return self.balanced
