# 2022-01-09
class Solution:
    def solve(self, nums):
        m ={i:0 for i in range(len(nums)+1)}
        for i in nums:
            m[i] += 1
        # print(m)
        return [i for i,j in m.items() if j == 0 and i !=0]
