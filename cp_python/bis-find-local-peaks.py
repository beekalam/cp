# 2022-01-08
class Solution:
    def solve(self, nums):
        if len(nums) == 0 or len(nums) == 1:
            return []

        st = set()
        for i in range(1,len(nums)):
            if i - 1 == 0 and nums[i-1] > nums[i]:
                st.add(i-1)
            elif i == len(nums) - 1 and nums[i] > nums[i-1]:
                st.add(i)
            elif i+1 < len(nums) and nums[i] > nums[i-1] and nums[i] > nums[i+1] and i not in st:
                st.add(i)
        return list(st)
