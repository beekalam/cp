# @notsolved: 2021-11-27
class Solution:
    def minimumBuckets(self, s: str) -> int:
        ans = 0
        stack = []
        for i, c in enumerate(s):
            l = len(stack)
            if l < 3:
                stack.append(c)
            else:
                if stack[0] == 'H' and stack[1] == 'H' and stack[2] == 'H':
                    return -1
                if stack[0] == 'H' and stack[1] == '.' and stack[2] == 'H':
                    ans += 1
                    stack = []
                    continue
                if stack[0] == '.' and stack[1] == 'H' and stack[2] == 'H':
                    ans += 1
                    stack.pop(0)
                    stack.pop(0)
                    continue
                if stack[0] == '.' and stack[1] == '.' and stack[2] == 'H':
                    stack.pop(0)
                    continue
                if stack[0] == 'H' and stack[1] == '.' and stack[2] == '.':
                    stack.pop(0)
                    stack.pop(0)
                    ans += 1
                    continue
        if len(stack) >= 2 and stack[-2] == stack[-1] and stack[-1] == 'H':
            ans = -1
        print("ans: ", ans)
        return ans


s = Solution()
assert s.minimumBuckets("H..H") == 2, "test1"
