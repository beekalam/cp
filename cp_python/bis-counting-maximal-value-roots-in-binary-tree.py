# 2021-12-05
import collections
# class Tree:
#     def __init__(self, val, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def __init__(self):
        self.cache = {}
        self.maxes = {}
        self.ans = 0
    def max_sub_tree(self, r):
        if r in self.cache:
            return self.cache[r]
        if r.left is None and r.right is None:
            mx = r.val
            self.cache[r] = mx
            return mx
        if r.left and r.right is None:
            mx = max(r.val, self.max_sub_tree(r.left))
            self.cache[r] = mx
            return mx
        if r.right and r.left is None:
            mx = max(r.val, self.max_sub_tree(r.right))
            self.cache[r] = mx
            return mx
        if r.right and r.left:
            mx =  max(r.val, self.max_sub_tree(r.right), self.max_sub_tree(r.left))
            self.cache[r] = mx
            return mx

    def solve(self, root):
        self.max_sub_tree(root)
        q = collections.deque()

        q.append(root)
        ans = 0
        while q:
            root = q.popleft()
            if root and root.left:
                q.append(root.left)
            if root and  root.right:
                q.append(root.right)
            if root and root.val >= self.max_sub_tree(root) :
                ans += 1
        return ans
