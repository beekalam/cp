class DivideLoot:
    def verify(self, N, loot):
        true_res, false_res = "possible", "impossible"
        if len(loot) == 1:
            return true_res
        if len(loot) == N and all([loot[0] == i for i in loot]):
            return true_res

        l = sorted(loot)

        sum_arr = []
        i, j = 0, len(l) - 1
        if len(l) % 2 == 1:
            j = len(l) - 2
            sum_arr.append(l[-1])
        while i < j:
            sum_arr.append(l[i] + l[j])
            i += 1
            j -= 1

        if len(sum_arr) > 0 and all([sum_arr[0] == i for i in sum_arr]) and len(sum_arr) == N:
            return true_res

        return false_res
