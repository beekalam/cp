# @notsolved: 2021-11-30
n, k = list(map(int, input().split()))
points = []
for i in range(n):
    row = list(map(int, input().split()))
    points.append((i + 1, sum(row)))

points.sort(key=lambda x: x[1], reverse=True)
m = dict(points)
for i in range(1, n + 1):
    if i <= k:
        print("Yes")
        continue
    if m[i] + 300 >= m[k]:
        print("Yes")
    else:
        print("No")
