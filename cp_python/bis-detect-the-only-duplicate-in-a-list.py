# https://binarysearch.com/problems/Detect-the-Only-Duplicate-in-a-List
class Solution:
    def solve(self, nums):
        n = len(nums)
        return sum(nums) -  (((n-1) * n) / 2)
