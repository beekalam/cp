# @struggled: 2021-11-14
from typing import Optional
from collections import deque

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
from utils.LinkedList import linkedlist_from_list, linkedlist_to_list, ListNode


class Solution:
    def reverseEvenLengthGroups(self, head: Optional[ListNode]) -> Optional[ListNode]:
        q = deque()
        h = head
        group_size = 1
        while h:
            q.clear()
            i = group_size
            eg = h
            while i:
                q.append(eg.val)
                if eg.next:
                    eg = eg.next
                else:
                    break
                i -= 1
            if len(q) % 2 == 0:
                while q:
                    h.val = q.pop()
                    h = h.next
            else:
                if h == eg:
                    h = eg.next
                else:
                    h = eg
            group_size += 1
        return head


s = Solution()

nodes = linkedlist_from_list([2, 1])
res = s.reverseEvenLengthGroups(nodes)
res = linkedlist_to_list(res)
assert res == [2, 1], "test1"

nodes = linkedlist_from_list([5, 2, 6, 3, 9, 1, 7, 3, 8, 4])
res = s.reverseEvenLengthGroups(nodes)
res = linkedlist_to_list(res)
# print(res)
assert res == [5, 6, 2, 3, 9, 1, 4, 8, 3, 7], "test2"

nodes = linkedlist_from_list([])
res = s.reverseEvenLengthGroups(nodes)
res = linkedlist_to_list(res)
assert res == [], "test3"
#
nodes = linkedlist_from_list([0, 4, 2, 1, 3])
res = s.reverseEvenLengthGroups(nodes)
res = linkedlist_to_list(res)
assert res == [0, 2, 4, 3, 1], "test4"
