# 2021-11-20
import collections
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def tolist(self, root: Optional[TreeNode]) -> List[int]:
        arr = []
        q = collections.deque()
        q.append(root)
        while q:
            t = q.popleft()
            arr.append(t.val)
            if t.right:
                q.append(t.right)
            if t.left:
                q.append(t.left)
        return arr
    def getMinimumDifference(self, root: Optional[TreeNode]) -> int:
        arr = self.tolist(root)
        arr.sort()
        ans = 10 ** 4
        for i in range(1,len(arr)):
            ans = min(ans, abs(arr[i] - arr[i-1]))
        return ans
