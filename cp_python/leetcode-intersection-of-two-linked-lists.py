# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def getIntersectionNode(self, headA, headB):
        """
        :type head1, head1: ListNode
        :rtype: ListNode
        """
        tmpa,tmpb,na,nb = headA,headB,0,0
        while tmpa:
            na += 1
            tmpa = tmpa.next
        while tmpb:
            nb += 1
            tmpb = tmpb.next
        if na > nb:
            diff = na - nb
            while diff:
                headA = headA.next
                diff -= 1
        if nb > na:
            diff = nb - na
            while diff:
                headB = headB.next
                diff -= 1
        while headA and headB:
            if headA.next and headB.next and headA.next == headB.next:
                return headA.next
