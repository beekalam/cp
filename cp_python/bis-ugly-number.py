def is_prime(n):
    """
    Assumes that n is a positive natural number
    """
    # We know 1 is not a prime number
    if n == 1:
        return False

    i = 2
    # This will loop from 2 to int(sqrt(x))
    while i * i <= n:
        # Check if i divides x without leaving a remainder
        if n % i == 0:
            # This means that n has a factor in between 2 and sqrt(n)
            # So it is not a prime number
            return False
        i += 1
    # If we did not find any factor in the above loop,
    # then n is a prime number
    return True


def prime_factors(n):
    factors = []
    if n % 2 == 0:
        factors.append(2)
    for i in range(3, n // 2):
        if i % 2 != 0 and n % i == 0 and is_prime(i):
            factors.append(i)
            print(len(factors))
    return factors


class Solution:
    def solve(self, n):
        if n == 0:
            return False
        if n in [2, 3, 5]:
            return True
        factors = prime_factors(n)
        if any([i in [2, 3, 5] for i in factors]):
            for i in [2, 3, 5]:
                if i in factors:
                    factors.remove(i)
        return len(factors) == 0


s = Solution()
print(s.solve(2 ** 31))
