# 2021-11-12
from typing import Optional


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def insertIntoBST(self, root: Optional[TreeNode], val: int) -> Optional[TreeNode]:
        if root is None:
            root = TreeNode(val)
            return root
        tmp = root
        while tmp:
            if val > tmp.val:
                if tmp.right:
                    tmp = tmp.right
                else:
                    tmp.right = TreeNode(val)
                    break
            elif val < tmp.val:
                if tmp.left:
                    tmp = tmp.left
                else:
                    tmp.left = TreeNode(val)
                    break
        return root
