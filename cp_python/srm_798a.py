import collections
import unittest


class SyllableCountEstimator:
    def is_vowel(self, c):
        return c in ['a', 'e', 'i', 'o', 'u']

    def is_vowel_group(self, c):
        return c in ['au', 'oa', 'oo', 'iou']

    def estimate(self, w):
        ans = 0
        for i in range(0, len(w)):
            if self.is_vowel(w[i]):
                ans += 1
            if i + 2 < len(w) and self.is_vowel_group(w[i:i + 2]):
                ans -= 1
            if i + 3 < len(w) and self.is_vowel_group(w[i:i + 3]):
                ans -= 1

        if w[-1] == 'e':
            ans -= 1
        if len(w) >= 3 and w[-2:] == 'le' and not self.is_vowel(w[-3]):
            ans += 1

        return 1 if ans < 1 else ans


class TestDotaSenate(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = SyllableCountEstimator()

    def test1(self):
        self.assertEqual(3, self.sol.estimate("potato"))

    def test2(self):
        self.assertEqual(1, self.sol.estimate("hol"))

    def test3(self):
        self.assertEqual(1, self.sol.estimate("gooooooal"))

    def test4(self):
        self.assertEqual(1, self.sol.estimate("rhythm"))

    def test5(self):
        self.assertEqual(1, self.sol.estimate("e"))

    def test6(self):
        self.assertEqual(1, self.sol.estimate("le"))

    def test7(self):
        self.assertEqual(3, self.sol.estimate("various"))

    def test8(self):
        self.assertEqual(4, self.sol.estimate("queued"))

    def test9(self):
        self.assertEqual(5, self.sol.estimate("qwertyuiopasdfghjkl"))

    def test10(self):
        self.assertEqual(4, self.sol.estimate("participle"))

    def test11(self):
        self.assertEqual(3, self.sol.estimate("ukulele"))
