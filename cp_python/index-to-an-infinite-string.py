class Solution:
    def solve(self, s, i, j):
        n = len(s)
        ans = ""
        j = j+ 1
        if (j - i) > n:
            while i % n != 0:
                ans += s[i]
                i += 1

            if (j - i) > n:
                # print("...", ans)
                # print((j - i) // n)
                # print("...", s * ((j - i) // n))
                ans += s * ((j - i) // n)
                j = (j - i) % 2
                print("ans: ", ans)
                print("j: ", j)
            while j > 0:
                ans += s[j % n]
                j -= 1
            return ans[:-1]
        return s[i % n:j % n]


s = Solution()
print(s.solve("hi", 2, 6))
# assert s.solve("hi", 2, 6) == "hihi", "wrong"
#
print(s.solve(s="YGY", i=1002, j=1010))
