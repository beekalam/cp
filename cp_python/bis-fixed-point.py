# 2021-11-18
class Solution:
    def solve(self, nums):
        L, U = 0, len(nums) - 1
        ans = -1
        while L <= U:
            M = (L + U) // 2
            if nums[M] < M:
                L = M + 1
            elif nums[M] == M:
                ans = M
                U = M - 1
            else:
                U = M - 1
        return ans


s = Solution()
nums = [-5, -2, 0, 3, 4]
assert s.solve(nums) == 3, "test1"

nums = [0, 1, 2]
assert s.solve(nums) == 0, "test2"
