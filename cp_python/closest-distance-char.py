import unittest
class Solution:
    def rfind(self, s, c, _from):
        i = _from
        while i >= 0:
            if s[i] == c:
                return i
            i = i - 1
        return -1

    def solve(self, s, c):
        n = len(s)
        ans = [n] * n
        for k in range(0, n):
            if s[k] == c:
                ans[k] = 0
            else:
                idx = s.find(c, k + 1)
                ridx = self.rfind(s, c, k - 1)
                if idx != -1 and ridx != -1:
                    ans[k] = min(idx - k, k - ridx)
                elif idx != -1:
                    ans[k] = min(ans[k], idx - k)
                elif ridx != -1:
                    ans[k] = min(ans[k], k - ridx)
        return ans

# s = Solution()
# ans = s.solve("aabaab", "b")
# ans = s.solve("baabaa", "b")
# print(ans)
# s.solve("ce", "c")
