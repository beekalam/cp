import math

class Solution_2:
    def solve(self, nums):
        n = len(nums)
        m = 10000
        j = 0

        if n < 2:
            return 0

        for i in range(n - 2, -1, -1):
            # print("i : {}".format(i))
            # if nums[i] > nums[i + 1]:
            j = i
            while j >= 0 and nums[j] > nums[j + 1]:
                j -= 1
            print("i: {}, j: {}".format(i, j))
            # m = min(m, abs((i + 1) - j))
            if j != i:
                m = abs((i + 1) - j)
            # if j == 0:
            #     m += 1
            print("m: {}".format(m))

        return 0 if m == 10000 else m

class Solution_x:
    def solve(self,nums):
        n = len(nums)
        j = 0
        _max = nums[0]
        ans = 0
        for i in range(1, len(nums)):
            num = nums[i]
            if num > _max:
               _max = num 
            if num < _max:
                ans += 1
        if ans > 0:
            ans += 1
        return ans
        # for i in range(n):
        #     # if i + 1  < n and nums[i+1] < nums[i]:
        #     #     j = j+ 1
        #     j = i
        #     print("i: {}, j: {}".format(i,j))
        #     while j < n-1 and nums[j+1] < nums[j]:
        #         j += 1
        #     ans = i - j
        #     print("ans: {}".format(ans))
        # return ans

class Solution:
    def solve(self, nums):
        if not nums:
            return 0
        left_max = nums[0]
        n = len(nums)
        end = -1
        for i in range(1, n):
            if nums[i] < left_max:
                end = i
            left_max = max(left_max, nums[i])
        if end == -1:
            return 0

        right_min = nums[-1]
        start = -1
        for i in range(n - 2, -1, -1):
            if nums[i] > right_min:
                start = i
            right_min = min(right_min, nums[i])

        print("end: {}, start: {}".format(end,start))
        return end - start + 1

s = Solution()
nums = [0, 1, 4, 3, 8, 9]
s.solve(nums)

# print('........................ 0')
# nums = [0, 0]
# print(s.solve(nums))


# print('======================  3')
# nums = [1,2,0]
# print(s.solve(nums))

# print('---------------- 2')
# nums = [0,1,0]
# print(s.solve(nums))
