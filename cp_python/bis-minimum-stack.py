# 2021-11-15
import collections
class MinimumStack:
    ElementWithCachedMin = collections.namedtuple('ElementWithCachedMin', ('element','min'))
    def __init__(self):
        self._items = []


    def append(self, val):
        self._items.append(
            self.ElementWithCachedMin(val, val if self.is_empty() else min(self.min(),val)))

    def is_empty(self):
        return len(self._items) == 0

    def peek(self):
        return self._items[-1].element


    def min(self):
        return self._items[-1].min


    def pop(self):
        l = self._items.pop()
        return l.element
