class Solution(object):
    def bruteForce(self,arr):
        ans = sum(arr)
        n = len(arr)

        for i in range(0,n):
            for j in range(i+1,n):
                if (j+1-i) % 2== 1:
                    # print(arr[i:j+1])
                    ans += sum(arr[i:j+1])
        # print("ans",ans)
        return ans
    def sumOddLengthSubarrays(self, arr):
        """
        :type arr: List[int]
        :rtype: int
        """
        return self.bruteForce(arr)

s = Solution()
# s.sumOddLengthSubarrays([1,4,2,5,3]) == 58
def test_1():
    assert s.sumOddLengthSubarrays([1,4,2,5,3]) == 58

def test_2():
    assert s.sumOddLengthSubarrays([1,2]) == 3

def test_3():
    assert s.sumOddLengthSubarrays([10,11,12]) == 66

def test_3():
    assert s.sumOddLengthSubarrays([10,11,12]) == 66
