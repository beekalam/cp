# 2021-11-11
class Solution(object):
    def intersection(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        ans = []
        nums1.sort()
        nums2.sort()
        m,n = len(nums1), len(nums2)
        i,j = 0,0
        while i < m and j <n:
            if nums1[i] > nums2[j]:
                j += 1
            elif nums2[j] > nums1[i]:
                i += 1
            elif nums1[i] == nums2[j]:
                if len(ans) == 0:
                    ans.append(nums1[i])
                elif ans[-1] != nums1[i]:
                    ans.append(nums1[i])
                i += 1
                j += 1
        return ans
        #return list(set(nums1).intersection(nums2))
