# 2021-11-13
import heapq


class Solution:
    def sol1(self, a, b, c):
        lst = [a, b, c]

        ans = 0
        while True:
            lst.sort(reverse=True)
            if lst[1] == 0 and lst[2] == 0:
                break
            lst[0] -= 1
            lst[1] -= 1
            ans += 1
        return ans

    def maximumScore(self, a: int, b: int, c: int) -> int:
        lst = [-a, -b, -c]
        heapq.heapify(lst)
        ans = 0
        while len(lst) >= 2:
            i, j = heapq.heappop(lst) * -1, heapq.heappop(lst) * -1
            i -= 1
            j -= 1
            ans += 1
            if i != 0:
                heapq.heappush(lst, i * -1)
            if j != 0:
                heapq.heappush(lst, j * -1)
        return ans


s = Solution()
assert s.maximumScore(2, 4, 6) == 6, "test1"
