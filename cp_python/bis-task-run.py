class Solution:
    def solve(self, tasks, k):
        i, n = 0, len(tasks)
        if n <= 1:
            return n
        if k == 0:
            return n
        last_seen = {}
        ans = 0
        while i < n:
            task = tasks[i]
            if task in last_seen:
                if (ans - last_seen[task]) <= k:
                    ans += k - (ans - last_seen[task])
                    # ans += 1
                    # continue
                    last_seen[task] = ans
                else:
                    last_seen[task] = ans + 1
                    ans += 1
            else:
                last_seen[task] = ans + 1
                ans += 1
            i += 1
        ans = ans
        print("ans: {}".format(ans))
        return ans


s = Solution()
assert s.solve([0, 1, 0, 1], k=2) == 5, "test1"
assert s.solve([0, 1], k=0) == 2, "test2"
assert s.solve([0, 0], k=1) == 3, "test3"
assert s.solve([0, 1], k=3) == 2, "test3"
# assert s.solve([0, 0], k= 268435458) == 2, "test3"
print("..........................")