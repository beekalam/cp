# 2021-11-27
s1 = input()
s2 = input()
s = [list(s1), list(s2)]
invalids = [[list("#."), list(".#")], [list(".#"), list('#.')]]

if s in invalids:
    print('No')
else:
    print('Yes')
