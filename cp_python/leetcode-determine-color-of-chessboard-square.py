class Solution:
    def squareIsWhite(self, coordinates: str) -> bool:
        m = {
            "a":1,
            "b":2,
            "c":3,
            "d":4,
            "e":5,
            "f":6,
            "g":7,
            "h":8
        }
        colors = {
            (1,1):'B',
            (1,0):'W',
            (0,1):'W',
            (0,0):'B'
        }
        r = m[coordinates[0]]
        c = int(coordinates[1])

        color = colors[(r%2,c%2)]
        return color == 'W'
