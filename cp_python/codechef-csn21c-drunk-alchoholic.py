t = int(input())
for i in range(t):
    n = int(input())
    if n % 2 == 0:
        print((n//2) * 2)
        ans = (n//2) * 2
    else:
        ans = ((n//2) * 2) + 3
    print(ans)
