# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, node):
        root = node
        while root is not None:
            if root.val > 0:
                next_node = root.next
                v = root.val - 1
                while next_node and v > 0:
                    v -= 1
                    next_node = next_node.next
                root.next = next_node
            root = root.next
        return node
