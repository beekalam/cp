import re
from typing import List


class Solution:
    def search(self, nums: List[int], target: int) -> int:
        low = 0
        high = len(nums) - 1
        mid = (low + high) // 2
        while True:
            print("mid: ", mid, f" mid[{mid}]: ", nums[mid])

            if nums[mid] == target:
                return mid

            if mid == 0 or mid == len(nums) - 1:
                return -1

            if mid - 1 >= 0 and nums[mid - 1] > nums[mid]:
                if target > nums[mid]:
                    high = mid
                else:
                    low = mid

                mid = (low + high) // 2
            else:
                if target > nums[mid]:
                    low = mid
                else:
                    high = mid

                mid = (low + high) // 2


s = Solution()
# print(s.search([4, 5, 6, 7, 0, 1, 2], target=0))
# assert s.search([4, 5, 6, 7, 0, 1, 2], target=0) == 4, "test1"
# assert s.search(nums=[4, 5, 6, 7, 0, 1, 2], target=3) == -1, "test2"
# assert s.search(nums=[1], target=0) == -1, "test3"
