from functools import lru_cache


def max_profit(daily_price):
    @lru_cache(maxsize=None)
    def get_best_profit(day, have_stock):
        """
        Returns the best profit that can be obtained by the end of the day.
        At the end of the day:
        * if have_stock is True, the trader must own the stock;
        * if have_stock is False, the trader must not own the stock.
        """
        if day < 0:
            if not have_stock:
                # Initial state: no stock and no profit.
                return 0
            else:
                # We are not allowed to have initial stock.
                # Add a very large penalty to eliminate this option.
                return -float('inf')
        price = daily_price[day]
        if have_stock:
            # We can reach this state by buying or holding.
            strategy_buy = get_best_profit(day - 1, False) - price
            strategy_hold = get_best_profit(day - 1, True)
            return max(strategy_buy, strategy_hold)
        else:
            # We can reach this state by selling or avoiding.
            strategy_sell = get_best_profit(day - 1, True) + price
            strategy_avoid = get_best_profit(day - 1, False)
            return max(strategy_sell, strategy_avoid)

    # Final state: end of last day, no shares owned.
    last_day = len(daily_price) - 1
    no_stock = False
    return get_best_profit(last_day, no_stock)


prices = [7, 1, 5, 3, 6, 4]
print(max_profit(prices))
