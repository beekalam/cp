import inspect
from functools import lru_cache


def stack_depth():
    return len(inspect.getouterframes(inspect.currentframe())) - 1


def fibonacci_brute_force(n):
    print("{indent}fibonacci({n}) called".format(
        indent=" " * stack_depth(), n=n))
    if n <= 2:
        return 1
    return fibonacci_brute_force(n - 1) + fibonacci_brute_force(n - 2)


@lru_cache(maxsize=None)
def fibonacci_dp_top_down(n):
    if n <= 2:
        return 1
    return fibonacci_dp_top_down(n - 1) + fibonacci_dp_top_down(n - 2)


def fibonacci_dp_bottom_up(n):
    previous = 1
    current = 1
    for i in range(n - 2):
        next = current + previous
        previous, current = current, next
    return current

# fibonacci_brute_force(6)
# print(fibonacci_dp_top_down(10))
