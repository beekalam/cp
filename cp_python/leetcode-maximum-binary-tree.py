# https://leetcode.com/problems/maximum-binary-tree/
# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution(object):
    def maxIndex(self, nums):
        maxIndex = 0
        for i in range(1, len(nums)):
            if nums[i] > nums[maxIndex]:
                maxIndex = i
        return maxIndex, nums[0:maxIndex], nums[maxIndex + 1:]

    def addNode(self, root, nums, addToLeft):
        if len(nums) == 0:
            return

        maxIndex, prefix, suffix = self.maxIndex(nums)
        node = TreeNode(nums[maxIndex])
        if addToLeft:
            root.left = node
        else:
            root.right = node

        if prefix:
            self.addNode(node, prefix, True)
        if suffix:
            self.addNode(node, suffix, False)

    def constructMaximumBinaryTree(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        maxIndex, prefix, suffix = self.maxIndex(nums)
        root = TreeNode(nums[maxIndex])
        self.addNode(root, prefix, True)
        self.addNode(root, suffix, False)
        return root


s = Solution()
print(s.constructMaximumBinaryTree([3, 2, 1, 6, 0, 5]))
