n = int(input())
nums = list(map(int, input().split()))
x = int(input())
sum = sum(nums)
# print("sum: {}".format(sum))
ans = (x // sum) * len(nums)
rem = x % sum

for n in nums:
    if rem > 0:
        rem -= n
        ans += 1
print(ans)

