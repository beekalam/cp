class Solution(object):
    def hasAlternatingBits(self, n):
        """
        :type n: int
        :rtype: bool
        """
        prev_bit = -1
        while n:
            bit = n & 1
            if prev_bit != -1 and prev_bit== bit:
                return False
            prev_bit = bit
            n >>= 1
        return True

s =Solution()
print(s.hasAlternatingBits(5))
print(s.hasAlternatingBits(7))
