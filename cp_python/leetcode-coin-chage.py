# @notsolved: 2021-12-08
import copy
from typing import List


class Solution:
    def __init__(self):
        self.mn = 0

    def do_solve(self, coins: List[int], amount: int, count: int):
        if amount == 0:
            self.mn = min(self.mn, count)
            return
        for idx, i in enumerate(coins):
            if i <= amount:
                c = copy.deepcopy(coins)
                c.pop(idx)
                self.do_solve(c, amount % i, count + (amount // i))
                self.do_solve(c, amount, count)

    def coinChange(self, coins: List[int], amount: int) -> int:
        self.mn = float('+inf')
        self.do_solve(sorted(coins, reverse=True), amount, 0)
        self.do_solve(sorted(coins), amount, 0)
        # print("ans; ", self.mn)
        if self.mn == float('+inf'):
            return -1
        # print("ans: ", self.mn)
        return self.mn


s = Solution()
# assert s.coinChange([1, 2, 5], 11) == 3, "test1"
# assert s.coinChange([2], 3) == -1, "test1"
# assert s.coinChange([1], 0) == 0, "test1"
# assert s.coinChange([1], 1) == 1, "test1"
# assert s.coinChange([1], 2) == 2, "test5"
nums = [186, 419, 83, 408]
amount = 6249
assert s.coinChange(nums, amount) == 2, "test6"
