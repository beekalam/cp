
import string
class Solution:
    # @param A : string
    # @return an integer
    def isPalindrome(self, A):
        A = A.lower().replace(" ","")
        for c in string.punctuation:
            A = A.replace(c,"")
        # print(A)
        return 1 if A[::-1] == A else 0

s = Solution()
print(s.isPalindrome("A man, a plan, a canal: Panama"))

