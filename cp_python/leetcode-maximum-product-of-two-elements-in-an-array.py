class Solution(object):
    def maxProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        max1 = min(nums[0], nums[1])
        max2 = max(nums[0], nums[1])
        for i in range(2, len(nums)):
            if nums[i] >= max2:
                max1, max2 = max2, nums[i]
            elif nums[i] >= max1:
                max1 = nums[i]
        # print(max1,max2)
        return (max1 - 1) * (max2 - 1)


s = Solution()
assert s.maxProduct([3, 4, 5, 2]) == 12, "test 1"
assert s.maxProduct([1, 5, 4, 5]) == 16, "test 2"
assert s.maxProduct([10, 2, 5, 2]) == 36, "test 3"
