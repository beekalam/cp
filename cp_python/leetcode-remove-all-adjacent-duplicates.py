class Solution(object):
    def removeDuplicates(self, s):
        """
        :type s: str
        :rtype: str
        """
        stack = []
        def remove_duplicates():
            if len(stack) >= 2 and stack[-1] == stack[-2]:
                stack.pop()
                stack.pop()

        for c in s:
            stack.append(c)
            remove_duplicates()
        remove_duplicates()

        return "".join(stack)
