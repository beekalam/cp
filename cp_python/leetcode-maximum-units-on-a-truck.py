class Solution(object):
    def maximumUnits(self, boxTypes, ts):
        """
        :type boxTypes: List[List[int]]
        :type truckSize: int
        :rtype: int
        """
        boxTypes.sort(key=lambda box:box[1],reverse=True)
        ans = 0
        print(boxTypes)
        for i,j in boxTypes:
            if i <= ts:
                ts = ts - i
                ans += (i * j)
            else:
                ans += (ts * j)
                ts = 0

            if ts == 0:
                break
        return ans

s = Solution()
print(s.maximumUnits([[3,1],[1,3],[2,2]], 4))
