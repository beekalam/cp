# https://leetcode.com/problems/remove-linked-list-elements/
# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution(object):
    def removeElements(self, head, val):
        """
        :type head: ListNode
        :type val: int
        :rtype: ListNode
        """
        dummy = ListNode()
        newhead = dummy
        tmp = head
        while tmp:
            while tmp and tmp.val == val:
                tmp = tmp.next
            if tmp:
                dummy.next = tmp
                dummy = dummy.next
                tmp = tmp.next
                dummy.next = None
        return newhead.next