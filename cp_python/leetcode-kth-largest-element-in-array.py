# 2021-11-14
import heapq

arr = [3,3,3,3,5,5,5,2,2,7]
class Solution(object):
    def findKthLargest(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        heapq.heapify(nums)
        ans = nums[-(k - 1)]
        # print(heapq.nlargest(k, nums))
        # print("..............................")
        return heapq.nlargest(k, nums)[k-1]


s = Solution()
nums = [3, 2, 1, 5, 6, 4]
assert s.findKthLargest(nums, 2) == 5

nums = [3, 2, 3, 1, 2, 4, 5, 5, 6]
k = 4
assert s.findKthLargest(nums, k)
