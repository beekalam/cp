# 2021-12-01
class RateLimiter:
    def __init__(self, expire):
        self.expire = expire
        self.last_requests = {}


    def limit(self, uid, timestamp):
        if uid in self.last_requests:
            if timestamp - self.last_requests[uid] >= self.expire:
                self.last_requests[uid] = timestamp
                return False
            else:
                return True
        else:
            self.last_requests[uid] = timestamp
            return False
