# 2021-11-12
import collections
from typing import Optional
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def recursive(self, root: Optional[TreeNode], val:int):
        def bst_helper(r:Optional[TreeNode]):
            if r is None:
                return None
            if r.val == val:
                return r
            right = bst_helper(r.right)
            if right:
                return right
            left = bst_helper(r.left)
            if left:
                return left
            return None
        return bst_helper(root)
    def searchBST(self, root: Optional[TreeNode], val: int) -> Optional[TreeNode]:
        # return self.recursive(root,val)
        q = collections.deque()
        q.append(root)
        while q:
            r = q.popleft()
            if r.val == val:
                return r
            if r.right:
                q.append(r.right)
            if r.left:
                q.append(r.left)
        return None
