# 2021-11-28
class Solution:
    def solve(self, n, m, k, inputs):
        idx = 0
        mat = []
        for i in range(n):
            row = []
            for j in range(m):
                row.append(set())
            mat.append(row)
        for r, c, l in inputs:
            idx += 1
            r, c = r - 1, c - 1
            for i in range(0, l):
                for j in range(0, l):
                    mat[r + i][c + j].add(idx)

        res = set()
        for i in range(n):
            for j in range(m):
                if mat[i][j]:
                    res.add(tuple(mat[i][j]))
        return len(res)

    def solve_cmd(self):
        n, m, k = list(map(int, input().split()))
        inputs = []
        for i in range(k):
            r, c, l = list(map(int, input().split()))
            inputs.append([r, c, l])
        print(self.solve(n, m, k, inputs))


s = Solution()
s.solve_cmd()
# s.solve(5, 5, 3, [[1, 1, 3], [2, 2, 4], [1, 3, 3]]) == 7, "test1"
