# 2021-12-26

from typing import List


class Solution:
    def brute_force(self, nums: List[int], k: int) -> float:
        mx = None
        for i in range(len(nums)):
            if i + k <= len(nums):
                avg = sum(nums[i:i + k]) / k
                if mx is None:
                    mx = avg
                else:
                    mx = max(avg, mx)
        print("mx: ", mx)
        return mx

    def findMaxAverage(self, nums: List[int], k: int) -> float:
        # return self.brute_force(nums, k)
        windowSum = 0
        windowStart = 0
        res = []
        for windowsEnd in range(len(nums)):
            windowSum += nums[windowsEnd]
            if windowsEnd >= k - 1:
                res.append(windowSum / k)
                windowSum -= nums[windowStart]
                windowStart += 1
        return max(res)


s = Solution()
assert s.findMaxAverage([1, 12, -5, -6, 50, 3], 4) == 12.75000, "test1"
assert s.findMaxAverage([-1], 1) == -1.000, "test2"
