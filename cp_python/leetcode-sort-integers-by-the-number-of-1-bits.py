from functools import cmp_to_key
class Solution(object):
    def sortByBits(self, arr):
        """
        :type arr: List[int]
        :rtype: List[int]
        """
        def count_bits(x):
            c = 0
            while x:
                c += x & 1
                x >>= 1
            return c
        def cmp_by_num_bits(a,b):
            diff =  count_bits(a) - count_bits(b)
            if diff == 0:
                return a - b
            return diff
        arr.sort(key=cmp_to_key(cmp_by_num_bits))
        return arr

s = Solution()
print(s.sortByBits(list(range(0,9))))
print(s.sortByBits([1024,512,256,128,64,32,16,8,4,2,1]))
