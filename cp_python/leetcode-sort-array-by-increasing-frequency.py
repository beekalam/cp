# 2021-11-10
import functools
import operator
from collections import Counter
from functools import cmp_to_key
class Solution(object):
    def frequencySort(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """

        c = Counter(nums)
        def sort_fn(a,b):
            if c[a] == c[b]:
                return b - a
            else:
                return c[a] - c[b]
        nums.sort(key=cmp_to_key(sort_fn))
        return nums
