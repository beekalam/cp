class PrimeCheck:
    def __init__(self):
        self.cache = {}
        self.max_checked = 2

    def is_prime(self,n):
        if n in self.cache:
            return True
        if n < self.max_checked:
            return False

        if n == 1:
            return False
        if n % 2 == 0:
            return False
        i = 2
        while i * i <= n:
            if n % i == 0:
                return False
            i += 1
        max_checked = max(n,self.max_checked)
        self.cache[n] = True
        return True

t = int(input())
for i in range(t):
    x,y = list(map(int, input().split()))
    ans = 0
    while x <= y:
        if x + 2 <= y and is_prime(x+2):
            x += 2
        else:
            x += 1
        ans += 1
    print(ans)

