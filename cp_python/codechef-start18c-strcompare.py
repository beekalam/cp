# @notsolved: 2021-11-22
t = int(input())
for _ in range(t):
    n = input()
    a = input()
    b = input()
    n = len(a)
    rs = 0
    ans = 0
    gt = 0
    for ac, bc in zip(a, b):
        if ac > bc:
            rs = 0
        elif ac == bc:
            rs += 1
        elif ac < bc:
            rs += 1
            ans += rs
    print(ans)
