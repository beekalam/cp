# 2021-12-20
from typing import List
class Solution :
    def sol1(self, nums: List[int]) -> int:
        n = len(nums)
        if n == 0:
            return 0
        mx = 1
        s = set()
        for i in nums:
            tmp = i
            count = 1
            while tmp+1 in s:
                tmp = tmp + 1
                count = count + 1
            tmp = i
            while tmp - 1 in s:
                tmp = tmp - 1
                count = count + 1
            s.add(i)
            mx = max(mx, count)
        return mx
    def longestConsecutive(self, nums: List[int]) -> int:
        return self.sol1(nums)
        # return self.sol2(nums)

    def sol2(self, nums: List[int]) -> int:
        n = len(nums)
        if n == 0:
            return 0
        nums.sort()
        mx = 1
        i = 0
        while i < n:
            j = i
            equals = 0
            while j + 1 < n and (nums[j] + 1 == nums[j+1] or nums[j] == nums[j+1]):
                if nums[j] == nums[j+1]:
                    equals += 1
                j += 1
            if j != i:
                mx = max(mx, (j-i+1) - equals)
                i = j
            else:
                i = i + 1
        return mx

s = Solution()
assert s.longestConsecutive([100, 4, 200, 1, 3,2]) ==  4, "test1"
