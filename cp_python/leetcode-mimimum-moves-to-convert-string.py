class Solution(object):
    def minimumMoves(self, s):
        ans, i, n = 0, 0, len(s)
        while i < n:
            # print("in while")
            if s[i] == 'X':
                ans += 1
                for j in range(1, 4):
                    # print('in for')
                    if i + 1 <= n:
                        i += 1
                    else:
                        break
            else:
                i += 1
        return ans



s = Solution()

# print(s.minimumMoves("XXX"))
# print(s.minimumMoves("XXOX"))
# print(s.minimumMoves("OOOO"))
print(s.minimumMoves("OXOX"))

