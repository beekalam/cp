# 2021-12-16
class Solution:
    def solve(self, s):
        lc = s.count('L')
        rc = s.count('R')
        qc = s.count('?')
        diff = abs(lc - rc)
        return diff + qc

s = Solution()
assert s.solve("LLRRR??") == 3, "test1"

