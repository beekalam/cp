# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, head):
        while head.next:
            if head.val >= head.next.val:
                return False
            head = head.next
        return True
