# 2021-12-26
from typing import List


class Solution:
    def Solution1(self, target: int, nums: List[int]) -> int:
        windowSum = 0
        minLength = float('inf')
        windowStart = 0
        windowEnd = 0
        while windowEnd < len(nums):
            windowSum += nums[windowEnd]
            while windowSum >= target:
                minLength = min(minLength, windowEnd - windowStart + 1)
                windowSum -= nums[windowStart]
                windowStart += 1
            windowEnd += 1

        if minLength == float('inf'):
            return 0
        return minLength

    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        return self.Solution1(target, nums)
        wsum = 0
        ws = 0
        mn = float('inf')
        we = 0
        while we < len(nums):
            wsum += nums[we]
            if wsum >= target:
                if wsum == target:
                    mn = min(we - ws, mn)
                wsum -= nums[ws]
                if wsum == target:
                    mn = min(we - ws, mn)
                ws += 1
            we += 1
        while ws < len(nums):
            wsum -= nums[ws]
            ws += 1
            if wsum == target:
                mn = min(we - ws, mn)
        print(mn)
        return mn if mn != float('inf') else 0


s = Solution()
assert s.minSubArrayLen(7, [2, 3, 1, 2, 4, 3]) == 2, "test1"
