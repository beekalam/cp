# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution(object):
    def mergeInBetween(self, list1, a, b, list2):
        """
        :type list1: ListNode
        :type a: int
        :type b: int
        :type list2: ListNode
        :rtype: ListNode
        """
        dummy = ListNode()
        dummy.next = list1
        idx = 0
        current_node = dummy
        tmp = None
        while current_node:
            if idx + 1 == a:
                tmp = current_node.next
                current_node.next = list2
                current_node = current_node.next
                break
            else:
                idx += 1
                current_node = current_node.next

        while tmp:
            if idx == b:
                break
            else:
                idx += 1
                tmp = tmp.next

        while current_node.next:
            current_node = current_node.next
        if tmp:
            current_node.next = tmp
        return dummy.next
