# 2021-12-24
n = int(input())
lst = list(map(int, input().split()))
s = sum(lst)
cs = 0
mn = sum(lst)
mn_idx = 0
for i in range(len(lst)):
    cs += lst[i]
    v = abs((s - cs) - (s - (s - cs)))
    if v < mn:
        mn = v
        mn_idx = i + 1

print(mn)
