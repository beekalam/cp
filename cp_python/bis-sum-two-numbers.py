from collections import Counter


class Solution:
    def solve(self, nums, k):
        c = Counter(nums)
        for num in c:
            rem = k - num
            # print("rem: ", rem, " c[rem] ", c[rem])
            if rem in c:
                if rem == num:
                    if c[rem] > 1:
                        return True
                else:
                    return True
        return False


s = Solution()
nums = [35, 8, 18, 3, 22]
# print(s.solve(nums,11))
print(s.solve([22], 44))
