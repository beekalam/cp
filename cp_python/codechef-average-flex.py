t = int(input())
for i in range(t):
    n = int(input())
    students = list(map(int, input().split()))
    students.sort()
    if n == 1:
        print(1)
    else:
        ans = 0
        for j in range(n):
            l = g = 0
            for k in range(n):
                if j == k:
                    continue
                if students[k] <= students[j]:
                    l += 1
                else:
                    g += 1
            if l >= g:
                ans += 1
        print(ans)
