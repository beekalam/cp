# https://leetcode.com/problems/minimum-number-of-steps-to-make-two-strings-anagram/
from collections import Counter
class Solution(object):
    def minSteps(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: int
        """
        cs = Counter(s)
        ct = Counter(t)
        ans = 0
        for i in cs:
            if i in ct and cs[i] > ct[i]:
                ans += cs[i] - ct[i]
            if i not in ct:
                 ans += cs[i]
        return ans


s = Solution()
assert s.minSteps("bab","aba") == 1
assert s.minSteps(s = "leetcode", t = "practice") == 5
assert s.minSteps(s = "anagram", t = "mangaar") == 0
assert s.minSteps(s = "xxyyzz", t = "xxyyzz") == 0
assert s.minSteps(s = "friend", t = "family") == 4

