#https://leetcode.com/problems/lucky-numbers-in-a-matrix/submissions/
class Solution(object):
    def luckyNumbers (self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        mr,mc = [],[]
        m,n = len(matrix),len(matrix[0])
        for row in matrix:
            mr.append(min(row))

        for i in range(n):
            col = []
            for j in range(m):
                col.append(matrix[j][i])
            mc.append(max(col))

        for i in range(m):
            for j in range(n):
                val = matrix[i][j]
                if val == mr[i] and val == mc[j]:
                    return [val]
        return []
