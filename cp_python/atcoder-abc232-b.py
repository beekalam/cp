import string

s = input()
t = input()


diff = abs(string.ascii_lowercase.index(s[0]) - string.ascii_lowercase.index(t[0]))
m = {}
for i, c in enumerate(string.ascii_lowercase):
    m[c] = string.ascii_lowercase[(i + diff) % 25]

ans = "Yes"
for a, b in zip(s, t):
    if m[a] != b:
        ans = "No"
        break
print(ans)