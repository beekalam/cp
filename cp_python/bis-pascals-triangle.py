# 2021-11-29
class Solution:
    def solve(self, n):
        tri = [[1],[1,1]]
        i = 2
        while i <= n:
            lr = tri[-1]
            nr = [1]
            for j in range(1,len(lr)):
                nr.append(lr[j] + lr[j-1])
            nr.append(1)
            tri.append(nr)
            i += 1
        return tri[n]
