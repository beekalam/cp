class Nodes:
    def __init__(self):
        self.edges = []

    def add(self, i, j):
        self.edges.append((i, j))

    def has_path_to(self, i, j):
        return (i, j) in self.edges

    def __repr__(self) -> str:
        return "edges: {}".format(self.edges)


class Graph:
    def __init__(self):
        self.nodes = {}

    def add(self, vertex, edges):
        self.nodes[vertex] = edges



class Solution:
    def __init__(self):
        self.map = {}

    def upsert(self, k, v):
        if k not in self.map:
            self.map[k] = Nodes()
        self.map[k].add(v[0], v[1])

    def solve(self, matrix):
        m, n = len(matrix), len(matrix[0])
        neighbours = [(i, j) for i in [1, -1] for j in [1, -1]]
        for r in range(m):
            for c in range(n):
                if matrix[r][c] == 0:
                    for i, j in neighbours:
                        a, b = r + i, c + j
                        if 0 <= a < m and 0 <= b < n and matrix[a][b] == 0:
                            self.upsert((r, c), (a, b))
        print(self.map)


s = Solution()
matrix = [
    [0, 1, 0],
    [0, 0, 1],
    [0, 0, 0]
]
s.solve(matrix)
