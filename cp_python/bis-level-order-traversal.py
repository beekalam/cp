# 2022-01-09
import collections
# class Tree:
#     def __init__(self, val, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def solve(self, root):
        q = collections.deque()
        q.append(root)
        res = []
        while q:
            r = q.popleft()
            res.append(r.val)
            if r.left:
                q.append(r.left)
            if r.right:
                q.append(r.right)

        return res
