class Solution:
    def solve(self, nums):
        n = len(nums)
        m = {}
        for i in reversed(range(n)):
            if nums[i] not in m:
                m[nums[i]] = [i,1]
            else:
                idx,count = m[nums[i]]
                count += 1
                m[nums[i]] = [idx, count]
        for k,v in m.items():
            idx,count = v
            if count > 1:
                nums.pop(idx)
        return nums
s=Solution()
assert s.solve([1,3,4,1,3,5])== [1, 3, 4, 5], "test1"
assert s.solve([1, 1, 1, 2, 2, 2, 3, 3, 3]) == [1, 1, 2, 2, 3, 3], "test 2"
