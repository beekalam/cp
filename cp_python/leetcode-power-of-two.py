# https://leetcode.com/problems/power-of-two/
import math
class Solution(object):
    def bruteForce(self, n):
        i = 0
        while True:
            p = pow(2,i)
            if p > n:
                return False
            if p == n:
                return True
            i += 1

    def isPowerOfTwo(self, n):
        """
        :type n: int
        :rtype: bool
        """
        if n == 0:
            return False
        return n & (n-1) == 0


s = Solution()
print(s.isPowerOfTwo(3))
