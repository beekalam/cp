from collections import defaultdict
class Solution:
    def twoOutOfThree(self, nums1: List[int], nums2: List[int], nums3: List[int]) -> List[int]:
        m = defaultdict(int)
        for num in [nums1, nums2,nums3]:
            for n in set(num):
                m[n] += 1
        return [k for k,v in m.items() if v >= 2]
