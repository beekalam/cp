# 2021-12-24
lst = list(map(int, input().split()))
mn = sum(lst)
for i in range(len(lst)):
    for j in range(len(lst)):
        if i != j:
            mn = min(mn, lst[i]+lst[j])
print(mn)
