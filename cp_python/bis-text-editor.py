class Solution:
    def solve(self, s):
        ans = []
        i = 0
        while i < len(s):
            if i+1 < len(s) and s[i] == '<' and s[i+1] == '-':
                i += 1
                if ans:
                    ans.pop()
            else:
                ans.append(s[i])
            i += 1
        return "".join(ans)
