# https://leetcode.com/problems/arithmetic-subarrays/submissions/
class Solution(object):
    def checkArithmeticSubarrays(self, nums, l, r):
        """
        :type nums: List[int]
        :type l: List[int]
        :type r: List[int]
        :rtype: List[bool]
        """
        def is_valid(arr):
            n = len(arr)
            if n < 2:
                return False
            arr.sort()
            diff = arr[1] - arr[0]
            return all(arr[i+1] - arr[i] == diff for i in range(n-1))

        ans = []
        for i,j in zip(l,r):
            ans.append(is_valid(nums[i:j+1]))
        return ans
