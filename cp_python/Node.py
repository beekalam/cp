class Node:
    def __init__(self, val, left=None, right=None):
        """
        :type left: Node
        :type right: Node
        """
        self.val = val
        self.left = left
        self.right = right
