(defun simplified-beginning-of-buffer ()
  "Move point tot he beginnig of the buffer. leave mark at the previous position"
  (interactive)
  (push-mark)
  (goto-char (point-min)))

(process-list)
(make-process :name "shell" :command (list "bash"))

(start-process "shell2" "foo" "-l" "/bin/bash")

(make-process :name "shell" :command (list "bash") :buffer (get-buffer "*scratch*"))
(print (process-send-string "shell" "ls -lh\n"))
