# 2021-10-28

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution(object):
    def getDecimalValue(self, head):
        """
        :type head: ListNode
        :rtype: int
        """
        A =[]
        while head:
            A.insert(0,head.val)
            head = head.next
        return sum(A[i] * (2 ** i) for i in range(len(A)))
