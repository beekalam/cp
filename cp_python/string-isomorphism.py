import itertools


class Solution:
    def solve2(self, s, t):
        seen = []
        for c in set(s):
            found_any = False
            for cc in set(t):
                if cc not in seen and t.count(cc) == s.count(c):
                    seen.append(cc)
                    found_any = True
                    print("c: {} cc: {}".format(c, cc))
                    break
            if not found_any:
                return False
        print(seen)
        return len(seen) == len(set(t))

    def solve3(self, s, t):
        ms, mt = {}, {}
        for k, grp in itertools.groupby(s):
            ms[k] = len(list(grp))
        for k, grp in itertools.groupby(t):
            mt[k] = len(list(grp))

        for k, v in ms.items():
            print("k: {}, v: {}".format(k, v))
            has_condition = k in mt.keys() and mt[k] == v
            if not has_condition:
                return False
        return False

    def solve(self, s, t):
        m, seen = {}, []
        for i, c in enumerate(s):
            if c not in seen:
                if s.count(c) == t.count(t[i]):
                    seen.append(c)
                    m[c] = t[i]
                else:
                    return False
            else:
                if m[c] != t[i]:
                    return False

        return True


s = Solution()
print(s.solve("coco", "kaka"))
# ss = "coco"
# tt = "kaka"

print(s.solve("anomura", "mullein"))
