# @notsolved: 2021-11-11
import time
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
from typing import Optional, List

from utils.LinkedList import ListNode, linkedlist_from_list


class Solution:
    def nextLargerNodes(self, head: Optional[ListNode]) -> List[int]:
        res, stack = [], []
        while head:
            while stack and stack[-1][1] < head.val:
                res[stack.pop()[0]] = head.val
            stack.append([len(res), head.val])
            res.append(0)
            head = head.next
        return res


s = Solution()
head = linkedlist_from_list([2, 1, 5])
assert s.nextLargerNodes(head) == [5, 5, 0]

# head = linkedlist_from_list([2, 7, 4, 3, 5])
# assert s.nextLargerNodes(head) == [7, 0, 5, 5, 0]
