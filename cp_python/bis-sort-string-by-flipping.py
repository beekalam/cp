class Solution:
    def solve(self, s):
        xc = s.count('x')
        rx = xc
        lx = 0
        ly = 0
        ans = len(s)
        for c in s:
            if c == 'x':
                lx += 1
                rx = xc - lx
                ans = min(ans,rx+ly)
            if c == 'y':
                ans = min(ans, rx+ly)
                ly += 1
        return ans
s = Solution()
# print(s.solve("yx"))
assert s.solve("xy") == 0, "test 1 failed"
assert s.solve("xxy") == 0, "test 2 failed"
assert s.solve("xyxxxyxyy") == 2, "test 3 failed"
assert s.solve("xyxyy") == 1, "test 4 failed"
assert s.solve("yx") == 1, "test 4 failed"

