# 2021-12-29
class Solution:
    def solve(self, nums):
        n = len(nums)
        for i in range(0,n-2,4):
            nums[i],nums[i+2] = nums[i+2],nums[i]
        for i in range(1,n-2,4):
            nums[i],nums[i+2] = nums[i+2],nums[i]
        return nums
