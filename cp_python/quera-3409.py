# 2021-12-21
# https://quera.ir/problemset/3409/
n = int(input())
table = [[str(i*j) for i in range(1, n+1)]
             for j in range(1, n+1)]
# print(table)
for row in table:
    print(" ".join(row))
