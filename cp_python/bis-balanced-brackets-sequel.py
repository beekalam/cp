class Solution:
    def solve(self, s):
        brackets = {
            "]": "[",
            "}": "{",
            ")": "("
        }
        stack = []
        for i, c in enumerate(s):
            # opening brackets
            if c in brackets.values():
                stack.append(c)
            else:  # closing brackets
                if len(stack) == 0:
                    return False
                elif stack[-1] != brackets[c]:
                    return False
                else:
                    stack.pop()

        if len(stack) != 0:
            return False

        return True


s = Solution()
ss = "[(])"
s.solve(ss)
