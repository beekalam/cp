class Solution:
    def solve(self, nums):
        nums.sort(reverse=True)
        return nums[0] > nums[1] * 2


s = Solution()
print(s.solve([3, 6, 15]))
