class Solution:
    def generateTheString(self, n: int) -> str:
        if n % 2 == 0:
            return "a" + self.generateTheString(n-1)
        else:
            return "b" * n
