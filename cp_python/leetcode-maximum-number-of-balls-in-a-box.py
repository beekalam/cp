# 2021-11-15
import collections


class Solution:
    def countBalls(self, lowLimit: int, highLimit: int) -> int:
        dic = collections.defaultdict(int)
        i = lowLimit
        while i <= highLimit:
            s = sum(int(c) for c in str(i))
            dic[s] += 1
            i += 1
        ans = max(dic.values())
        return ans


s = Solution()
assert s.countBalls(1, 10) == 2, "test1"

# dic = {1:1,10:3}
# ans = max(dic, key=lambda key: dic[key])
