import heapq
import math
from io import StringIO
data = [19, 9, 4, 10, 11]

def show_tree(tree, total_width=36, fill=' '):
    """Pretty-print a tree."""
    output = StringIO()
    last_row = -1
    for i, n in enumerate(tree):
        if i:
            row = int(math.floor(math.log(i + 1, 2)))
        else:
            row = 0
            if row != last_row:
                output.write('\n')
            columns = 2 ** row
            col_width = int(math.floor(total_width / columns))
            output.write(str(n).center(col_width, fill))
            last_row = row
    print(output.getvalue())
    print('-' * total_width)
    print()

# heap = []
# for n in data:
#     print('add {:>3}:'.format(n))
#     heapq.heappush(heap, n)
#     show_tree(heap)
heap = [-10, -30, -30, -20]
heapq.heapify(heap)
show_tree(heap)
heapq.heappush(heap,-40)
heapq.heappush(heap,-50)
print(heap)
print(heapq.nlargest(2,heap))
