# 2021-12-26
import collections
class Solution:
    def solve(self, s):
        counter = collections.Counter()
        ws = 0
        we = 0
        mx = 0
        while we < len(s):
            counter.update(s[we])
            if len(counter) <= 2:
                mx = max(mx, sum(list(counter.values())))
            while len(counter) > 2 and ws < len(s):
                counter.subtract(s[ws])
                if counter[s[ws]] == 0:
                    del counter[s[ws]]
                ws += 1
            we += 1
        return mx

