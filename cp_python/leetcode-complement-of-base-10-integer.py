#https://leetcode.com/problems/complement-of-base-10-integer/
class Solution(object):
    def bitwiseComplement(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 0:
            return 1
        complement = ""
        ans = 0
        idx = 0
        while n:
            complement = 0 if n % 2 == 1 else 1
            ans += pow(2,idx) * complement
            idx += 1
            n //= 2

        return ans
