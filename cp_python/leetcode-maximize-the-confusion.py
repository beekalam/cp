import time
import random

class Solution(object):
    def count(self,_from,s,n,k,value):
        while _from < n:
            if s[_from] == value and k > 0:
                k -= 1
                _from += 1
                continue
            if s[_from] == value and k <= 0:
                break
            _from += 1
        return _from

    def maxConsecutiveAnswers_1(self, answerKey, k):
        ans = 0
        i,j = 0,0
        n = len(answerKey)
        kk = k
        while j < n:
            if answerKey[i] == 'F':
                kk = k - 1
            j = self.count(i+1,answerKey,n,kk,'F')
            print(j-i)
            ans = max(ans, j -i)
            print(answerKey[i:j-i])
            i = j
            
        i,j = 0,0
        kk = k
        while j < n:
            if answerKey[i] == 'T':
                kk = k - 1
            j = self.count(i+1,answerKey,n,kk,'T')
            print(j-i)
            ans = max(ans, j -i)
            print(answerKey[i:j-i])
            i = j

        return ans

    def maxConsecutiveAnswers(self, answerKey, k):
        ans = 0
        i,j = 0,0
        n = len(answerKey)
        kk = k
        while i < n:
            m = i
            while j < n:
                kk = k
                if answerKey[i] == 'F':
                    kk = k - 1
                j = self.count(m+1,answerKey,n,kk,'F')
                # print(j-m)
                ans = max(ans, j -m)
                # print(answerKey[m:j-m])
                m = j
            
            m = i
            while j < n:
                kk = k
                if answerKey[i] == 'T':
                    kk = k - 1
                j = self.count(m+1,answerKey,n,kk,'T')
                # print(j-m)
                ans = max(ans, j -m)
                # print(answerKey[m:j-m])
                m = j
            
            i += 1

        return ans
s = Solution()
print(s.maxConsecutiveAnswers(answerKey = "TTFF", k = 2))
print(s.maxConsecutiveAnswers(answerKey = "TFFT", k = 1))
print(s.maxConsecutiveAnswers(answerKey = "TTFTTFTT", k = 1))

# print(s.maxConsecutiveAnswers(answerKey = "FFFTTFTTFT",k = 3))


##############################################

# def id_generator(size=6, chars="TF"):
#     return ''.join(random.choice(chars) for _ in range(size))

# mystr = id_generator(5 * 10000,"T")
# start = time.time()
# print(s.maxConsecutiveAnswers(answerKey=mystr, k=1)) 
# end = time.time()
# print(end - start)




