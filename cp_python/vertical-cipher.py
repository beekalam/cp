class Solution:
    def solve(self, s, n):
        return [s[i::n] for i in range(n)]


s = Solution()
print(s.solve("abcdefghi", 5))
