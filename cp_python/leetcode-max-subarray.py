# 2021-12-11
from typing import List


class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        rs = nums[0]
        mx = rs
        for i in range(1, len(nums)):
            # print(rs)
            mx = max(rs, mx)
            if nums[i] >= 0:
                if rs > 0:
                    rs += nums[i]
                else:
                    rs = nums[i]
            elif nums[i] < 0:
                if rs > 0 and rs + nums[i] > 0:
                    rs += nums[i]
                else:
                    rs = nums[i]
            else:
                rs = nums[i]
            mx = max(rs, mx)
        return mx


s = Solution()
nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4]
assert s.maxSubArray(nums) == 6, "test1"

assert s.maxSubArray([-1]) == -1, "test2"
#
nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4]
assert s.maxSubArray(nums) == 6, "test3"

nums = [2, 0, 3, -2]
assert s.maxSubArray(nums) == 5, "test4"
