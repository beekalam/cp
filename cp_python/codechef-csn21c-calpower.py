import string
m = {}
for i,c in enumerate(string.ascii_lowercase):
    m[c] = i+1
t = int(input())
for i in range(t):
    s = input()
    print( sum( (i+1) * m[c] for i,c in enumerate(sorted(s))) )
