import math
from collections import Counter


class Solution:
    def solve(self, s):
        counts = Counter(s)
        cone = counts['1']
        cz = 0
        ans = -math.inf
        for i in range(len(s) - 1):
            c = s[i]
            if c == '0':
                cz += 1
            else:
                cone -= 1
            # print("cz: {}, cone: {}".format(cz, cone))
            ans = max(ans, cz + cone)
        return ans


s = Solution()
# print(s.solve("001001110111"))
# print(s.solve("00"))
s = "ass"
print(
    "".join(reversed(s))
)
