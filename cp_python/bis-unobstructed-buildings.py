# https://binarysearch.com/problems/Unobstructed-Buildings
class Solution:
    def solve(self, heights):
        n = len(heights)
        if n == 0:
            return []
        if n == 1:
            return [0]
        stack = [n-1]
        for i in range(n-2,-1,-1):
            if heights[stack[-1]] < heights[i]:
                stack.append(i)
        return sorted(stack)
