# 2022-01-08
# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, node):
        current = node
        previous = None
        nxt = None
        while current is not None:
            nxt = current.next
            current.next = previous
            previous = current
            current = nxt
        return previous
