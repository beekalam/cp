import math


class Solution:
    def can_be_made(self, word, letters):
        for c in set(word):
            if letters.count(c) < word.count(c):
                return False
        return True

    def solve(self, words, letters):
        m = 0
        for w in words:
            if self.can_be_made(w, letters):
                m = max(m, len(w))
        return m


words = ["the", "word", "love", "scott", "finder", "dictionary"]
letters = "fanierdow"

words = ["credit", "nirvana", "karma", "empathy", "hang", "aaaaaaaaa"]
letters = "afnvlfkm"

s = Solution()
print(s.solve(words, letters))
