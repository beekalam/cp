class Solution:

    def solve(self, path):
        ans = []
        for dir in path:
            if dir == ".." and len(ans):
                ans.pop()
            elif dir not in [".", ".."]:
                ans.append(dir)
        return ans


s = Solution()

print(s.solve(["usr", "..", "usr", ".", "local", "bin", "docker"]))
print(s.solve(["bin", "..", ".."]))
