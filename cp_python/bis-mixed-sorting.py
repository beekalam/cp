# 2021-11-22
class Solution:
    def solve(self, nums):
        n, i, j = len(nums), 0, 0
        evens = list(filter(lambda i: i % 2 == 0, nums))
        odds = list(filter(lambda i: i % 2 == 1, nums))
        evens.sort(reverse=True)
        odds.sort()
        for i, c in enumerate(nums):
            if c % 2 == 0:
                nums[i] = evens.pop()
            else:
                nums[i] = odds.pop()
        return nums


s = Solution()
assert s.solve(nums=[8, 13, 11, 90, -5, 4]) == [4, 13, 11, 8, -5, 90], "test1"
