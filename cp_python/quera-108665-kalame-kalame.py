# 2021-11-28
s = input()
c = len([c for c in s if c in 'aeiou'])
print(2 ** c)
