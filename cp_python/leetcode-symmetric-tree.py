# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from collections import deque
class Solution(object):
    def _isSymmetric(self, left, right):
        if left is None and right is not None:
            return False
        if left is not None and right is None:
            return False
        if left is None and right is None:
            return True
        if left.val != right.val:
            return False
        return self._isSymmetric(left.left, right.right) and \
                self._isSymmetric(left.right, right.left)

    def _is_symmetric_iterative(self,root):
        q = deque()
        q.append(root.left)
        q.append(root.right)
        while q:
            sta = q.popleft()
            stb = q.popleft()
            if sta is None and stb is None:
                continue

            if sta is not None and stb is not None:
                if sta.val != stb.val:
                    return False
                q.extend([sta.left,stb.right, sta.right, stb.left])
            else:
                return False

        return True
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        # return self._isSymmetric(root.right, root.left)
        return self._is_symmetric_iterative(root)
