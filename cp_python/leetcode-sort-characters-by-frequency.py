from collections import Counter
from functools import cmp_to_key
class Solution(object):
    def frequencySort(self, s):
        """
        :type s: str
        :rtype: str
        """
        c = Counter(s)
        s = list(s)
        def sort_by_freq(a,b):
            if c[a] == c[b]:
                return ord(b) - ord(a)
            return c[b] - c[a]
        s.sort(key=cmp_to_key(sort_by_freq))
        return "".join(s)
