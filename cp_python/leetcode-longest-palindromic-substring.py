# @notsolved: 2021-12-21
class Solution:
    def is_palindrome(self, s):
        return s == "".join(reversed(s))

    def longestPalindrome(self, s: str) -> str:
        n = len(s)
        ans = ""
        for i in range(n):
            j = i
            k = i
            while j - 1 >= 0 and k + 1 < n and s[j - 1] == s[k + 1]:
                j -= 1
                k += 1
            # print((k-j) + 1)
            if len(ans) < ((k - j) + 1):
                tmp = s[j:k + 1]
                if self.is_palindrome(tmp):
                    ans = tmp
                # print("1.ans: ", ans)
            j = i
            k = i
            diff = 1
            while j - (diff - 1) >= 0 and k + diff < n and s[j] == s[k]:
                diff += 1
                # print((k - j) + 1)
            j = j - (diff - 1)
            k = k + diff
            if len(ans) < ((k - j) + 1):
                tmp = s[j:k + 1]
                if self.is_palindrome(tmp):
                    ans = tmp
                # print("2.ans: ", ans)
        print(ans)
        return ans


s = Solution()
# assert s.longestPalindrome("babad") == "bab", "test1"
assert s.longestPalindrome("cbbd") == "bb", "test2"
