# union find
class DSU:
    def __init__(self, N):
        self.p = list(range(N))

    def find(self, x):
        if self.p[x] != x:
            self.p[x] = self.find(self.p[x])
        return self.p[x]

    def union(self, x, y):
        xr = self.find(x)
        yr = self.find(y)
        if yr < xr:
            self.p[xr] = yr
        else:
            self.p[yr] = xr

s = DSU(4)
s.union(1,2)
s.union(1,3)
s.union(2,3)
print(s.p)

