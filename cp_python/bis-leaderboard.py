# 2021-12-04
import collections
class Solution:
    def solve(self, nums):
        dic = collections.defaultdict(list)
        for idx,val in enumerate(nums):
            dic[val].append(idx)
        rank = 0
        for v in sorted(dic.keys(), reverse=True):
            for i in dic[v]:
                nums[i] = rank
            rank += 1
        return nums


s = Solution()
nums = [50, 30, 50, 90, 10]

assert s.solve(nums) == [1, 2, 1, 0, 3], "test 1"
