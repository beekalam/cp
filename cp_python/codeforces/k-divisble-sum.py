import unittest


class KDivisableSum:
    def solve(self, n, k):
        if n % 2 == 1:
            return k
        if k > n:
            return k


class TestKDivisibleSum(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = KDivisableSum()

    def test1(self):
        self.assertEqual(5, self.sol.solve(1, 5))

    def test2(self):
        self.assertEqual(2, self.sol.solve(4, 3))

    def test3(self):
        self.assertEqual(1, self.sol.solve(8, 8))

    def test4(self):
        self.assertEqual(8, self.sol.solve(3, 8))
