# 2022-01-02
import collections

class Solution:
    def solve(self, votes):
        seen= collections.defaultdict(int)
        for candidate_id, voter_id in votes:
            seen[voter_id] += 1
            if seen[voter_id] == 2:
                return True

        return False
