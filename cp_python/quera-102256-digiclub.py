# @notsolved: 2021-11-30
n = int(input())
a = list(map(int, input().split()))
k = int(input())
count, points, i = 1, 0, 0
a.sort(reverse=True)
# print(a)
if k > a[0]:
    points = sum(range(a[0], a[k] + 1))
    k = 0

while k:
    while i + 1 < n and a[i] == a[i + 1]:
        count += 1
        i += 1
    if (k - count) >= 0:
        points += count * a[i]
        k = k - count
    else:
        points += k * a[i]
        break
    a[i] = a[i] - 1
print(points % ((10 ** 9) + 7))
