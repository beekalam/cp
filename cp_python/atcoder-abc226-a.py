# @readme.notsolved 2021-11-07
import math

num = float(input())

if int(num) == 0:
    print(0)
else:
    n = int(num * 10)
    if n % 10 >= 5:
        num = math.ceil(num)
    elif n % 10 == 0:
        num = int(num)
    else:
        num = math.floor(num)
    print(num)
