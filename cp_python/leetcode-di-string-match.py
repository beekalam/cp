class Solution(object):
    def diStringMatch(self, s):
        """
        :type s: str
        :rtype: List[int]
        """
        n = len(s)
        i ,j = 0,n
        idx = 0
        ans = []
        while i < j:
            if s[idx] == 'D':
                ans.append(j)
                j -= 1
            else:
                ans.append(i)
                i += 1
            idx += 1
        while len(ans) <= n:
            ans.append(i)
            i += 1
        return ans

s = Solution()
print(s.diStringMatch("IDID"))
