# 2021-12-15
class Solution:
    def solve(self, n):
        return sum(i for i in range(1,(n*2)+1,2))
