class Solution:
    def solve(self, nums, target):
        nums.sort(reverse=True)
        # print(nums)
        l, r = 0, 1
        min_pair = float('-inf')
        n = len(nums)
        for r in range(n):
            for l in range(r + 1, n):
                if nums[r] + nums[l] < target:
                    min_pair = max(min_pair, nums[r] + nums[l])
                    break
        # print("min_pair: ", min_pair)
        return min_pair


s = Solution()
assert s.solve([5, 1, 2, 3], 4) == 3, "test2"
assert s.solve([-2, -3, -1], -3) == -4, "test1"
assert s.solve([1, 2, 3, 3], 6) == 5, "test3"
