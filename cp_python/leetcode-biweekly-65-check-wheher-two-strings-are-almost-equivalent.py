# 2021-11-13
import collections
class Solution:
    def checkAlmostEquivalent(self, word1: str, word2: str) -> bool:
        c1 = collections.Counter(word1)
        c2 = collections.Counter(word2)
        for c in c1:
            diff = abs(c2[c] - c1[c])
            if diff > 3:
                return False
        for c in c2:
            diff = abs(c2[c] - c1[c])
            if diff > 3:
                return False
        return True


s = Solution()
word1 = "abcdeef"
word2 = "abaaacc"
assert s.checkAlmostEquivalent(word1,word2) == True,"test1"

word1 = "aaaa"
word2 = "bccb"
assert s.checkAlmostEquivalent(word1,word2) == False,"test2"

word1 = "cccddabba"
word2 = "babababab"
assert s.checkAlmostEquivalent(word1,word2) == True,"test3"

