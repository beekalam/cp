class Solution:
    def solve(self, s):
        n = len(s)
        if n == 0:
            return 0
        # m =  o = 0
        # z = 1
        # for c in s:
        #     if c == '0' and z == 0:
        #         m = max(o,m)
        #         z = 1
        #         o = 0
        #     if c == '0' and z == 1:
        #         o += 1
        #         z -=1
        #     if c == '1':
        #         o += 1
        #     m = max(o,m)
        #     print("o: {}".format(o))
        #     print("z: {}".format(z))
        # m = max(o,m)
        # return m

        # o = z = 0
        # m = 0
        # total_ones = 0
        # for c in s:
        #     if c == '1':
        #         o += 1
        #         total_ones += 1
        #     else:
        #         if z == 0:
        #             z += 1
        #             o += 1
        #         else:
        #             m = max(o,m)
        #             z = 0
        #             o = 1
        #     m = max(o,m)
        # m = max(o,m)

        # o = 0
        # z = 0
        # m = 0
        # total_ones = 0
        # for c in s:
        #     if z == 1 and c == '0':
        #         m = max(m,o+z)
        #         o = 0
        #     if c == '1':
        #         o += 1
        #         total_ones += 1
        #     if c == '0' and z == 0:
        #         z += 1

        #     m = max(m,o + z)
        # m = max(m,o+z)
        # if total_ones == 0:
        #     return 1
        # return m

        m = 0
        n = len(s)
        i = 0
        while i < n:
            j = i
            z = 0
            o = 0
            while j < n:
                if s[j] == '1':
                    o += 1
                else:
                    z += 1
                if z == 2:
                    m = max(m, o + z - 1)
                    break
                m = max(m, o + z)
                j += 1

            if j >= n:
                break
            if j > i:
                i = j -1
            else:
                i += 1

        return m


s = Solution()
assert 2 == s.solve("001"),"test 1 failed"
assert 4 == s.solve("10110"), "test 2 failed"
assert 2 == s.solve("001"), "test 3 failed"
assert 1 == s.solve("000"), "test 4 failed"
assert 2 == s.solve("0010"), "test 5 failed"
assert 3 == s.solve("0101"), "test 6 failed"
assert 1 == s.solve("00"), "test 7 failed"
assert 4 == s.solve("01101"), "test 8 failed"
print(".............................")
print(s.solve("01101"))
