# 2021-11-10
from typing import Optional
from collections import deque

from utils.LinkedList import linkedlist_from_list, linkedlist_to_list, ListNode


class Solution:
    def swapNodes(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        q = deque()
        tmp = head
        idx = 0
        kth = None
        while tmp:
            idx += 1
            if idx == k:
                kth = tmp
            q.append(tmp)
            if len(q) > k:
                q.popleft()
            tmp = tmp.next
        kth_end = q.popleft()
        kth_end.val, kth.val = kth.val, kth_end.val
        return head


s = Solution()

res = s.swapNodes(linkedlist_from_list([1, 2, 3, 4, 5]), 2)
assert linkedlist_to_list(res) == [1, 4, 3, 2, 5]

res = s.swapNodes(linkedlist_from_list([7, 9, 6, 6, 7, 8, 3, 0, 9, 5]), 5)
assert linkedlist_to_list(res) == [7, 9, 6, 6, 8, 7, 3, 0, 9, 5]

res = s.swapNodes(linkedlist_from_list([1]), 1)
assert linkedlist_to_list(res) == [1]
