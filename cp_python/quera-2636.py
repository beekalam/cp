# 2021-12-21
l = [1,1,2,2,2,8]
inp = list(map(int,input().split()))
ans = [str(l[i] - inp[i]) for i in range(len(inp))]
print(" ".join(ans))
