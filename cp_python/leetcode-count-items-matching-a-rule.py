class Solution(object):
    def countMatches(self, items, ruleKey, ruleValue):
        """
        :type items: List[List[str]]
        :type ruleKey: str
        :type ruleValue: str
        :rtype: int
        """
        ans = 0
        for t,c,n in items:
            if ruleKey == "type" and t == ruleValue:
                ans += 1
            elif ruleKey == "color" and c == ruleValue:
                ans += 1
            elif ruleKey == "name" and n == ruleValue:
                ans += 1
        return ans

s = Solution()

items = [["phone", "blue", "pixel"], ["computer", "silver", "lenovo"], ["phone", "gold", "iphone"]]
ruleKey = "color"
ruleValue = "silver"
print(s.countMatches(items, ruleKey, ruleValue))
