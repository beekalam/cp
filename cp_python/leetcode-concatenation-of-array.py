import copy
class Solution(object):
    def getConcatenation(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        nums.extend(nums)
        return nums

s = Solution()
print(s.getConcatenation([1, 2, 1]))