# 2021-11-29
import collections
from typing import List
# https://github.com/samgh/6-Weeks-to-Interview-Ready/blob/master/module_1/python/find_anagrams.py


class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        cp = collections.Counter(p)
        cs = collections.Counter(s[0])
        length, i = 1, 0
        ans = []
        while i < len(s):
            if cs == cp:
                ans.append((i + 1) - length)
            if length >= len(p):
                c = s[(i + 1) - length]
                cs.subtract(s[(i + 1) - length])
                if cs[c] == 0:
                    del cs[c]
                length -= 1
                if cs == cp:
                    ans.append((i + 1) - length)
            length += 1
            i += 1
            if i < len(s):
                cs.update(s[i])
        if cs == cp:
            ans.append(i - length)
        return ans

    def findAnagrams1(self, s: str, p: str) -> List[int]:
        cp = collections.Counter(p)
        n, m = len(s), len(p)
        i, j = 0, 1
        cs = collections.Counter(s[0])
        ans = []
        while j < n:
            if cs == cp:
                print('...')
                cs.subtract(s[i])
                ans.append(i)
                i += 1
            elif (j - i + 1) >= m:
                cs.subtract(s[i])
                i += 1
            cs.update(s[j])
            print(cs)
            j += 1
        print(ans)
        return ans

    def bruteForce(self, s: str, p: str) -> List[int]:
        m, n = len(s), len(p)
        ans = []
        for i in range(0, m):
            if sorted(s[i:i + n]) == sorted(p):
                ans.append(i)
        return ans


s = Solution()

assert s.findAnagrams(s="cbaebabacd", p="abc") == [0, 6], "test1"
assert s.findAnagrams(s="c", p="c") == [0], "test2"
assert s.findAnagrams(s="cc", p="c") == [0, 1], "test3"
assert s.findAnagrams(s="abab", p="ab") == [0, 1, 2], "test4"
