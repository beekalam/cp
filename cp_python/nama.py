k = int(input())

template = """
########.......########
#{}.......{}#
########.......########
#{}.......{}#
########.......########
#{}.......{}#
########.......########
#{}.......{}#
#######################
"""
replacements = []
for i in range(1, 9):
    if i <= k:
        replacements.append("ghorfe{}".format(i))
    else:
        replacements.append(".......")

print(template.format(*replacements))
