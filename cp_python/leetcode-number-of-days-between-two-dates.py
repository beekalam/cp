# 2021-11-29
import datetime
class Solution:
    def sol1(self, date1: str, date2: str) -> int:
        format = "%Y-%m-%d"
        d1 = datetime.datetime.strptime(date1,format)
        d2 = datetime.datetime.strptime(date2, format)
        if d1 > d2:
            return (d1-d2).days
        else:
            return (d2 - d1).days
    def daysBetweenDates(self, date1: str, date2: str) -> int:
		# return self.sol1(date1, date2)
        

s = Solution()
print(s.daysBetweenDates("2019-06-29", "2019-06-03"))
