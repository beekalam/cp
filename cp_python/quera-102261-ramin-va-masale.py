# 2021-11-29
import math

q = int(input())
cache = []
answers = []
for _ in range(q):
    r, l = list(map(int, input().split()))
    ans = 0
    i = int(math.sqrt(r)) - 1
    while True:
        j = i * i
        if j >= r and j <= l:
            ans += 1
        if j > l:
            break
        i += 1
    answers.append(ans)

for a in answers:
    print(a)
