t = int(input())
for i in range(t):
    n, v = list(map(int, input().split()))
    x = n - 1
    MAX = (x * (x + 1)) / 2

    # at first stop we fill the tank
    # at the rest of stops we stop and fill +1
    # at point x we can go to the last check point
    # without buying.
    x = n - v
    MIN = (x * (x + 1)) / 2
    MIN -= 1
    MIN += v
    print("{} {}".format(int(MAX), int(MIN)))
