# @readme.notsolved 2021-11-07

# @readme: correct answer
from collections import deque
from collections import namedtuple

# @snippet: namedtuple example
move = namedtuple('move', ('time', 'prerequisites'))
n = int(input())
m = {}
for i in range(1, n + 1):
    line = list(map(int, input().split()))
    m[i] = move(line[0], line[2:] if len(line) > 2 else [])

q = deque()
q.append((n, m[n]))
seen = set()
ans = 0
while q:
    idx, mv = q.popleft()
    if idx not in seen:
        ans += mv.time
        for p in mv.prerequisites:
            q.append((p, m[p]))
    seen.add(idx)
print(ans)
