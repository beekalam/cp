class Solution(object):
    def __init__(self):
        self.m = {}
    def SolveRecursive(self,n):
        if n in self.m:
            return self.m[n]
        if n == 0:
            return 1
        if n < 0:
            return 0
        else:
            ans = self.SolveRecursive(n-1)
            self.m[n-1] = self.m.get(n-1,ans)
            ans += self.SolveRecursive(n-2)
            self.m[n-2] = self.m.get(n-2, ans)
            return ans
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        return self.SolveRecursive(n)
        if n == 1:
            return 1
        if n == 2:
            return 2
        dp = [1,2]
        ans = 0
        while dp:
            new_dp = []
            for i in [1,2]:
                for j in dp:
                    if i + j < n:
                        new_dp.append(i + j)
                    if i + j == n:
                        ans += 1
            dp = new_dp
        return ans
s = Solution()
print(s.climbStairs(9000))
