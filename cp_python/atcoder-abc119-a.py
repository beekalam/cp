# 2021-11-29
year, month, day = input().split('/')
year, month, day = int(year), int(month), int(day)
if year <= 2019 and month <= 4 and day <= 30:
    print("Heisei")
else:
    print("TBD")
