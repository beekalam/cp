# 2021-12-21
angles = list(map(int, input().split()))

if sum(angles) == 180 and all(i > 0 for i in angles):
    print("Yes")
else:
    print("No")
