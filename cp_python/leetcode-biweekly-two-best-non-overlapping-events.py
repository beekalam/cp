class Solution(object):
    def overlaps(self, a, b):
        return a[0] <= b[0] <= a[1] or a[0] <= b[1] <= a[1] or b[0] <= a[0] <= b[1] or b[0] <= a[1] <= b[1]

    def maxTwoEvents(self, events):
        """
        :type events: List[List[int]]
        :rtype: int
        """
        # events.sort(key=lambda a: a[0])
        sum = events[0][2]
        i = 1
        n = len(events)
        ans = sum
        while i < n:
            ans = max(ans, sum, events[i][2])
            if not self.overlaps(events[i], events[i - 1]):
                sum += events[i][2]
            else:
                sum = events[i][2]
            ans = max(ans, sum, events[i][2])
            i += 1
        ans = max(ans, sum)
        print("ans: {}".format(ans))
        return ans


s = Solution()
assert s.maxTwoEvents(events=[[1, 3, 2], [4, 5, 2], [2, 4, 3]]) == 4, "test1"
# assert s.maxTwoEvents(events=[[1, 3, 2], [4, 5, 2], [1, 5, 5]]) == 5, "test2"
# assert s.maxTwoEvents(events=[[1, 5, 3], [1, 5, 1], [6, 6, 5]]) == 8, "test3"
