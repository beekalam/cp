class Solution:
    def solve(self, s):
        a = list(s)
        c = 0
        for i in range(0, len(a) - 2):
            if a[i] == a[i + 1] and a[i + 1] == a[i + 2] and a[i] == '0':
                c += 1
                a[i + 1] = '1'
            elif a[i] == a[i + 1] and a[i + 1] == a[i + 2] and a[i] == '1':
                c += 1
                a[i + 1] = '0'
        return c


s = Solution()
assert s.solve(s="1100011") == 1, "test1"
