t = int(input())
for i in range(t):
    s = input()
    cab = s.count("ab")
    cba = s.count("ba")
    s = list(s)
    n = len(s)
    if cab == cba:
        print("".join(s))
    elif cba > cab:
        diff = cba - cba
        idx = 0
        while diff:
            diff -= 1
            while idx < n:
                if idx + 2 <n and s[idx] == 'b' and s[idx+1] == 'a' and s[idx+2] =='b':
                    idx += 1
                    continue
                if idx + 1 < n and s[idx] == 'b' and s[idx+1]=='a':
                    s[idx] = "a"
                    break
        print("".join(s))
    elif cab > cba:
        diff = cab - cba
        idx = 0
        while diff:
            diff -= 1
            while idx < n:
                if idx +2 <n and s[idx]=='a' and s[idx + 1] == 'b' and s[idx+2] == 'a':
                    idx += 1
                    continue
                if idx +1 < n and s[idx] == 'a' and s[idx+1] == 'b':
                    s[idx] = "b"
                    break
        print("".join(s))

