# 2022-01-08
# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, node, k):
        stack = []
        # lst,head = None, None
        current = node
        head = LLNode(0)
        lst = head
        while current:
            if len(stack) != k:
                stack.append(current)
                current = current.next
            else:
                while len(stack) > 0:
                    lst.next = stack.pop()
                    lst = lst.next
        while len(stack) > 0:
            lst.next = stack.pop()
            lst = lst.next
        lst.next = None
        return head.next
