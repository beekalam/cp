t = int(input())
for _ in range(t):
	n,s,a = list(map(int, input().split()))
	ans = -1
	if a >= s:
		ans = -1
	else:
		res = (s-a) / n
		ans = -1 if (res - int(res)) != 0 else res
	print(int(ans))
