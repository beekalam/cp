# 2021-11-24
class Solution:
    def solve(self, nums):
        nums.sort()
        diff = abs(nums[1] - nums[0])
        for i in range(2,len(nums)):
            if abs(nums[i] - nums[i-1]) != diff:
                return False
        return True
