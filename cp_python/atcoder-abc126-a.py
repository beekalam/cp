# 2021-11-29
n,k = list(map(int, input().split()))
s = input()
res = ""
for i,c in enumerate(s):
    if i == k-1:
        res += c.lower()
    else:
        res += c
print(res)
