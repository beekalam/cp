# 2022-01-07
class Solution:
    def solve(self, intervals):
        intersection = [
            max(i[0] for i in intervals),
            min(i[1] for i in intervals)
        ]
        return intersection
