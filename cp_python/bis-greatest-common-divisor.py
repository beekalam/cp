class Solution:
    def gcd(self, a, b):
        return a if b == 0 else self.gcd(b, a % b)

    def solve(self, nums):

        n = len(nums)
        if n == 1:
            return nums[0]
        ans = nums[0]
        for i in range(1,n):
            ans = self.gcd(ans, nums[i])
        return ans

s = Solution()
print(s.solve([6,12,14]))
