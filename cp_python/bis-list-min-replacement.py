# 2021-11-30
class Solution:
    def solve(self, nums):
        if len(nums) == 1:
            return [0]
        mn = old_mn = nums[0]

        for i in range(1,len(nums)):
            old_mn = mn
            mn = min(nums[i],mn)
            nums[i] = old_mn
        nums[0] = 0
        return nums
