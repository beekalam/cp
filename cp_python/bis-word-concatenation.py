import itertools
import string

from string import ascii_letters  # in Python 2 one would import letters

alphabet = string.ascii_letters


class Trie_Node:
    def __init__(self, ch):
        self.isWord = False
        self.c = ch
        self.children = {c: None for c in alphabet}


class Trie:
    def __init__(self):
        self.root = Trie_Node('\0')

    def add_word(self, word):
        curr = self.root
        for c in word:
            if curr.children[c] is None:
                curr.children[c] = Trie_Node(c)
            curr = curr.children[c]
        curr.isWord = True

    def is_word(self, word):
        curr = self.root
        for c in word:
            if curr.children[c] is None:
                return False
            curr = curr.children[c]
        return curr.isWord

    def is_concat(self, word):
        curr = self.root
        n = len(word)
        for i, c in enumerate(word):
            if curr.isWord:
                rest = word[i:]
                if self.is_word(rest) or self.is_concat(rest):
                    return True
            if curr.children[c] is not None:
                curr = curr.children[c]
            else:
                return False
        return False


words = ["news", "paper", "newspaper", "binary", "search", "binarysearch"]

t = Trie()
for w in words:
    t.add_word(w)
ans = 0
for w in words:
    if t.is_concat(w):
        ans += 1
print(ans)


# words = sorted(words, key=len)

# t = Trie([words[0]])
# for i in range(1, len(words)):
#     print(is_concatenation(t, words[i], 0))
#     add(t, words[i])

# class Solution:
#     def solve(self, words):
#         l = len(words)
#         ans = 0
#         for i in range(l):

#             for j in range(i+1,l):
#                 if (words[i] + words[j]) in words:
#                     ans += 1
#                 elif (words[j] + words[i]) in words:
#                     ans += 1
#                 else:
#                     m,n = words[i],words[j]
#                     if len(words[i]) < len(words[j]):
#                         m,n = words[j],words[i]
#                     for p in range(100):
#                         if n * p == m:
#                             ans += 1
#                     # found = True
#                     # q = 0
#                     # for p in m:
#                     #     if q == len(n):
#                     #         q = 0
#                     #     if p != q:
#                     #         found = False
#                     #     q += 1
#                     # if found and (q == len(n) or q == 0):
#                     #     ans += 1

#         return ans


# s = Solution()
# words = ["news", "paper", "newspaper", "binary", "search", "binarysearch"]
# ans = s.solve(words)
# print(ans)

# words = ["cc", "c"]
# ans = s.solve(words)
# print(ans)

# words =["b","bb","d"]
# ans = s.solve(words)
# print(ans)

# words = ["a","ba","aaa"]
# ans = s.solve(words)
# print(ans)

# words = ["b","bcb"]
# ans = s.solve(words)
# print(ans)

# words = ["bc","bcb"]
# ans = s.solve(words)
# print(ans)

# words = ["c","aa","aaa"]
# ans = s.solve(words)
# print(ans)

# words = ["b", "dc", "dcb"]
# ans = s.solve(words)
# print(ans)
