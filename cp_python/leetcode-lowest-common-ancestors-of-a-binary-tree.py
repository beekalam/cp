# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
import collections


class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        Ancestor = collections.namedtuple('Ancestor', ('node', 'parents'))
        qa, pa = [], []
        q = [Ancestor(root, [])]
        while q:
            r = q.pop(0)
            if r.node == p:
                pa = r.parents
                pa.append(r.node)
            if r.node == q:
                qa = r.parents
                qa.append(r.node)
            if r.node.right:
                parents = r.parents
                parents.append(r.node)
                q.append(Ancestor(r.node.right, parents))
            if r.node.left:
                parents = r.parents
                parents.append(r.node)
                q.append(Ancestor(r.node.left, parents))
        print([i.val for i in pa])
        print([i.val for i in qa])
        for i in pa:
            for j in qa:
                if i == j:
                    return i
