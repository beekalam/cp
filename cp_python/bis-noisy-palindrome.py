# 2021-12-24
class Solution:
    def solve(self, s):
        i = 0
        j = len(s) - 1
        while i < j:
            if s[i].isdigit() or s[i].isupper():
                i += 1
            elif s[j].isdigit() or s[j].isupper():
                j -= 1
            elif s[i] != s[j]:
                return False
            else:
                i += 1
                j -= 1
        return True
