# 2022-01-09
def find_subsets(nums):
    subsets = []
    subsets.append([])
    for cn in nums:
        n = len(subsets)
        for i in range(n):
            st = list(subsets[i])
            st.append(cn)
            subsets.append(st)
    return subsets

class Solution:

    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        st = set( [tuple(v) for v in find_subsets(nums) ] )
        return [list(v) for v in st]
