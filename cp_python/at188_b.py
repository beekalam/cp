n = int(input())

lst1 = input().split(' ')
lst2 = input().split(' ')

res = [int(lst1[i]) * int(lst2[i]) for i in range(n)]

# print(sum(res))
if sum(res) == 0:
    print("Yes")
else:
    print("No")
