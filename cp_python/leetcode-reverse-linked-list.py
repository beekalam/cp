# 2021-11-30
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
import collections
class Solution:
    def sol1(self,head: Optional[ListNode]) -> Optional[ListNode]:
        q = collections.deque()
        tmp = head
        while tmp:
            q.append(tmp)
            tmp = tmp.next
        while q:
            a = q.popleft()
            if q:
                b = q.pop()
                a.val,b.val = b.val,a.val
        return head
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        return self.sol1(head)
