import collections
class Solution:
    def solve(self, nums):
        snums = collections.Counter(nums)
        ans = []
        for i in nums:
            if snums[i] == 1:
                ans.append(i)
        return ans
