class Solution:
    def correct_order(self, s):
        increasing_seq = all([s[i] >= s[i - 1] for i in range(1, len(s))])
        decreasing_seq = all([s[i] <= s[i - 1] for i in range(1, len(s))])
        return increasing_seq or decreasing_seq

    def solve(self, s):
        ans, st = [], ""
        for i, c in enumerate(s):
            if len(st) < 2:
                st = st + c
            elif self.correct_order(st + c):
                st = st + c
            else:
                print(st)
                ans.append(st)
                st = c

        if len(st):
            ans.append(st)
        print(".........................")
        print(ans)
        print(".........................")
        return len(ans)


s = Solution()
assert s.solve("dccd") == 2, "failed"
