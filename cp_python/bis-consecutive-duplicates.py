# 2021-11-27
class Solution:
    def solve(self, s):
        if s == "":
            return ""
        arr = [s[0]]
        for i in range(1,len(s)):
            if arr[-1] == s[i]:
                continue
            else:
                arr.append(s[i])
        ans = "".join(arr)
        return ans
