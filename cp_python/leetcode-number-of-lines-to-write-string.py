# https://leetcode.com/problems/number-of-lines-to-write-string/
# 2021-11-10
from typing import List
import string


class Solution:
    def numberOfLines(self, widths: List[int], s: str) -> List[int]:
        sl = string.ascii_lowercase
        w = [widths[sl.index(c)] for c in s]
        i, l, lines = 0, 0, 0

        while i < len(w):
            # if w[i] + l == 100:
            #     lines += 1
            #     l = 0
            if w[i] + l > 100:
                lines += 1
                l = w[i]
            else:
                l += w[i]
            i += 1
        if l:
            lines += 1
        return [lines, l]


s = Solution()

widths = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
ss = "abcdefghijklmnopqrstuvwxyz"
assert s.numberOfLines(widths, ss) == [3, 60], "test1"

widths = [3, 4, 10, 4, 8, 7, 3, 3, 4, 9, 8, 2, 9, 6, 2, 8, 4, 9, 9, 10, 2, 4, 9, 10, 8, 2]
ss = "mqblbtpvicqhbrejb"
assert s.numberOfLines(widths, ss) == [1, 100], "test2"
