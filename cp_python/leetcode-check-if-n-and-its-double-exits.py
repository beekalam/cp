# 2021-11-16
from typing import List
import collections
class Solution:
    def checkIfExist(self, arr: List[int]) -> bool:
        dic = collections.Counter(arr)
        if 0 in dic and dic[0] == 1:
            del dic[0]
        arr = [k for k in dic if k * 2 in dic]
        return len(arr) > 0



s = Solution()
assert s.checkIfExist([10,2,5,3]) == True, "test1"
assert s.checkIfExist([-2,0,10,-19,4,6,-8]) == False, "test2"
assert s.checkIfExist([-20,8,-6,-14,0,-19,14,4]) == True, "test3"
assert s.checkIfExist([0,0]) == True, "test4"
