# 2021-11-22
from functools import cmp_to_key
from typing import List
import heapq


def sort_nums(a, b):
    la, lb = len(a), len(b)
    if la == lb:
        for ca, cb in zip(a, b):
            ia, ib = int(ca), int(cb)
            if ia != ib:
                return int(ca) - int(cb)
        return 0
    else:
        return la - lb


class Solution:
    def sol2(self, nums: List[str], k: int) -> str:
        nums.sort(key=cmp_to_key(sort_nums))
        return nums[-k]
        # print(nums)

    def kthLargestNumber(self, nums: List[str], k: int) -> str:
        return self.sol2(nums, k)
        nums = list(map(lambda x: int(x) * -1, nums))
        heapq.heapify(nums)
        ans = 0
        while k:
            ans = heapq.heappop(nums) * -1
            k -= 1
        return str(ans)


s = Solution()
# assert s.kthLargestNumber(nums=["3", "6", "7", "10"], k=4) == "3", "test1"
assert s.kthLargestNumber(nums=["0", "0"], k=2) == "0", "test2"
