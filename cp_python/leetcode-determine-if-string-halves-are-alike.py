class Solution(object):
    def halvesAreAlike(self, s):
        """
        :type s: str
        :rtype: bool
        """
        n = len(s)
        mid = n // 2
        j = mid
        a,b = 0,0
        vowels = set("aeiouAEIOU")
        for i in range(mid):
            if s[i] in vowels:
                a += 1
            if s[j] in vowels:
                b += 1
            j += 1
        return a == b
