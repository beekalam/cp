# 2021-11-12
from typing import Optional
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def recursive(self, root,val):
        if root is None:
            return True
        if root.val != val:
            return False
        return self.recursive(root.right, root.val) and self.recursive(root.left,root.val)

    def isUnivalTree(self, root: Optional[TreeNode]) -> bool:
        return self.recursive(root, root.val)
