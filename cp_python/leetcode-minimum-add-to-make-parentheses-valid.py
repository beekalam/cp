# https://leetcode.com/problems/minimum-add-to-make-parentheses-valid/solution/
class Solution(object):
    def minAddToMakeValid(self, s):
        """
        :type s: str
        :rtype: int
        """
        st = []
        for c in s:
            if c == '(':
                st.append(c)
            else:
                if st and st[-1] == '(':
                    st.pop()
                else:
                    st.append(c)
        return len(st)
