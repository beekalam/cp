# 2021-11-22
import heapq
import math
import bisect


class MedianFinder:

    def __init__(self):
        self.nums = []

    def addNum(self, num: int) -> None:
        # heapq.heappush(self.nums, num)
        bisect.insort(self.nums,num)

    def addNums(self, nums):
        for i in nums:
            self.addNum(i)

    def findMedian(self) -> float:
        n = len(self.nums)
        # print(self.nums)
        if n % 2 == 0:
            idx = int(n / 2)
            return (self.nums[idx] + self.nums[idx - 1]) / 2
        else:
            idx = math.floor(n / 2)
            return self.nums[idx]


# Your MedianFinder object will be instantiated and called as such:
obj = MedianFinder()
obj.addNum(1)
obj.addNum(2)
assert obj.findMedian() == 1.5, "test1"
obj.addNum(3)
assert obj.findMedian() == 2.0, "test2"

obj = MedianFinder()
obj.addNums([-1, -2, -3, -4, -5])

res = obj.findMedian()
# print("res: ", res)
assert res == -3.00000
