# 2022-01-03
import collections
class Solution:
    def solve(self, nums):
        if len(nums) == 0:
            return 0
        return max(collections.Counter(nums).values())
