from collections import Counter
class Solution(object):
    def countKDifference(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        ans = 0
        n = len(nums)
        for (i,vi) in enumerate(nums):
            ans += len([ abs(j - vi)
                         for j in nums[i+1:]
                         if abs(j - vi) == k  ])
        return ans

s = Solution()
nums = [1,2,2,1]
k = 1
assert s.countKDifference(nums,k) == 4,"test1"

nums = [1,3]
k = 3
assert s.countKDifference(nums,k) == 0,"test2"

nums = [3,2,1,5,4]
k = 2
print(s.countKDifference(nums,k))

