class Solution(object):
    def bruteforce(self, nums1, m, nums2, n):
        j = 0
        for i in range(m, m + n):
            nums1[i] = nums2[j]
            j += 1
        nums1.sort()
        return nums1

    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: None Do not return anything, modify nums1 in-place instead.
        """
        # return self.bruteforce(nums1,m, nums2, n)
        return self.iterative(nums1, m, nums2, n)

    def iterative(self, nums1, m, nums2, n):
        m = m - 1
        n = n - 1
        index = len(nums1) - 1
        while index >= 0:
            # print(nums1)
            if m < 0 :
                nums1[index] = nums2[n]
                n -= 1
            elif n < 0:
                nums1[index] = nums1[m]
                m -= 1
            else:
                if nums1[m] > nums2[n]:
                    nums1[index] = nums1[m]
                    m -= 1
                else:
                    nums1[index] = nums2[n]
                    n -= 1

            index -= 1
        return nums1


s = Solution()
print(s.merge([4, 0, 0, 0, 0, 0], 1, [1, 2, 3, 5, 6], 5))
print("..............................")
print(s.merge([1, 2, 4, 5, 6, 0], 5, [3], 1))
print("..............................")
print(s.merge([0, 0, 3, 0, 0, 0, 0, 0, 0], 3, [-1, 1, 1, 1, 2, 3], 6))
