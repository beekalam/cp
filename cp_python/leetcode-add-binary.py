class Solution(object):
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        if len(a) < len(b):
            return self.addBinary(b, a)
        m, n = len(a), len(b)
        carry = 0
        ans = []
        ai = m - 1
        bi = n - 1
        while ai > -1:
            p = int(a[ai])
            q = 0 if bi < 0 else int(b[bi])
            ai -= 1
            bi -= 1
            if p + q + carry == 3:
                carry = 1
                ans.append('1')
            elif p + q + carry == 2:
                carry = 1
                ans.append('0')
            elif p + q + carry == 1:
                carry = 0
                ans.append('1')
            else:
                ans.append('0')
        if carry != 0:
            ans.append('1')

        return "".join(reversed(ans))


s = Solution()
assert s.addBinary("11", "1") == "100"
assert s.addBinary("1010", "1011") == "10101"
