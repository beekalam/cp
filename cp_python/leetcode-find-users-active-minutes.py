import collections
from typing import List

class Solution:
    def findingUsersActiveMinutes(self, logs: List[List[int]], k: int) -> List[int]:
        users = collections.defaultdict(int)
        for id,time in (set(map(tuple,logs))):
            users[id] += 1

        uam = collections.defaultdict(int)
        for id,user_uam in users.items():
            uam[user_uam] += 1

        ans =[0] * k

        for i in range(0,k+1):
            if i+1 in uam:
                ans[i] = uam[i+1]
        return ans
