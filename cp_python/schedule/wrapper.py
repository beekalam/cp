import datetime
import functools
import sys


def wrap():
    def decorate(func):
        @functools.wraps(func)
        def call(*args, **kwargs):
            if func.__name__ == 'should_run' and datetime.datetime.now() == datetime.datetime(2010, 1, 6, 13, 16):
                return False

            result = func(*args, **kwargs)
            if func.__name__ == 'to':
                # print(result.interval)
                # print(result.latest)
                interval = result.latest
                latest = result.latest + result.interval
                result.interval, result.latest = interval, latest
                return result

            if func.__name__ == '__repr__':
                return str(result).replace('Every', 'Har')

            return result

        return call

    return decorate
