import math

ans = 0
l = []
for _ in range(0, 6):
    l.append(int(input().strip()))
    if len(l) == 2:
        ans += min(l)
        l = []

print(ans)
