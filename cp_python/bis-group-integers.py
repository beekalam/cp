# 2021-12-07
import collections



class Solution:
    def gcd(self, a, b):
        return a if b == 0 else self.gcd(b, a % b)
    # @snippet gcd multiple numbers
    def gcd_array(self, nums):

        n = len(nums)
        if n == 1:
            return nums[0]
        ans = nums[0]
        for i in range(1, n):
            ans = self.gcd(ans, nums[i])
        return ans

    def solve(self, nums):
        if len(nums) < 2:
            return False
        cnt = collections.Counter(nums)
        gcd = self.gcd_array(list(cnt.values()))
        # print("gcd: ", gcd)
        return gcd > 1


s = Solution()
nums = [2, 3, 5, 8, 3, 2, 5, 8]
assert s.solve(nums) == True, "test1"

nums = [3, 0, 0, 3, 3, 3]
assert s.solve(nums) == True, "test1"

assert s.solve([1]) == False, "test2"
#
assert s.solve([1, 2]) == False, "test2"
assert s.solve([2, 2]) == True, "test2"
