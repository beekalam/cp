class Solution(object):
    def removeOuterParentheses(self, s):
        level = 0
        ans = ""
        for c in s:
            if c == '(':
                level += 1
                if level >= 2:
                    ans += c
            else:
                if level >= 2:
                    ans += c
                level -= 1

        print("level:{}, ans:{}".format(level, ans))
        return ans


s = Solution()
# print(s.removeOuterParentheses("(()())(())"))
print(s.removeOuterParentheses("(()())(())"))
# print(s.removeOuterParentheses("(()()(()))"))
# print(s.removeOuterParentheses("((()))"))
