def can_solve(r, g, b):
    if r == g:
        return r
    r -= 1
    g -= 1
    b += 2
    print("r: {},g: {},b: {}".format(r, g, b))
    if r == b:
        return r + 1
    if g == b:
        return g + 1
    return 0


def solve(r, g, b):
    m = min(
        can_solve(r, g, b),
        can_solve(r, b, g),
        can_solve(b, g, r)
    )
    return -1 if m == 0 else m


def read_input():
    for i in range(int(input())):
        r, g, b = list(map(int, input().split()))
        print(solve(r, g, b))


def tests():
    # assert solve(1, 2, 2) == 2, "test1"
    # assert solve(1, 2, 3) == -1, "test2"
    # assert solve(1, 2, 4) == 4, "test3"
    # assert solve(1, 2, 2) == 2, "test4"
    # assert solve(2, 2, 2) == 2, "test4"
    # assert solve(3, 3, 3) == 3, "test4"
    # assert solve(1, 3, 3) == 3, "test4"
    # assert solve(1, 1, 1) == 1, "test4"
    # assert solve(2, 1, 2) == 2, "test4"
    # assert solve(3, 1, 2) == -1, "test4"
    # assert solve(2, 1, 3) == -1, "test4"
    # assert solve(200, 100, 300) == -1, "test4"
    # assert solve(200, 100, 300) == -1, "test4"
    # assert solve(2, 1, 3) == 1, "test44"
    print(solve(1, 2, 4))


# read_input()
tests()
