# 2021-11-19
import collections
from typing import List
"""
# Definition for Employee.
class Employee:
    def __init__(self, id: int, importance: int, subordinates: List[int]):
        self.id = id
        self.importance = importance
        self.subordinates = subordinates
"""
class Solution:
    def getImportance(self, employees: List['Employee'], id: int) -> int:
        emp = {}
        for e in employees:
            emp[e.id] = e

        q = collections.deque([id])
        ans ,seen = 0, set()
        while q:
            e = emp[q.pop()]
            seen.add(e.id)
            ans += e.importance
            q.extend(e.subordinates)
        return ans
