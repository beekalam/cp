# 2021-11-16
from typing import List
class Solution:
    def evaluate(self, s: str, knowledge: List[List[str]]) -> str:
        dic = dict(knowledge)
        prev_word =""
        words = []
        for c in s:
            if c == "(":
                words.append(prev_word)
                prev_word = ""
            elif c == ")":
                if prev_word in dic:
                    words.append(dic[prev_word])
                else:
                    words.append("?")
                prev_word = ""
            else:
                prev_word += c
        words.append(prev_word)
        ans = "".join(words)
        return ans



s = Solution()
ss = "(name)is(age)yearsold"
assert  s.evaluate(ss,[["name","bob"],["age","two"]])=="bobistwoyearsold","test1"



