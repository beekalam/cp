import re
class Solution:
    def areNumbersAscending(self, s: str) -> bool:
        nums = list(map(int,re.findall(r'\d+', s)))
        if len(nums) == 0:
            return False
        for i in range(1, len(nums)):
            if nums[i] <= nums[i-1]:
                return False
        return True


s = Solution()
ss = "1 box has 3 blue 4 red 6 green and 12 yellow marbles"
assert s.areNumbersAscending(ss) == True,"test1"

ss = "hello world 5 x 5"
assert s.areNumbersAscending(ss) == False,"test2"
