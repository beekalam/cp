# @notsolved: 2021-11-21
import collections
from typing import List,Optional
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def insertIntoBST(self, root: Optional[TreeNode], val: int) -> Optional[TreeNode]:
        if root is None:
            root = TreeNode(val)
            return root
        tmp = root
        while tmp:
            if val > tmp.val:
                if tmp.right:
                    tmp = tmp.right
                else:
                    tmp.right = TreeNode(val)
                    break
            elif val < tmp.val:
                if tmp.left:
                    tmp = tmp.left
                else:
                    tmp.left = TreeNode(val)
                    break
        return root
    def find(self,root:Optional[TreeNode], key:int)->Optional[List[TreeNode]]:
       parents = {}
       q = collections.deque()
       q.append(root)
       while q:
            node = q.popleft()
            if node.left:
                parents[node.left] = node
            if node.right:
                parents[node.right] = node
            if node.left and node.left.val == key:
                return [parents[node], node]
            if node.right and node.right.val == key:
                return [parents[node],node]

    def tolist(self, root: Optional[TreeNode]) -> List[int]:
        arr = []
        q = collections.deque()
        if root:
            q.append(root)
        while q:
            t = q.popleft()
            arr.append(t.val)
            if t.right:
                q.append(t.right)
            if t.left:
                q.append(t.left)
        return arr
    def deleteNode(self, root: Optional[TreeNode], key: int) -> Optional[TreeNode]:
        pparent,parent = self.find(root,key)
        if parent.left and parent.left.val == key:
            tmp = parent
            if pparent.left == parent:
                pparent.left = parent.left
            else:
                pparent.right = parent.left
            for i in self.tolist(tmp.right):
                self.insertIntoBST(root,i)

        if parent.right and parent.right.val == key:
            tmp = parent

            if pparent.left == parent:
                pparent.left = parent.right
            else:
                pparent.right = parent.right
            for i in self.tolist(tmp.left):
                self.insertIntoBST(root,i)
