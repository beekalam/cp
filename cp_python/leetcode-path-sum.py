# 2022-01-09
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def hasPS(self, root, targetSum, current_sum):
        if root is None:
            return False
        if root.right is None and root.left is None and targetSum == current_sum + root.val:
            return True
        return self.hasPS(root.left,targetSum, current_sum + root.val) \
                or self.hasPS(root.right, targetSum, current_sum + root.val)
    def hasPathSum(self, root: Optional[TreeNode], targetSum: int) -> bool:
        return self.hasPS(root,targetSum,0)
