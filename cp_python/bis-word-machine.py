class Solution:
    def solve(self, ops):
        st = []
        ans = 0
        for op in ops:
            if op.isnumeric():
                st.append(int(op))
            elif op == "POP":
                if len(st) == 0:
                    return -1
                else:
                    st.pop()
            elif op == "DUP":
                if len(st) == 0:
                    return -1
                else:
                    st.append(st[-1])
            elif op == "+":
                if len(st) < 2:
                    return -1
                else:
                    st.append(st.pop()+ st.pop())
            elif op == "-":
                if len(st) < 2:
                    return -1
                else:
                    st.append(st.pop() - st.pop())
        return st[-1]
