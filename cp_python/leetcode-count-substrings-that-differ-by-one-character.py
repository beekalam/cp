# @notsolved
import string
from collections import Counter


class Solution:
    def countSubstrings(self, s: str, t: str) -> int:
        def diff(ss):
            ret = 0
            for k in range(lt):
                dif = 0
                for m in range(len(ss)):
                    if k + m >= lt:
                        dif = -1
                        break
                    if ss[m] != t[k + m]:
                        dif += 1
                if dif == 1:
                    ret += 1
            return ret

        ls, lt = len(s), len(t)
        ans = 0
        for i in range(ls):
            for j in range(i + 1, ls):
                ans += diff(s[i:j + 1])

        # i, j, ans = 0, 0, 0
        # while i < lt and j < ls:
        #     if s[i] != s[j]:
        #         m, n = i + 1, j + 1
        #         while m < lt and n < ls and t[m] == s[n]:
        #             ans += 1
        #             m, n = i + 1, j + 1
        #         m, n = i - 1, j - 1
        #         while m >= 0 and n >= 0 and t[m] == s[n]:
        #             ans += 1
        #             m, n = i - 1, j - 1
        #
        #     i, j = i + 1, j + 1
        sc = Counter(s)
        tc = Counter(t)
        s_sum, t_sum = sum(sc.values()), sum(tc.values())
        for c in string.ascii_lowercase:
            if c in sc:
                ans += sc[c] * (t_sum - tc[c] if c in tc else 0)
        print("ans: ", ans)
        return ans


s = Solution()
# assert s.countSubstrings("aba", "baba") == 6, "test1"
assert s.countSubstrings("ab", "bb") == 3, "test2"
