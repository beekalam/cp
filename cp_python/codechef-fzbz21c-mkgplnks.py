# 2021-11-26
import itertools

t = int(input())
for i in range(t):
    n = input()
    s = input()
    bc, wc = 0, 0
    for c, _ in itertools.groupby(s):
        if c == 'W':
            wc += 1
        else:
            bc += 1
    ans = min(bc, wc)
    print(ans)
