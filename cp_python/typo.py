s = input().strip()
t = input().strip()


def solve(s, t):
    if s == t:
        print("Yes")
        return
    diff = []
    for i in range(len(s)):
        if s[i] != t[i]:
            diff.append(i)

    if len(diff) != 2:
        print("No")
        return

    i, j = diff
    if abs(i - j) != 1:
        print("No")
        return

    s = list(s)
    s[i], s[j] = s[j], s[i]
    if "".join(s) == t:
        print("Yes")
        return

    print("No")


solve(s, t)
