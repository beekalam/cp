# @notsolved: 2021-12-24
# https://atcoder.jp/contests/abc129/tasks/abc129_c

n,m = list(map(int, input().split()))
broken = set([0,1])
for _ in range(m):
	i = int(input())
	broken.add(i)
	if i-1 >= 0:
		broken.add(i-1)
	if i+1 < n:
		broken.add(i+1)


ans = pow(2, n - len(broken))
print(ans % 1000000007)
