# 2021-12-31
class RangeSum:
    def __init__(self, nums):
        self.nums = nums
        self.prefix = []
        rs = 0
        for i in range(len(nums)):
            rs += self.nums[i]
            self.prefix.append(rs)
        print(self.prefix)

    def total(self, i, j):
        ans = self.prefix[j-1] - self.prefix[i] + self.nums[i]
        return ans
