import string


class Solution_first:
    def minimize(self, m, c, _except=[]):
        if c in _except:
            return c
        if c in m:
            _except.append(c)
            _min = None
            for k in m[c]:
                if _min is None:
                    _min = min(k, self.minimize(m, k, _except))
                else:
                    _min = min(_min, k, self.minimize(m, k, _except))
            _except.pop()
            return _min
        else:
            return c

    def buildMap(self, a, b):
        self.m = {}
        for i, c in enumerate(a):
            cc = b[i]
            if c in self.m.keys():
                self.m[c].append(cc)
            else:
                self.m[c] = [cc]
            if cc in self.m.keys():
                self.m[cc].append(c)
            else:
                self.m[cc] = [c]

    def solve(self, a, b, target):
        self.buildMap(a, b)
        alpha = list(set(a + b))
        for c in alpha:
            self.m[c] = list(set(self.m[c]))
            self.m[c] = self.minimize(self.m, c)
        ans = ""
        for c in target:
            if c in self.m:
                ans += min(c, self.m[c])
            else:
                ans += c

        # print(m)
        return ans


class Solution_xx:
    def find(self, c):
        if self.papa[c] != c:
            self.papa[c] = self.find(self.papa[c])
        return self.papa[c]

    def solve(self, a, b, target):
        self.papa = list(range(0, 26))
        for i, c in enumerate(a):
            p1 = self.find(ord(c) - ord('a'))
            p2 = self.find(ord(b[i]) - ord('a'))
            if p1 < p2:
                self.papa[p2] = p1
            else:
                self.papa[p1] = p2
            print(self.papa)
        ans = ""
        for c in target:
            p1 = self.find(ord(c) - ord('a'))
            ans += chr(ord('a') + p1)
        return ans


class Solution:
    def solve(self, a, b, target):
        p = {}
        for x in "abcdefghijklmnopqrstuvwxyz":
            p[x] = x

        def rof(x):
            if x == p[x]:
                return x
            p[x] = rof(p[x])
            return p[x]

        for i, x in enumerate(a):
            u = rof(x)
            v = rof(b[i])
            if u != v:
                p[v] = u
        q = {}
        for x in "abcdefghijklmnopqrstuvwxyz":
            y = rof(x)
            q[y] = min(q.get(y, y), x)
        return "".join(q[rof(x)] for x in target)


s = Solution()

a = "axz"
b = "xdz"
target = "ddxz"

print(s.solve(a, b, target))
a = "abc"
b = "def"
target = "xyz"
print(s.solve(a, b, target))

a = "mgmgmgmgmssssmgmggsgmsggmmgmgmmssmgmgggmsgmmmgsssmsggmmssmgmgggssssgmmsmmsgmmggggssgsgmmssmsmsmggsmmgmgmmgmsgssmmmssggsgmssmsgggsgmgmsgsssgsggsmgsssssssgssmggsmmssgmgmgsggssmgmmmssmgggmsgggsssgsssmgssgsssggssmsggmggmmsmsmgsmmmmgsmgmgmmggsssmgsgggmmmgssmgsssmsggmsmmmmmggmmggmssggmmsgmsgmgmmggssssmgsssmsggmgmsggmgmgmssmgssgssssmsmgsmmmmmmgsgmmggsmsmmmsmsggmgmgmgmggssmsssgmssmmssmsssgmggssssgssmsggmmggsmmgggsmmmsmsmmmmmsggmgsggsmsgsgssssgmggsgmgssmgmsgggmgsmsssgsmggssgsmgmmgsggmggmgsssgsmsgmmsggmgmmmmgsmsmssgmmmgmggmsmssmmmgsmggsmmmsmssggmgggggmmggsgsggsgsmssmmggmsmsgmgggggmsmgmmsmgmmsmsggmgggmgggsssmgmgsmmmsgssmgssssggmgsmmsgsgggmsmmsssgmssmgssgsgggsgmgssmmgssmsmgmgmsssggmmmmgssgggmgmssgsgssmg"
b = "ssggmggggmssggmggmsmggmgmsmsmgmgmggsgmmmmggsgssssgggmmsssggsmggsmmgmsmsmgmsssmgmssgssmggsmgsmgmsgmgsmmsmgsgmgmgsggsgggmmssgssgmsmsgmmsmggmssmssggmssmgmmgmmmggmssmggggssmgsgssssmsmgggsmsmmmsgmsmmsmssgmmsmgsgssmsssmmssggsgsmsmsmmmssggmmsssmmssgsgggmssgsssgmggsggssmsmsgmmsssggmgmssmgggmssssggggggssmgsgsgggsgmgmgsgssssssggmggssggmsssssssssmsmsmssggmmgmmmsgsggmgssssmsmmmggsggsmgmgsmmgmssmsmmmmsmmmssgmgggggmgmggsgmgmmmsmmmmsssmsmsgmgmsgmmmmsmgmmmmggmgmggmgggsmmmmsgmgggsmggmsmmmmmsgmmssmsmgmssgmgssggsmgmgmgsgmggmmmsssggmgmsssmgmmgsgmgmmgsgggmsgmgmsssmsmsmssgssmsgmssmsgsmmssggmsmgsgmgggggsgsmgsmgmmgmmmssmssmggsgmggsggmmsmgssmgssmssmsggmsgssmssmgmmgsgssmmggmggmsmssgsgmgsmsssgmsgsmggsgsmsgsgsgggggmsss"
target = "mhhltkvgtuowpiljxckvrknoeebkldzzmvkqqcntvqzxnywbbtjckmytqrzwujvlyduiwyqfvzqeccxequtgkrtqjjbcuxkpqbsmxdhwhviqzrgkdyopkwiuvzzdpxtmivozejjkfiwcsmacuzvxtxqlbbxfskebmvbfrlbapayxwvwbdtwwffkjiyxawxnhlqnrwyevyevmqxymuazsahemmqvaalbwpgzknvdhycvwpxouzchaarzmumohkzhgvxxmijlibjplojstzivdvzxkmivtosewsqmgdqfxcuzptjxgzbdqgemydfaxfwqdeshgzndvhfhpmkbpfhntsdaifcabgfttmcqbccpugfuwjsbiunwcacvnvhtpaioinfjigttzpoztldljjydhcmscueyeycdunqcvgjembifvpotjxclnosdohimtmokdbwbvxwtkivxrxisekvtjyhdgkgymisgksrlhpgniefnyrorsbekcfxumvlojofrmqmatyzifprcvpivqsiillqdvtoirxosxwsgonstmurhhkszokaiqmgcsnnfxbjiledhfgfpctcehlyanbvompquzhsckxabbdgmeororiewnubqkfmzuuszcckzhgryjcfzdcmlqroqrovtcglxuvlmiltxzmycajmplhsnihghiabjofkzgpeeluulcjqnsgsjzytcbnmphwvlypmjurgfayfzqewqwvubcwtclhfsfvqqfdxcarmovgdzfueftwgmwtglmlrcmxalgydclrdrzymsfywsf"
# print(s.solve(a, b, target))
a = "bb"
b = "ab"
target = "ab"
# print(s.solve(a, b, target))
