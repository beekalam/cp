# 2021-11-29
s = input()
a = int(s[0] + s[1])
b = int(s[2] + s[3])
def is_valid_month(m):
    return m >=1 and m <= 12
def is_valid_year(y):
    return y>=0 and y <= 99

mmyy_condition = is_valid_month(a) and is_valid_year(b)
yymm_condition = is_valid_year(a) and is_valid_month(b)

if mmyy_condition and yymm_condition:
    print("AMBIGUOUS")
elif mmyy_condition:
    print("MMYY")
elif yymm_condition:
    print("YYMM")
else:
    print("NA")
