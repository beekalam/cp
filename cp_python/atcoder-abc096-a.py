# 2021-11-29
import datetime

m, d = list(map(int, input().split()))
start = datetime.datetime.strptime("2018-1-1", "%Y-%m-%d")
ans = 0
while start.year == 2018 and m >= start.month:
    if m == start.month and d >= start.day and start.day == start.month:
        ans += 1
    elif m > start.month and start.day == start.month:
        ans += 1
    start += datetime.timedelta(days=1)
print(ans)
