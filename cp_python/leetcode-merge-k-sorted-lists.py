# 2021-12-22
from ListNode import ListNode
from typing import List
from typing import Optional
import heapq


class Solution:
    def mergeKLists(self, lists: List[Optional[ListNode]]) -> Optional[ListNode]:
        if len(lists) == 0:
            return None
        ans = []
        heapq.heapify(ans)
        for head in lists:
            while head:
                heapq.heappush(ans, head.val)
                head = head.next
        tmp = ListNode()
        head = tmp
        while ans:
            val = heapq.heappop(ans)
            tmp.next = ListNode(val)
            tmp = tmp.next

        return head.next
