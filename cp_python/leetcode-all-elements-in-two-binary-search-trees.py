# https://leetcode.com/problems/all-elements-in-two-binary-search-trees/submissions/
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution(object):
    #@snippet tree to array
    def extractNodes(self,root):
        stack =[]
        if root:
            stack = [root]
        ans = []
        while stack:
            node = stack.pop()
            ans.append(node.val)
            if node.right:
                stack.append(node.right)
            if node.left:
                stack.append(node.left)
        return ans

    def getAllElements(self, root1, root2):
        """
        :type root1: TreeNode
        :type root2: TreeNode
        :rtype: List[int]
        """
        arr = self.extractNodes(root1)
        arr.extend(self.extractNodes(root2))
        return sorted(arr)
