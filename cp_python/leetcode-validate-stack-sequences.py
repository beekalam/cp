# 2021-11-11
from typing import List


class Solution:
    def validateStackSequences(self, pushed: List[int], popped: List[int]) -> bool:
        i, j = 0, 0
        m, n = len(pushed), len(popped)
        stack = []

        while i < n or j < m:
            if stack:
                if stack[-1] == popped[i]:
                    i += 1
                    stack.pop()
                elif j < m:
                    stack.append(pushed[j])
                    j += 1
                else:
                    return False
            else:
                if j < m:
                    stack.append(pushed[j])
                    j += 1
                else:
                    return False

        return len(stack) == 0


s = Solution()
pushed = [1, 2, 3, 4, 5]
popped = [4, 5, 3, 2, 1]
assert s.validateStackSequences(pushed, popped) == True, "test1"
