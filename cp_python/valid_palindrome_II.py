import unittest
from collections import Counter


class Solution(object):
    def isPalindrome(self, s):
        i, j = 0, len(s) - 1
        while i < j:
            if s[i] != s[j]:
                return False
            i += 1
            j -= 1
        return True

    def validPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        i, j = 0, len(s) - 1
        while i < j:
            print(f"i: {i}  j: {j}")
            if s[i] != s[j]:
                a, b = s[i:j], s[i + 1:j]
                if len(a) == 1 and len(b) == 1:
                    if a == b:
                        return True
                    else:
                        return False
                print("a: ", a, "\nb: ", b)
                return self.isPalindrome(a) or self.isPalindrome(b)

            i += 1
            j -= 1
        return True


class TestValidPalindrome(unittest.TestCase):

    def setUp(self) -> None:
        self.sol = Solution()

    def test_1(self):
        self.assertEqual(True, self.sol.validPalindrome("aba"))

    def test_2(self):
        self.assertEqual(True, self.sol.validPalindrome("abca"))

    def test_3(self):
        self.assertEqual(False, self.sol.validPalindrome("abc"))

    def test_4(self):
        self.assertEqual(True, self.sol.validPalindrome("deeee"))
