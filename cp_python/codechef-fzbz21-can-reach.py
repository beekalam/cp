# 2021-11-26
import math

t = int(input())
for i in range(t):
    x, y, k = list(map(int, input().split()))
    if abs(x) % k == 0 and abs(y) % k == 0:
        print("yes")
    else:
        print("no")
