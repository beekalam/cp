class Solution:
    def solve(self, s):
        depth = 0
        xcount = 0
        max_depth = 0
        n = len(s)
        m = {}
        for i, c in enumerate(s):
            if c == '(':
                m[depth] = m.get(depth, 0) + xcount
                depth += 1
                max_depth = max(depth, max_depth)
                xcount = 0
            elif c == 'X':
                xcount += 1
            else:  # ')' brackets
                m[depth] = m.get(depth, 0) + xcount
                xcount = 0
                depth -= 1
        ans = []
        # print(m)
        for i in range(1, max_depth + 1):
            if i in m.keys():
                ans.append(m[i])
        return ans


s = Solution()
ss = "(XX(XX(X))X)"
print(s.solve(ss))
