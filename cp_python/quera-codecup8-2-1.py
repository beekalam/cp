# https://quera.org/contest/assignments/64033/problems/226341
input = [c for c in str(input())]
table = [[0 for i in range(8)] for i in range(2)]
n, m, i, j = 2, 8, 1, 0
table[i][j] = 1

dead = False
for c in input:
    if j + 1 >= m:
        dead = True
        break
    if c == 'L' and i - 1 < 0:
        dead = True
        break
    elif c == 'R' and i + 1 >= n:
        dead = True
        break

    j += 1
    if c == 'L':
        i -= 1
    elif c == 'R':
        i += 1
    table[i][j] = 1

if dead:
    print("DEATH")
else:
    print(''.join([str(i) for i in table[0]]))
    print(''.join([str(i) for i in table[1]]))
