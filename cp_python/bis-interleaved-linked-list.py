# class LLNode:
#     def __init__(self, val, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def solve(self, l0, l1):
        d = LLNode(0)
        head = d
        while l0 or l1:
            if l0:
                d.next = l0
                l0 = l0.next
                d = d.next
            if l1:
                d.next = l1
                l1 = l1.next
                d = d.next
        return head.next
