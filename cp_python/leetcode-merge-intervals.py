from operator import itemgetter
from typing import List


class Solution:
    def overlaps(self, a, b):
        return a[0] <= b[1] <= a[1] or a[0] <= b[0] <= a[1]

    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        intervals = sorted(intervals, key=itemgetter(0))
        print(intervals)
        tmp = []
        ans = []
        for i, item in enumerate(intervals):
            if len(tmp) == 0:
                tmp = item
            elif self.overlaps(tmp, item) or self.overlaps(item, tmp):
                tmp = [min(tmp[0], item[0]), max(tmp[1], item[1])]
            else:
                ans.append(tmp)
                tmp = item
        if len(tmp) != 0:
            ans.append(tmp)
        print(ans)
        return ans


s = Solution()
assert s.merge([[1, 3], [2, 6], [8, 10], [15, 18]]) == [[1, 6], [8, 10], [15, 18]], "test1"
