# @notsolved: 2021-12-25
import string
from typing import List


class Solution:
    def has_word(self, s: str, dic: List[str]) -> bool:
        if len(s) == 0:
            return True

        i = 1
        while i <= len(s):
            if s[0:i] in dic:
                s = s[i:]
                print(s)
                return self.has_word(s, dic)
            i += 1

    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        return self.has_word(s, wordDict)


s = Solution()
# assert s.wordBreak("leetcode", ["leet", "code"]) == True, "test2"
assert s.wordBreak("aaaaaaa", ["aaaa", "aaa"]) == True, "test3"
