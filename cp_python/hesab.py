import datetime
from functools import cmp_to_key


def time_cmp(a, b):
    if a[2] > b[2]:
        return -1
    else:
        return 1


n = int(input())
transactions = []
for i in range(n):
    transaction = input().split()
    time_component = transaction[2].split(':')
    hour, minute = int(time_component[0]), int(time_component[1])
    transaction[2] = datetime.time(hour, minute)
    transactions.append(tuple(transaction))

transactions.sort(key=cmp_to_key(time_cmp), reverse=True)

sum_dep = 0
sum_wit = 0
min_diff = 0
invalid = False
for t in transactions:
    if t[-1] == 'FAIL':
        if sum_dep >= sum_wit:
            invalid = True
            break
        continue

    if t[0] == 'DEP':
        sum_dep += int(t[1])
    else:
        sum_wit += int(t[1])
    min_diff = min(min_diff, sum_dep - sum_wit)

if invalid:
    print("DOROGHE")
else:
    print(abs(min_diff))
