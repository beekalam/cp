# 2021-11-17
class Solution:
    def solve(self, nums):
        carry = 1
        for i in range(len(nums)-1,-1,-1):
            s = nums[i] + carry
            carry = 0
            if s > 9:
                carry = 1
                s = s - 10
            nums[i] = s
        if carry != 0:
            nums.insert(0,carry)
        return nums
