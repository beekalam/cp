def longest_common_substring(a, b):
    ans, mi, mj = 0, 0, 0
    for i in range(len(a)):
        for j in range(len(b)):
            if a[i] == b[j]:
                count, m, n = 0, i, j
                while m < len(a) and n < len(b) and a[m] == b[n]:
                    m += 1
                    n += 1
                    count += 1
                if count > ans:
                    ans = count
                    mi = i
                    mj = j
    return mi, mj, ans


class Solution:

    def solve(self, s):
        m = len(s)
        if m < 2:
            return 0
        if m == 2:
            return 1 if s[0] == s[1] else 0
        if m % 2 == 0:
            n = m / 2
            n = int(n)
            _, _, length = longest_common_substring(s[0:n], s[n:])
            if length > 0:
                return length
            else:
                return max(self.solve(s[0:n]), self.solve(s[n:]))
        else:
            n = (m - 1) / 2
            n = int(n)
            _, _, length = longest_common_substring(s[0:n], s[n + 1:])
            if length > 0:
                return length
            else:
                return max(self.solve(s[0:n + 1]), self.solve(s[n:]))


s = Solution()
assert s.solve("abcdzabcd") == 4, "test1"
assert s.solve("abcdefg") == 0, "test2"
assert s.solve("") == 0, "test3"
assert s.solve("a") == 0, "test3"
