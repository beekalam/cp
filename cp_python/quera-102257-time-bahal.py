# @notsolved: 2021-11-30
from functools import cmp_to_key

n = int(input())
a = []
for i, j in zip(input().split(), input().split()):
    a.append((int(i), int(j)))

k = int(input())


def sort(i, j):
    return (j[0] * j[1]) - (i[0] * i[1])


a.sort(key=cmp_to_key(sort))
print(a)
