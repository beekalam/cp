import string


class Solution:
    def freqAlphabets(self, s: str) -> str:
        mapping = {}
        for i, c in enumerate(string.ascii_lowercase):
            if 0 <= i <= 8:
                mapping[str(i + 1)] = c
            else:
                mapping[str(i + 1) + "#"] = c
        # print(mapping)
        n = len(s)
        i = 0
        ans = ""
        while i < n:
            if i + 3 <= n and s[i:i + 3] in mapping:
                # print(s[i:i + 3])
                ans += mapping[s[i:i + 3]]
                i += 3
            elif i + 2 <= n and s[i:i + 2] in mapping:
                # print(s[i:i + 2])
                ans += mapping[s[i:i + 2]]
                i += 2
            else:
                # print(s[i])
                ans += mapping[s[i]]
                i += 1
        return ans


s = Solution()
# assert s.freqAlphabets("10#11#12") == "jkab", "test1"
assert s.freqAlphabets(
    "12345678910#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#") == \
       "abcdefghijklmnopqrstuvwxyz", "test2"
