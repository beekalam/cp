# 2021-11-13
from typing import Optional
import collections
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def __init__(self):
        self.cache = {}
    def subtree_has_one(self,root):
        if root is None:
            return False
        if root in self.cache:
            return self.cache[root]

        if root.val == 1:
            self.cache[root] = True
            return self.cache[root]

        self.cache[root] = self.subtree_has_one(root.left) or self.subtree_has_one(root.right)
        return self.cache[root]

    def pruneTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:


        q = collections.deque()
        q.append(root)
        while q:
            r = q.popleft()
            if r.left:
                if not self.subtree_has_one(r.left):
                    r.left = None
                else:
                    q.append(r.left)

            if r.right:
                if not self.subtree_has_one(r.right):
                    r.right = None
                else:
                    q.append(r.right)
        if root.left is None and root.right is None and root.val != 1:
            root = None
        return root
