# 2021-11-11
import math
from typing import List


class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        stack = []
        for t in tokens:
            if t in ['+', '-', '*', '/']:
                evl = 0
                a = stack.pop()
                b = stack.pop()
                if t == "+":
                    evl = b + a
                elif t == '-':
                    evl = b - a
                elif t == '*':
                    evl = b * a
                elif t == '/':
                    evl = int(b / a)
                stack.append(evl)
            else:
                stack.append(int(t))
        ans = stack[-1]
        return ans


s = Solution()
tokens = ["2", "1", "+", "3", "*"]
assert s.evalRPN(tokens) == 9, "test1"

tokens = ["4", "13", "5", "/", "+"]
assert s.evalRPN(tokens) == 6, "test2"

tokens = ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]
assert s.evalRPN(tokens) == 22, "test3"
