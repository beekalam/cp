from typing import List


class Solution:
    def intervalIntersection(self, firstList: List[List[int]], secondList: List[List[int]]) -> List[List[int]]:
        def overlaps(a, b):
            return (a[0] <= b[0] and b[0] <= a[1]) or (a[0] <= b[1] and b[1] <= a[1])

        i, j = 0, 0
        fn, sn, = len(firstList), len(secondList)
        res = []
        while i < fn and j < sn:
            p = firstList[i]
            q = secondList[j]

            if overlaps(p, q):
                res.append([max(p[0], q[0]), min(p[1], q[1])])
                i += 1
            elif overlaps(q, p):
                res.append([max(p[0], q[0]), min(p[1], q[1])])
            if q[0] > p[1]:
                i += 1
                continue
            if p[0] > q[1]:
                j += 1

            print(f"i: {i} j: {j}")
            print(f"i: {p} j: {q}")
            print(".......................")
        print(res)

        return res


s = Solution()
firstList = [[0, 2], [5, 10], [13, 23], [24, 25]]
secondList = [[1, 5], [8, 12], [15, 24], [25, 26]]
assert s.intervalIntersection(firstList, secondList) == [[1, 2], [5, 5], [8, 10], [15, 23], [24, 24], [25, 25]], "test1"
