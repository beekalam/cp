# 2021-12-21
n = int(input())
divisors = []
for i in range(1,n):
    if n % i == 0:
        divisors.append(i)
if sum(divisors) == n:
    print("YES")
else:
    print("NO")
