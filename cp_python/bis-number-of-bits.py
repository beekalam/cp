# 2021-12-20
class Solution:
    def solve(self, n):
        ans = 0
        while n:
            if n & 1:
                ans += 1
            n= n >> 1
        return ans
