class Solution:
    def solve(self, nums):
        nums.sort()
        print(nums)
        n = len(nums)
        ans = 0
        count = 0
        for i in range(n):
            if i + 1 < n and nums[i] == nums[i + 1]:
                count += 1

            if i + 1 < n and nums[i] + 1 == nums[i + 1]:
                ans += count + 1 if count > 0 else 1
                count = 0
            if i + 1 < n and nums[i] != nums[i+1]:
                count = 0
        return ans
