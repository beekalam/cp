import math
from sys import stdin


def read_int():
    return int(input().strip())


def solve(n, m, a, b):
    if n == a or n == b or m == a or m == b:
        print(1)
    else:
        ans = min(n, m) / max(a, b)
        ans = int(math.ceil(ans))
        print(ans)


n, m, a, b = read_int(), read_int(), read_int(), read_int()
solve(n, m, a, b)
# solve(3,3,1,3)
# solve(2,2,1,1)
# solve(3,3,2,2)
