from collections import deque


class Solution:
    def add(self, a, b):
        ans = []
        for i in range(3):
            s = a[i] + b[i]
            if s >= 10:
                s = s - 10
            ans.append(s)
        # print(ans)
        return int("".join([str(i) for i in ans]))

    def solve(self, a, b):
        a = deque(a)
        b = deque(b)
        has_ans = False
        for i in range(6):
            for j in range(6):
                b.rotate(1)
                if self.add(a, b) % 6 == 0:
                    has_ans = True
            a.rotate(1)
        if has_ans:
            return 'Boro joloo :)'
        else:
            return 'Gir oftadi :('

    def solve_cmd(self):
        a = map(int, input().split())
        b = map(int, input().split())
        print(s.solve(a, b))


s = Solution()
s.solve_cmd()
# assert s.solve([1, 8, 9, 7, 2], [3, 4, 5, 0, 6]) == 'Boro joloo :)'
# assert s.solve([1, 3, 5, 7, 9], [0, 2, 4, 6, 8]) == 'Gir oftadi :('
