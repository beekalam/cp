import bisect
n,q = list(map(int, input().split()))
arr = list(map(int, input().split()))
arr.sort()
for i in range(q):
    j = int(input())
    idx = bisect.bisect_left(arr,j)
    print(n - idx)