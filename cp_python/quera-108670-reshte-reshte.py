import collections


class Solution:
    def first_condition(self, selected, sus):
        x = len(selected) == len(sus) + 1 and len(set(selected).difference(set(sus))) == 1
        y = len(selected) + 1 == len(sus) and len(set(sus).difference(set(selected))) == 1
        return x or y

    def third_condition(self, a, b):
        return len(a) == len(b) and a.lower() == b.lower()

    def second_condition(self, a, b):
        # print(a, b)
        if len(a) != len(b):
            return False
        diff = 0
        for i, j in zip(a, b):
            if i != j and i.lower() != j.lower():
                diff += 1
        return diff == 1

    # return len(a) == len(b) and len(set(a).difference(set(b))) == 1

    def solve(self, suspicious, selected):
        for s in selected:
            sum = 0
            print(s)
            for sus in suspicious:
                print(sus)
                sum += self.first_condition(s, sus)
                print(sum)
                sum += self.second_condition(s, sus)
                print(sum)
                sum += self.third_condition(s, sus)
                print(sum)
            print(sum)
            print("....................................")

    def solve_cmd(self):
        k, n = list(map(int, input().split()))
        sus = []
        sel = []
        for i in range(k):
            sus.append(input())
        for i in range(n):
            sel.append(input())
        # print(sel)
        # print(sus)
        return self.solve(sus, sel)


s = Solution()
# s.solve_cmd()
# s.solve([hamKaran,system],[systemi])

# assert s.first_condition('hamKarani', 'hamKaran') == True, "test1"

# assert s.second_condition('hamkaran', 'hamKaran') == False, "test1"
#
# assert s.second_condition('hamKaran', 'system') == False, "test1"
# assert s.second_condition('system', 'hamKaran') == False, "test1"
#
# assert s.first_condition('hamKarani', 'hamKaran') == True, "test1"
# assert s.first_condition('sstem', 'system') == True, "test2"
# assert s.first_condition('hamkaran', 'hamKaran') == False, "test3"
# assert s.third_condition("hamkaran", "hamKaran") == True, "test4"

# assert s.third_condition("hamkarani", "hamKaran") == False, "test4"
# assert s.first_condition("hamkarani", "hamKaran") == False, "test4"
# assert s.second_condition("hamkarani", "hamKaran") == False, "test4"

# assert s.first_condition("pYstem", "system") == False, "test4"
# assert s.first_condition("pYstem", "systemi") == False, "test4"
# assert s.first_condition("pYstem", "system") == False, "test4"
# assert s.second_condition("pYstem", "systemi") == False, "test4"
assert s.second_condition("pYstem", "system") == False, "test4"
# assert s.third_condition("pYstem", "systemi") == False, "test4"
# assert s.third_condition("pYstem", "system") == False, "test4"
#
# assert s.second_condition("pystem", "system") == True, "test4"
# assert s.first_condition("pystem", "system") == False, "test4"
# assert s.third_condition("pystem", "system") == False, "test41"
#
# assert s.second_condition("pystemi", "systemi") == True, "test4"
# assert s.third_condition("pystemi", "systemi") == False, "test4"
# assert s.first_condition("pystemi", "systemi") == False, "test4"
#
# assert s.first_condition("systema", "system") == True, "test4"
# assert s.second_condition("systema", "systemi") == True, "test41"
sssss = """
3 1
hamKaran
system
systemi
hamkaran
"""
