# 2021-11-19
from typing import List


class Bank:

    def __init__(self, balance: List[int]):
        self.accounts = balance

    def transfer(self, account1: int, account2: int, money: int) -> bool:
        if not self.is_valid_account(account1) or not self.is_valid_account(account2):
            return False
        if not self.can_withdraw(account1, money):
            return False

        if not self.withdraw(account1, money):
            return False
        res = self.deposit(account2, money)
        # print("res: ", res)
        return res

    def deposit(self, account: int, money: int) -> bool:
        account -= 1
        if self.is_valid_account(account) and account <= len(self.accounts):
            self.accounts[account] += money
            return True
        else:
            return False

    def can_withdraw(self, account: int, money: int) -> bool:
        if self.is_valid_account(account) and self.accounts[account - 1] >= money:
            return True
        return False

    def is_valid_account(self, account) -> bool:
        return account - 1 < len(self.accounts)

    def withdraw(self, account: int, money: int) -> bool:
        if self.can_withdraw(account, money):
            self.accounts[account - 1] -= money
            return True
        return False


# Your Bank object will be instantiated and called as such:
balance = [10, 100, 20, 50, 30]
obj = Bank(balance)
assert obj.withdraw(3, 10) == True, "test1"
assert obj.transfer(5, 1, 20) == True, "test2"
assert obj.deposit(5, 20) == True, "test3"
assert obj.transfer(3, 4, 15) == False, "test4"
assert obj.withdraw(10, 15) == False, "test5"
