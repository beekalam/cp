# 2021-11-28
class Solution:
    def solve(self, n, k, s):
        def rotate(char):
            ret = ord(char) + 1
            if ret > ord('z'):
                return 'a'
            return chr(ret)

        def encode(string):
            s = string[-1] + string[0:len(string) - 1]
            res = ""
            for c in s:
                res += rotate(c)
            return res

        for _ in range(k):
            s = encode(s)
        return s

    def solve_cmd(self):
        n = int(input())
        k = int(input())
        s = input()
        print(self.solve(n, k, s))


s = Solution()
s.solve_cmd()
# assert s.solve(3,1,"abz") == 'abc', 'test1'
# assert s.solve(4,5,"abcd") == 'ifgh', 'test2'
# assert s.solve(1, 1, "a") == 'b', 'test2'
