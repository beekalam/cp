class Solution(object):
    def getNext(self, word):
        for w in word:
            for c in w:
                yield c

    def solve1(self, word1, word2):
        w1 = self.getNext(word1)
        w2 = self.getNext(word2)
        while True:
            a = next(w1, None)
            b = next(w2, None)
            if a is None and b is not None:
                return False
            if a is not None and b is None:
                return False
            if a is None and b is None:
                break
            if a and b and a != b:
                return False
        a = next(w1, None)
        b = next(w2, None)
        if a is not None or b is not None:
            return False
        return True

    def solve2(self, word1, word2):
        i = ii = j = jj = 0
        n, m = len(word1), len(word2)
        while i < n and j < m:
            if word1[i][ii] != word2[j][jj]:
                return False
            ii += 1
            jj += 1
            if ii == len(word1[i]):
                i += 1
                ii = 0
            if jj == len(word2[j]):
                j += 1
                jj = 0
        if i != n or ii != 0:
            return False
        if j != m or jj != 0:
            return False
        return True

    def arrayStringsAreEqual(self, word1, word2):
        """
        :type word1: List[str]
        :type word2: List[str]
        :rtype: bool
        """
        # return self.solve1(word1, word2)
        return self.solve2(word1, word2)


s = Solution()

print(s.arrayStringsAreEqual(["ab", "c"], ["a", "bc"]))


def test_1():
    assert s.arrayStringsAreEqual(["ab", "c"], ["a", "bc"]) == True


def test2():
    assert s.arrayStringsAreEqual(["abc", "d", "defg"], ["abcddef"]) == False
