from collections import deque


class Solution(object):
    def countStudents(self, students, sandwiches):
        """
        :type students: List[int]
        :type sandwiches: List[int]
        :rtype: int
        """
        n = len(students)
        students = deque(students)
        sandwiches = deque(sandwiches)
        rotate_count = 0
        while True:
            if students[0] == sandwiches[0]:
                students.popleft()
                sandwiches.popleft()
            else:
                students.rotate(-1)
                rotate_count += 1
            if rotate_count > n or len(students) == 0:
                break
        return len(students)


s = Solution()
students = [1, 1, 0, 0]
sandwiches = [0, 1, 0, 1]
print(s.countStudents(students, sandwiches))
students = [1, 1, 1, 0, 0, 1]
sandwiches = [1, 0, 0, 0, 1, 1]
print(s.countStudents(students, sandwiches))
