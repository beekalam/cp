# 2021-11-14
from typing import List
from collections import deque


class Solution:
    def timeRequiredToBuy(self, tickets: List[int], k: int) -> int:
        q = deque()
        for i, t in enumerate(tickets):
            q.append((t, i))
        ans = 0
        while q:
            ans += 1
            t, i = q.popleft()
            t -= 1
            if t == 0 and i == k:
                break
            if t > 0:
                q.append((t, i))
        # print("ans: {}".format(ans))
        return ans


s = Solution()
assert s.timeRequiredToBuy([2, 3, 2], 2) == 6, "test1"
assert s.timeRequiredToBuy([5, 1, 1, 1], 0) == 8, "test2"
