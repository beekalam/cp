import {LinkedListNode, LinkedList} from './LinkedList2.js'

class LRUCache {
    constructor(capacity) {
        this.capacity = capacity
        this.cache = {}
        this.cacheVals = new LinkedList()
    }

    Set(key, value) {
        if (this.cache[key]) {
            let node = this.cache[key]
            node.data = value
            this.cacheVals.remove(node)
            this.cacheVals.insertAtTail(node)
        } else {
            this.evictIfNeeded()
            let node = new LinkedListNode(key, value)
            this.cacheVals.insertAtTail(node)
            this.cache[key] = node
        }
    }

    Get(key) {
        if (this.cache[key]) {
            let node = this.cache[key]
            this.cacheVals.remove(node)
            this.cacheVals.insertAtTail(node)
            return node.data
        } else {
            return -1
        }
    }

    evictIfNeeded() {
        if (this.cacheVals.size >= this.capacity) {
            node = this.cacheVals.removeHead()
            delete this.cache[node.key]
        }
    }

    printcache() {
        let node = this.cacheVals.head
        while (node) {
            process.stdout.write("(" + node.key + "," + node.data + ")")
            node = node.next
        }
        console.log()
    }
}

var obj = new LRUCache(2)
obj.Set(10, 20)
obj.printcache()

obj.Set(15, 25)
obj.printcache()

obj.Set(20, 30)
obj.printcache()

obj.Set(25, 35)
obj.printcache()

obj.Set(5, 40)
obj.printcache()


