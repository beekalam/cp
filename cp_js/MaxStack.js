import {MyStack} from './Stack.js'

class MaxStack{
    // Constructor
    constructor(){
        this.max_stack = new MyStack()
        this.main_stack = new MyStack()
    }

    // Removes and returns value from max_stack
    pop(){
        this.max_stack.pop()
        return this.main_stack.pop()
    }

    // Pushes values into max_stack
    push(value){
        this.main_stack.push(value)
        if ((this.max_stack.is_empty() || this.max_stack.top() < value))
            this.max_stack.push(value)
        else
            this.max_stack.push(this.max_stack.top())
    }

    // Returns maximum value from newStack in O(1) Time
    max_rating(){
        if (!(this.max_stack.is_empty()))
            return this.max_stack.top()
    }
}

var ratings = new MaxStack()
ratings.push(5)
ratings.push(0)
ratings.push(2)
ratings.push(4)
ratings.push(6)
ratings.push(3)
ratings.push(10)

console.log(ratings.main_stack.stack_list)
console.log("Maximum Rating: " + ratings.max_rating())

ratings.pop() // Back button effect
console.log("\nAfter clicking back button\n")
console.log(ratings.main_stack.stack_list)
console.log("Maximum value: " + ratings.max_rating())


