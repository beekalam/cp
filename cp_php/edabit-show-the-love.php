<?php

use PHPUnit\Framework\TestCase;

require __DIR__ . '/bootstrap.php';

function showTheLove($arr)
{
    $total = 0;
    $min = min($arr);
    $min_pos = array_search($min, $arr);

    foreach ($arr as $k => &$v) {
        if ($v != $min) {
            $deduction = (($v * 25) / 100);
            $total += $deduction;
            $v -= $deduction;
        }
    }
    $arr[$min_pos] += $total;
    return $arr;
}


class _showTheLoveTest extends TestCase
{
    function test_showTheLove()
    {
        $this->assertEquals(showTheLove([4, 1, 4]), [3, 3, 3]);
        $this->assertEquals(showTheLove([16, 10, 8]), [12, 7.5, 14.5]);
    }
}