<?php
require __DIR__ . '/vendor/autoload.php';

function hasSpaces($str)
{
    return strpos($str, ' ') !== False;
}

assert(hasSpaces("hello") == False, "test1");
assert(hasSpaces("hello, world") == False, "test2");
assert(hasSpaces("") == False, "test3");
