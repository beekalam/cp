<?php
// 2021-12-01
// require_once __DIR__ . "/vendor/autoload.php";

function read_int()
{
    $str = trim(fgets(STDIN));
    return (int)$str;
}

function read_int_array()
{
    $str = fgets(STDIN);
    $arr = explode(" ", $str);
    $arr = array_map(function ($in) {
        return (int)$in;
    }, $arr);
    return $arr;
}

$n = read_int();
$arr = read_int_array();
if (1 == count($arr)) {
    echo "1" . PHP_EOL;
} else {
    $i = 0;
    $j = 1;
    while (true) {
        if ($j >= count($arr)) {
            break;
        }
        if ($arr[$j] < $arr[$i]) {
            $j++;
        } else {
            $i = $j;
            $j = $i + 1;
        }
    }
    echo ($i + 1) . PHP_EOL;
}

