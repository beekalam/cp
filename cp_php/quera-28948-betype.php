<?php
// 2021-12-01
$s = fgets(STDIN);
$stack = [];
foreach (str_split($s) as $c) {
    if ($c == '=') {
        if (count($stack) > 0) {
            array_pop($stack);
        }
    } else {
        $stack[] = $c;
    }
}
echo implode($stack) . PHP_EOL;
