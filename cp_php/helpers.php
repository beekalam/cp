<?php
if (!function_exists('generate')) {
    function generate($start, $end, $step = 1)
    {
        for ($i = $start; $i < $end; $i += $step) {
            yield $i;
        }
    }
}

// if (!function_exists('dump')) {
//     function dump($input)
//     {
//         if (is_array($input) || is_object($input)) {
//             $input = print_r($input, true);
//         }
//         echo $input . PHP_EOL;
//     }
// }


if (!function_exists('dd')) {
    function dd($input)
    {
        dump($input);
        die();
    }
}

if (!function_exists('len')) {
    function len($input)
    {
        if (is_array($input)) {
            return count($input);
        }
        if (is_string($input)) {
            return strlen($input);
        }
    }
}

