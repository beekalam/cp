<?php
// require_once __DIR__ . "/vendor/autoload.php";

class IO
{
    public static function stdin_stream()
    {
        while ($line = fgets(STDIN)) {
            yield $line;
        }
    }

    public static function read_int_array()
    {
        $str = fgets(STDIN);
        $arr = explode(" ", $str);
        $arr = array_map(function ($in) {
            return (int)$in;
        }, $arr);
        return $arr;
    }

    public static function readline()
    {
        return trim(fgets(STDIN));
    }

    public static function read_int()
    {
        $str = trim(fgets(STDIN));
        return (int)$str;
    }
}
//----------------------------------------------------------------
$n = IO::read_int();
$arr = IO::read_int_array();
sort($arr);
$ans = [];
$i = 0;
$j = count($arr) - 1;
while ($i <= $j) {
    if ($i == $j) {
        $ans[] = $arr[$i];
    } else {
        $ans[] = $arr[$j];
        $ans[] = $arr[$i];
    }
    $j--;
    $i++;
}
echo implode(' ', $ans) . PHP_EOL;