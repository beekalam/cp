<?php
// 2021-12-02

// require_once __DIR__ . "/vendor/autoload.php";

class IO
{
    public static function stdin_stream()
    {
        while ($line = fgets(STDIN)) {
            yield $line;
        }
    }

    public static function read_int_array()
    {
        $str = fgets(STDIN);
        $arr = explode(" ", $str);
        $arr = array_map(function ($in) {
            return (int)$in;
        }, $arr);
        return $arr;
    }

    public static function readline()
    {
        return trim(fgets(STDIN));
    }

    public static function read_int()
    {
        $str = trim(fgets(STDIN));
        return (int)$str;
    }
}

//-------------------------------------------------------
$arr = [];
$user_map = [];
$max = 0;
foreach (range(1, 4) as $i) {
    $a = IO::read_int_array();
    $arr[$i] = $a;
    foreach ($a as $j) {
        $user_map[$j] = $i;
    }
    $max = max($max, max($a));
}
// dump($user_map);

print($user_map[$max]);
