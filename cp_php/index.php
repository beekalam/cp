<?php

use PHPUnit\Framework\TestCase;

require __DIR__ . '/bootstrap.php';

function charAtPos($r, $s)
{
    $res = [];
    if ($s === 'even') {
        for ($i = 0; $i < len($r); $i++) {
            if ($i % 2 === 0) {
                $res[] = $r[$i];
            }
        }
    }
    if ($s === 'odd') {
        for ($i = len($r) - 1; $i > 0; $i--) {
            if ($i % 2 === 1) {
                $res[] = $r[$i];
            }
        }
        $res = array_reverse($res);
        dump($res);
    }

    if (is_array($r)) {
        dump("res: ");
        dump($res);
        return $res;
    }

    return implode("", $res);
}

class CharAtPos extends TestCase
{
    public function tests()
    {
        // $this->assertEquals(charAtPos("EDABIT", "even"), "EAI");
        // $this->assertEquals(charAtPos("EDABIT", "odd"), "DBT");
        // $this->assertEquals(charAtPos("QWERTYUIOP", "even"), "QETUO");
        // $this->assertEquals(charAtPos("POIUYTREWQ", "odd"), "OUTEQ");
        // $this->assertEquals(charAtPos("ASDFGHJKLZ", "odd"), "SFHKZ");
        // $this->assertEquals(charAtPos("ASDFGHJKLZ", "even"), "ADGJL");
        $this->assertEquals(charAtPos([2, 4, 6, 8, 10], "even"), [4, 8]);
        // $this->assertEquals(charAtPos([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "odd"), [2, 4, 6, 8, 10]);
        // $this->assertEquals(charAtPos(["!", "@", "#", "$", "%", "^", "&", "*", "(", ")"], "odd"), ["@", "$", "^", "*", ")"]);
        // $this->assertEquals(charAtPos([")", "(", "*", "&", "^", "%", "$", "#", "@", "!"], "odd"), ["(", "&", "%", "#", "!"]);
        // $this->assertEquals(charAtPos(["A", "R", "B", "I", "T", "R", "A", "R", "I", "L", "Y"], "odd"), ["A", "B", "T", "A", "I", "Y"]);
    }
}