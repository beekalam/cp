<?php
$fmt = 'Y-m-d';
$today = date_create_from_format($fmt, '2018-10-12');

$date = $_POST['date'];
$date = date_create_from_format($fmt, $date);

if ($date < $today) {
    echo "gone";
} else {
    echo $date->diff($today)->days;
}

