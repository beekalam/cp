<?php
// 2021-12-03

$json = json_decode(file_get_contents(__DIR__ . "/students.json"), true);
$ans = [];
// var_dump($json);
foreach ($json as $item) {
    if (!isset($ans[$item['id']])) {
        $ans[$item['id']] = [
            'bdate' => $item['bdate'],
            'name'  => ucwords(strtolower($item['name'])),
            'age'   => strval(date_diff(date_create($item['bdate']), date_create("2019/10/4"))->y),
        ];
    }
}
file_put_contents(__DIR__ . "/students_fixed.json", json_encode($ans));
//-------------------------------------------------------------------------------
// require_once __DIR__ . "/../../vendor/autoload.php";
//
// use Assert\Assertion;
//
// $ans = file_get_contents(__DIR__ . "/students_fixed.json");
// $exp = '{"9830011":{"bdate":"1994\/9\/24","name":"Sardar Azmoun","age":"25"},"9864656":{"bdate":"1992\/5\/14","name":"Elon Musk","age":"27"}}';
// Assertion::eq($ans, $exp);
