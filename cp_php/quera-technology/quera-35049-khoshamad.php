<?php
function read_int()
{
    $str = trim(fgets(STDIN));
    return (int)$str;
}

$n = read_int();
echo sprintf("Hello CodeCup %d!", $n) . PHP_EOL;
