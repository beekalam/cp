<?php
require_once __DIR__ . "/../../vendor/autoload.php";
$url = "https://atofighi.github.io/codecup4-problem/index.html";
$html = file_get_contents($url);

class HTMLParser
{
    public $content;

    public function __construct()
    {

    }

    public function getContent($url)
    {
        $this->content = file_get_contents($url);
        return $this;
    }

    public function getTags($tag)
    {
        $count = 0;
        $result = [];
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->preserveWhiteSpace = false;
        $dom->loadHTML($this->content);
        $elements = $dom->getElementsByTagName($tag);
        foreach ($elements as $node) {
            $result[$count]['value'] = trim(
                preg_replace('/\s+/', ' ', $node->nodeValue));
            if ($node->hasAttributes()) {
                foreach ($node->attributes as $name => $attr) {
                    $result[$count]['attributes'][$name] =
                        $attr->value;
                }
            }
            $count++;
        }
        return $result;
    }
}

$htmlparser = new HTMLParser();
$url = "https://atofighi.github.io/codecup4-problem/index.html";
$htmlparser->getContent($url);
$res = $htmlparser->getTags('a');
$url = parse_url($url, PHP_URL_SCHEME | PHP_URL_HOST | PHP_URL_PATH);
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
var_dump($actual_link);
$newres = [];
foreach ($res as $r) {
    if (\Illuminate\Support\Str::startsWith($r["value"], "User #")) {
        $user = strstr($r["value"], "#");
        $newres[$user] = $r["attributes"]["href"];
    }
}

var_dump($newres);
// echo $htmlparser->content;
