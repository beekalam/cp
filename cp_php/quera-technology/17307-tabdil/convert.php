<?php
$file = $_GET['path'];
// $file = "file.txt";
$path = __DIR__ . "/$file";
$arr = array_map('trim', explode(PHP_EOL, file_get_contents($path)));
$date = date_create_from_format("d F Y", $arr[0]);

$ans = [];
foreach (array_slice($arr, 1) as $s) {
    if ($s) {
        list($user, $day) = explode(':', $s);
        $time = $date;
        if ($day === 'tomorrow') {
            $d = date_create_from_format("d F Y", $arr[0]);
            $time = $d->add(date_interval_create_from_date_string("1 day"));
        } elseif ($day === 'yesterday') {
            $d = date_create_from_format("d F Y", $arr[0]);
            $time = $d->sub(date_interval_create_from_date_string("1 day"));
        }
        $ans[] = [
            'user' => $user,
            'time' => $time->format('Y-m-d')
        ];
    }
}
// var_dump($ans);
file_put_contents(__DIR__ . "/INFO.json", json_encode($ans));
// var_dump(json_decode(file_get_contents(__DIR__."/INFO.json")));
