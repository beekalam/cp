<?php

class MD5Hash implements HashStrategy
{
    private static $instance = null;

    private function __construct()
    {
    }

    public function hash($data)
    {
        return md5($data);
    }

    public static function getInstance()
    {
        if (is_null(MD5Hash::$instance)) {
            MD5Hash::$instance = new self();
        }
        return MD5Hash::$instance;
    }
}
