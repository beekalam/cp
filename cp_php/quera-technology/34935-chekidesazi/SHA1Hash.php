<?php

class SHA1Hash implements HashStrategy
{
    private static $instance = null;

    private function __construct()
    {
    }


    public function hash($data)
    {
        return sha1($data);
    }

    public static function getInstance()
    {
        if (is_null(SHA1Hash::$instance)) {
            SHA1Hash::$instance = new self();
        }
        return SHA1Hash::$instance;
    }
}