<?php

use Assert\Assertion;

require_once __DIR__ . "/HashStrategy.php";
require_once __DIR__ . "/MD5Hash.php";
require_once __DIR__ . "/SHA1Hash.php";
require_once __DIR__ . "/HashFactory.php";


$factory = new HashFactory();

$factory->register('sha1', SHA1Hash::getInstance());

$factory->register('md5', MD5Hash::getInstance());

// $hasher = $factory->make($_GET['strategy']);

// echo $hasher->hash($_GET['data']);

//------------------------------------------
require_once "../vendor/autoload.php";
Assertion::isInstanceOf($factory->make('sha1'), SHA1Hash::class);
Assertion::isInstanceOf($factory->make('md5'), MD5Hash::class);
// Assertion::isInstanceOf($factory->make('invalid'), Exception::class);
$instance = new SHA1Hash();




