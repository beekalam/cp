<?php
// @notsolved: 2021-12-03
// @notesolved: 2021-12-09 there's something wrong with the judge.

function findPhoneNumbers($input)
{
    $pattern = '@(\s09\d{9}\s)|(\s[+]9891\d{8}\s)\b@';
    if (preg_match_all($pattern, $input, $matches)) {
        return array_map('trim', $matches[0]);
    }
    return [];
}

function findHashtags($input)
{
    $pattern = '/\s#[a-zA-Z0-9]{3,}\s/';
    if (preg_match_all($pattern, $input, $matches)) {
        return array_map('trim', $matches[0]);
    }
    return [];
}

function boldEmails($input)
{
    $pattern = '/(\b\s\w+[._]*@\w+\.[a-zA-z]{3}\s)/';
    return trim(preg_replace($pattern, '<b>$1</b>', $input));
}

//------------------------------------------------------------------
// require_once __DIR__ . "/../../vendor/autoload.php";
//
// use Assert\Assertion;
//
// $res = findPhoneNumbers("In shomareye mane: 09101007567 vali behtare +989101007567 ro save koni. In 9111231234 va0914513 kar nemikonan.");
// Assertion::eq($res, array(
//     0 => '09101007567',
//     1 => '+989101007567'
// ));
//
// $res = findHashtags("Salam#bad #goodMOrning khoobi#to #4yourlove #bi-man");
// Assertion::eq($res, array(
//     0 => '#goodMOrning',
//     1 => '#4yourlove'
// ));
// //
// $res = boldEmails("Soalatono az info@test.com info_test@Quera.ir ya info@Quera123.com ya test_#23@alaki.core beporsid");
// // print($res);
// $exp = 'Soalatono az info_test@Quera.ir ya <b>info@Quera123.com</b> ya test_#23@alaki.core beporsid';
// // Assertion::eq($res, $exp);
//
//
// print_r(boldEmails("Soalatono az info_test@Quera.ir ya info@Quera123.com ya test_#23@alaki.core beporsid"));
