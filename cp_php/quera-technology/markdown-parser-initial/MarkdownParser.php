<?php

class MarkdownParser
{
    public $rules = [];

    public function __construct()
    {
        $default_rules = [
            'Header',
            'Bold',
            'Italic',
            'Image',
            'Link',
            'Code',
            'HorizontalRule'
        ];
        foreach ($default_rules as $rule) {
            if (class_exists($rule)) {
                $this->rules[] = new $rule();
            }
        }
    }

    public function addRule($rule)
    {
        $this->rules[] = $rule;
    }

    public function render($content)
    {
        foreach ($this->rules as $rule) {
            $content = $rule->parse($content);
        }

        return $content;
    }
}

require_once __DIR__ . "/autoload.php";
$m = new MarkdownParser();
$content = <<<EOT
# heading
##heading
###   heading
####  heading
#####    heading
######heading
**bold 1**
__bold 2__
*italic 1*
_italic 2_
EOT;
$b = new Header();
$bold = new Bold();
$italic = new Italic();
// var_dump($b->parse($content));
// var_dump($bold->parse($content));
// var_dump($m->render($content));
var_dump($italic->parse($content));
