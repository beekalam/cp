<?php

class Bold extends RegexRule
{


    public function parse($content)
    {
        return preg_replace($this->rule(), $this->replacement(), $content);
    }

    public function rule()
    {

        return "@(\*{2}(([[:alnum:]]+(?!= $) ?)+)\*{2})|(\_{2}(([[:alnum:]]+(?!= $) ?)+)\_{2})@";
        // return "@\*\*(\w+)\*\*@";
    }

    public function replacement()
    {
        return '<b>$2$4</b>' . PHP_EOL;
    }
}

