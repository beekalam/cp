<?php

class Header extends RegexRule
{

    public function parse($content)
    {
        return preg_replace($this->rule(), $this->replacement(), $content);
    }

    public function rule()
    {
        return "@#+\s*(\w+\s*)@";
    }

    public function replacement()
    {
        return '<h3>$1</h3>' . PHP_EOL;
    }
}

