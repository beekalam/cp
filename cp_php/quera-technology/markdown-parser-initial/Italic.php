<?php

class Italic extends RegexRule
{


    public function parse($content)
    {
        $regex2 = "/(\s(\_(?![*\s])(?:[^*]*[^*\s])?\_)\s)/";
        $replace2 = '<i>$1</i>';
        $content = preg_replace($this->rule(), $this->replacement(), $content);
        return $content;
        $content = preg_replace($regex2, $replace2, $content);
        return trim('-', trim($content, '*'));
    }

    public function rule()
    {
        $regex = "/\s(\*(?![*\s])(?:[^*]*[^*\s])?\*)\s/";
        return $regex;
        // return sprintf("@\b(\*%s\*)\b|(\_%s\_)@", $words, $words);
    }

    public function replacement()
    {
        return '<i>$1</i>';
    }
}

