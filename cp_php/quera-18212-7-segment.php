<?php
// @notsolved: 2021-12-03
require_once __DIR__ . "/vendor/autoload.php";

function has_fractional_part($input)
{
    return $input - floor($input) != 0;
}

$s = trim(fgets(STDIN));
// $s = "2.3e10";
$seven_segment = [
    0 => 6,
    1 => 2,
    2 => 5,
    3 => 5,
    4 => 4,
    5 => 5,
    6 => 6,
    7 => 3,
    8 => 7,
    9 => 6
];
$parts = explode('e', $s);
$first = floatval($parts[0]);
$second = intval($parts[1]);
// dump($first);
// dump(has_fractional_part($first));
while ($second > 1 && has_fractional_part($first)) {
    $second = $second / 10;
    $first = $first * 10;
}
$first = intval($first);
if ($second > 1) {
    $first = $first * $second;
}
dump($first);
$ans = 0;
foreach (str_split($first) as $i) {
    if ($i != '.') {
        $ans += $seven_segment[$i];
    }
}
// $ans += $second * $seven_segment[0];
echo $ans . PHP_EOL;
