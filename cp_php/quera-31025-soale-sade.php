<?php
// 2021-12-01
require_once __DIR__ . "/vendor/autoload.php";


function read_int_array()
{
    $str = fgets(STDIN);
    $arr = explode(" ", $str);
    $arr = array_map(function ($in) {
        return (int)$in;
    }, $arr);
    return $arr;
}

list($n, $k) = read_int_array();
while ($k) {
    $n = $n / 2.0;
    $k--;
}
echo floor($n) . PHP_EOL;
