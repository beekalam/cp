<?php
// 2021-12-02
// require_once __DIR__ . "/vendor/autoload.php";

class IO
{
    public static function stdin_stream()
    {
        while ($line = fgets(STDIN)) {
            yield $line;
        }
    }

    public static function read_int_array()
    {
        $str = fgets(STDIN);
        $arr = explode(" ", $str);
        $arr = array_map(function ($in) {
            return (int)$in;
        }, $arr);
        return $arr;
    }

    public static function readline()
    {
        return trim(fgets(STDIN));
    }

    public static function read_int()
    {
        $str = trim(fgets(STDIN));
        return (int)$str;
    }
}


$n = IO::read_int();
$a = IO::readline();
$m = IO::read_int();
$o = IO::readline();

$i = 0;
foreach (str_split($a) as $c) {
    if ($i < $m && $o[$i] == $c) {
        $i++;
    } else {
        break;
    }
}

$ans = 0;
if ($i == 0) {
    $ans = $n + $m;
} else {
    $ans = ($n - $i) + ($m - $i);
}

echo $ans . PHP_EOL;
