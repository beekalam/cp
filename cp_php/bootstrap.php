<?php

require __DIR__ . '/vendor/autoload.php';

// Active assert and make it quiet
assert_options(ASSERT_ACTIVE, true);
assert_options(ASSERT_BAIL, true);
assert_options(ASSERT_WARNING, false);

// Create a handler function
function my_assert_handler($file, $line, $code)
{
    echo "Assertion Failed:
        File '$file'\n 
        Line '$line'\n
        Code '$code'";
}

// Set up the callback
assert_options(ASSERT_CALLBACK, 'my_assert_handler');
