<?php
// @notsolved: 2021-12-02
// require_once __DIR__ . "/vendor/autoload.php";


function read_int()
{
    $str = trim(fgets(STDIN));
    return (int)$str;
}

function read_int_array()
{
    $str = fgets(STDIN);
    $arr = explode(" ", $str);
    $arr = array_map(function ($in) {
        return (int)$in;
    }, $arr);
    return $arr;
}

function build_array($r, $c, $default = 0)
{
    if ($r == 0)
        throw new InvalidArgumentException("r cannot be empty");
    $arr = range(0, $r - 1);
    if ($c > 0) {
        foreach ($arr as &$row) {
            $row = array_fill(0, $c, $default);
        }
    }
    return $arr;
}

list($m, $n) = read_int_array();
$k = read_int();
foreach (range(0, $k - 1) as $i) {
    $arr[] = read_int_array();
}

$ans = build_array($m, $n);

foreach ($arr as $point) {
    list($i, $j) = $point;
    $ans[$i - 1][$j - 1] = '*';
}

for ($i = 0; $i < $m; $i++) {
    for ($j = 0; $j < $n; $j++) {

        $cross_join = [[0, 1], [0, -1], [1, 0], [-1, 0],
            [1, 1], [1, -1], [-1, 1], [-1, -1]];

        foreach ($cross_join as $cj) {
            list($ii, $jj) = $cj;
            $ni = $i + $ii;//- 1;
            $nj = $j + $jj;//- 1;
            if ($ans[$i][$j] != '*' and
                $ni >= 0 and $nj >= 0 and $ni < $m and
                $nj < $n and $ans[$ni][$nj] == '*') {
                $ans[$i][$j] += 1;
            }
        }
    }
}
foreach ($ans as $row) {
    echo implode(' ', $row) . "\n";
}
