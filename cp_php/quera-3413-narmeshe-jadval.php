<?php
require_once __DIR__ . "/vendor/autoload.php";

function read_int()
{
    $str = trim(fgets(STDIN));
    return (int)$str;
}

function read_int_array()
{
    $str = fgets(STDIN);
    $arr = explode(" ", $str);
    $arr = array_map(function ($in) {
        return (int)$in;
    }, $arr);
    return $arr;
}

function build_array($r, $c, $default = 0)
{
    if ($r == 0)
        throw new InvalidArgumentException("r cannot be empty");
    $arr = range(0, $r - 1);
    if ($c > 0) {
        foreach ($arr as &$row) {
            $row = array_fill(0, $c, $default);
        }
    }
    return $arr;
}

function readline()
{
    return trim(fgets(STDIN));
}

function rotate_matrix($arr)
{
    $new_arr = [];
    $c = count($arr[0]);
    for ($j = 0, $jMax = count($arr[0]); $j < $jMax; $j++) {
        $row = [];
        for ($i = 0, $iMax = count($arr); $i < $iMax; $i++) {
            $row[] = $arr[$i][$j];
        }
        $new_arr[] = $row;
    }
    if (count($new_arr) > 0) {
        $last_row = $new_arr[count($new_arr) - 1];
        $new_arr[count($new_arr) - 1] = array_reverse($last_row);
    }

    return $new_arr;
}

function make_vertically_symmetric($mat)
{
}

function print_matrix($mat)
{
    foreach ($mat as $row) {
        echo implode(' ', $row) . PHP_EOL;
    }
}

$mat = [
    str_split('WWB'),
    str_split('BWB'),
    str_split('WWW')
];

// $mat = rotate_matrix($mat);
// print_matrix($mat);
// exit(0);


function writeln($input)
{
    echo $input . PHP_EOL;
}

$n = read_int();
$arr = [];
foreach (range(0, $n - 1) as $i) {
    $arr[] = str_split(readline());
}
writeln(print_r($arr, true));
$q = read_int();
foreach (range(0, $q - 1) as $z) {
    $line = readline();
    writeln($line);
    if ($line == 'H') {

    } elseif ($line == 'V') {

    } elseif ($line == '90') {
        rotate($arr);
    }
}