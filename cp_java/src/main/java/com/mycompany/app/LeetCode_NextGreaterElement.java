package com.mycompany.app;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class LeetCode_NextGreaterElement {
    public static void test() {
        var c = new LeetCode_NextGreaterElement();
        int[] nums1 = {1,2,3};
        int[] nums2 = {1,3,4,2};
        var ans = c.nextGreaterElement(nums1,nums2);
        for (int i : ans){
            System.out.println(i);
        }
    }

    public int[] nextGreaterElement(int[] findNums, int[] nums) {
        Map<Integer, Integer> map = new HashMap<>(); // map from x to next greater element of x
        Stack<Integer> stack = new Stack<>();
        for (int num : nums) {
            while (!stack.isEmpty() && stack.peek() < num)
                map.put(stack.pop(), num);
            stack.push(num);
        }
        for (int i = 0; i < findNums.length; i++)
            findNums[i] = map.getOrDefault(findNums[i], -1);
        return findNums;
    }
}
