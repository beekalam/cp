package com.mycompany.app;

import java.util.StringJoiner;

public class Helpers {

    public static void PrintArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            System.out.print(",");
        }
        System.out.println();
    }
}
