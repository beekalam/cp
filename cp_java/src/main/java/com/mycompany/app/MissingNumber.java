// 2022-01-09
package com.mycompany.app;

import java.util.Arrays;

public class MissingNumber {
    public static int findMissingnumber(int[] nums) {
        int i = 0;
        while (i < nums.length) {
            if (nums[i] < nums.length && nums[i] != nums[nums[i]]) {
                swap(nums, i, nums[i]);
            } else {
                i++;
            }
//            System.out.print("i: ");
//            System.out.println(i);
            Helpers.PrintArray(nums);
        }


        for (i = 0; i < nums.length; i++) {
            if (nums[i] != i)
                return i;
        }

        return nums.length;
    }

    private static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public static void _main() {
        System.out.println(MissingNumber.findMissingnumber(new int[]{
                4, 0, 3, 1
        }));
    }
}
