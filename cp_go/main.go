package main

// 2021-12-16

import (
	"fmt"
	"gitlab.com/beekalam/go_cp/leetcode"
	"strings"
)

func solve2(s string) {
	s = strings.TrimSpace(s)
	s = strings.ToLower(s)
	st := []string{}
	words := []string{"bale", "kheir", "areh", "na"}
	ans := "YES"
	for i, _ := range s {
		for _, w := range words {
			if i+len(w) <= len(s) && s[i:i+len(w)] == w {
				st = append(st, w)
			}
		}
	}
	//fmt.Println(st)
	//fmt.Println(len(st))
	if len(st) < 1 || len(st)%2 == 1 {
		ans = "NO"
	} else {
		for len(st) > 0 {
			//fmt.Println(st)
			head := st[0]
			st = st[1:]
			tail := st[len(st)-1]
			st = st[:len(st)-1]
			if head == "bale" && tail != "kheir" {
				ans = "NO"
			}
			if head == "areh" && tail != "na" {
				ans = "NO"
			}
			if head == "kheir" || head == "na" {
				ans = "NO"
			}
		}
	}

	fmt.Println(ans)
}

func solve(s string) {
	s = strings.TrimSpace(s)
	s = strings.ToLower(s)
	st := []string{}
	words := []string{"bale", "kheir", "areh", "na"}
	m := make(map[string]int)
	m["bale"] = 0
	m["areh"] = 0
	ans := "YES"
	for i, _ := range s {
		for _, w := range words {
			if i+len(w) <= len(s) && s[i:i+len(w)] == w {
				st = append(st, w)
				switch w {
				case "bale":
					m["bale"] += 1
				case "areh":
					m["areh"] += 1
				case "kheir":
					m["bale"] -= 1
				case "na":
					m["areh"] -= 1
				}
			}
		}
	}
	if m["bale"] != 0 || m["areh"] != 0 {
		ans = "No"
	}
	if len(st) < 1 || len(st)%2 == 1 {
		ans = "NO"
	} else {
		for len(st) > 0 {
			//fmt.Println(st)
			head := st[0]
			st = st[1:]
			tail := st[len(st)-1]
			st = st[:len(st)-1]
			if head == "bale" && tail != "kheir" {
				ans = "NO"
			}
			if head == "areh" && tail != "na" {
				ans = "NO"
			}
			if head == "kheir" || head == "na" {
				ans = "NO"
			}
		}
	}
	fmt.Println(ans)
}

func main() {
	//leetcode.TestLongestConsecutiveSequence()
	// leetcode.MainWays()
	//n := "9245"
	//fmt.Println(strconv.Atoi(string(n[0])))
	// s := ""
	//s := "areh na areh na"
	//s := "balex==1arehy==1y++nakheir"
	//s := "baleball==1na"
	//s := "balearehkheirna"
	// _, _ = fmt.Scanf("%s\n", &s)
	// solve(s)
}
