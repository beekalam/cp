package leetcode

func deleteDuplicates(head *ListNode) *ListNode {
	tmp := head
	for tmp != nil {
		tmp2 := tmp.Next
		for tmp2 != nil && tmp2.Val == tmp.Val {
			tmp2 = tmp2.Next
		}
		tmp.Next = tmp2
		tmp = tmp.Next
	}
	return head
}
