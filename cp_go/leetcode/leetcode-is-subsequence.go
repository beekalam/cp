package leetcode

// 2021-12-15
func isSubsequence(s string, t string) bool {
	if len(s) == 0 && len(t) != 0 {
		return true
	}
	i := 0
	j := 0
	for i < len(s) && j < len(t) {
		if s[i] == t[j] {
			i += 1
			j += 1
		} else {
			j += 1
		}
	}

	if i == len(s) {
		return true
	}
	return false
}
