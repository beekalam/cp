package leetcode

import "fmt"

func hammingWeight(num uint32) int {
	var c int = 0
	for num != 0 {
		if num&1 == 1 {
			c += 1
		}
		num >>= 1
	}
	return c
}

func Demo() {
	fmt.Println(hammingWeight(00000000000000000000000000001010))
}
