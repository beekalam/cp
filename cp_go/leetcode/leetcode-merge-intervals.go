// 2021-12-18
package leetcode

import (
	"fmt"
	"math"
	"sort"
)

// @snippet custom type sort
type Interval [][]int

func (interval Interval) Len() int {
	return len(interval)
}

func (interval Interval) Less(i, j int) bool {
	ii := interval[i]
	jj := interval[j]
	return ii[0] < jj[0]
}

func (interval Interval) Swap(i, j int) {
	tmp := interval[j]
	interval[j] = interval[i]
	interval[i] = tmp
}
func (interval Interval) print() {
	for _, item := range interval {
		fmt.Println(item)
	}
}
func Overlaps(interval, interval2 []int) bool {
	if interval[0] >= interval2[0] && interval[0] <= interval2[1] {
		return true
	}

	if interval[1] >= interval2[0] && interval[1] <= interval2[0] {
		return true
	}
	return false

}

func merge(intervals [][]int) [][]int {
	ninterval := Interval(intervals)
	sort.Sort(ninterval)
	//ninterval.print()
	tmp := make([]int, 0)
	ans := make([][]int, 0)
	for _, item := range ninterval {
		if len(tmp) == 0 {
			tmp = item
		} else if Overlaps(tmp, item) || Overlaps(item, tmp) {
			tmp[0] = int(math.Min(float64(tmp[0]), float64(item[0])))
			tmp[1] = int(math.Max(float64(tmp[1]), float64(item[1])))
		} else {
			ans = append(ans, tmp)
			tmp = item
		}
	}
	if len(tmp) != 0 {
		ans = append(ans, tmp)
	}
	return ans
}

func TestMerge() {

	intervals := make([][]int, 0)
	intervals = append(intervals, []int{2, 6})
	intervals = append(intervals, []int{1, 3})
	intervals = append(intervals, []int{8, 10})
	intervals = append(intervals, []int{15, 18})
	fmt.Println(merge(intervals))
}
