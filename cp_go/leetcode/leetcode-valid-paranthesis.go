package leetcode

import (
	"fmt"
	"strings"
)

func isValid(s string) bool {
	var stack []string
	m := map[string]string{"(": ")", "[": "]", "{": "}"}
	opening := "([{"
	closing := ")}]"
	for _, c := range s {
		cs := fmt.Sprintf("%q", c)
		if strings.Contains(opening, cs) {
			stack = append(stack, cs)
			fmt.Println(stack)
		} else if strings.Contains(closing, cs) {
			if len(stack) == 0 {
				return false
			} else {
				top := fmt.Sprintf("%q", stack[len(stack)-1])
				if m[string(c)] != top {
					return false
				} else {
					stack = stack[:len(stack)]
				}
			}
		}
	}
	if len(stack) != 0 {
		return false
	}
	return true
}

func TestIsValid() {
	s := "()"
	fmt.Println(isValid(s))
}
