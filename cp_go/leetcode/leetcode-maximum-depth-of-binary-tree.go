package leetcode

import "math"

func max(a, b int) int {
	return int(math.Max(float64(a), float64(b)))
}

func depth(root *TreeNode) int {
	if root == nil {
		return 0
	}

	return max(1+depth(root.Left), 1+depth(root.Right))
}
func maxDepth(root *TreeNode) int {
	return int(depth(root))
}
