// 2021-12-11
package leetcode

import "fmt"

func maxint(a int, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
func maxSubArray(nums []int) int {
	rs := nums[0]
	mx := rs
	for i, val := range nums {
		if i == 0 {
			continue
		}
		mx = maxint(rs, mx)
		if val >= 0 {
			if rs > 0 {
				rs += val
			} else {
				rs = val
			}
		} else if val < 0 {
			if rs > 0 && rs+val > 0 {
				rs += val
			} else {
				rs = val
			}
		} else {
			rs = val
		}
		mx = maxint(rs, mx)
	}
	return mx
}

func MainMaxSubArray() {
	arr := []int{-2, 1, -3, 4, -1, 2, 1, -5, 4}
	print(maxSubArray(arr)) // 6
	arr2 := []int{-2, -1}

	fmt.Println()
	print(maxSubArray(arr2)) //-1
}
