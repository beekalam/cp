package leetcode

import (
	"strconv"
)

func addBinary(a string, b string) string {
	if len(b) > len(a) {
		return addBinary(b, a)
	}
	ans := ""
	carry := 0
	for i, j := len(a)-1, len(b)-1; i >= 0; i -= 1 {
		da, _ := strconv.Atoi(string(a[i]))
		sum := da + carry

		if j >= 0 {
			db, _ := strconv.Atoi(string(b[j]))
			sum += db
		}
		j -= 1
		carry = 0

		if sum == 3 {
			carry = 1
			sum = 1
		} else if sum == 2 {
			carry = 1
			sum = 0
		}
		ans = strconv.Itoa(sum) + ans
	}
	if carry == 1 {
		ans = "1" + ans
	}
	return ans
}

func MainAddBinary() {
	print(addBinary("11", "1"))
}
