package leetcode

// @notsolved: 2021-12-15

import "fmt"

func StringToMap(s string) map[string]int {
	m := make(map[string]int)
	for _, c := range s {
		sc := string(c)
		if _, ok := m[sc]; ok {
			m[sc] += 1
		} else {
			m[sc] = 1
		}
	}
	return m
}

// https://leetcode.com/problems/repeated-substring-pattern/discuss/1622970/97-fast
func repeatedSubstringPattern(s string) bool {
	n := len(s)
	dp := make([]int, n)
	dp[0] = 0
	for i, j := 0, 1; j < n; j += 1 {
		fmt.Println(dp)
		if s[i] == s[j] {
			dp[j] = i + 1
			i++
		} else {
			for i > 0 {
				i = dp[i-1]
				if s[i] == s[j] {
					break
				}
			}
			if s[i] == s[j] {
				dp[j] = i + 1
				i++
			} else {
				dp[j] = 0
			}
		}
	}
	fmt.Println(dp)
	return dp[n-1]%(n-dp[n-1]) == 0 && dp[n-1] != 0
}
func TestRepeatedSubstringPattern() {
	fmt.Println(repeatedSubstringPattern("abab"))
	//fmt.Println(repeatedSubstringPattern("aba"))
	//fmt.Println(repeatedSubstringPattern("abcabcabcabc"))
	//fmt.Println(repeatedSubstringPattern("abac"))
	//fmt.Println(repeatedSubstringPattern("ababba"))
}
