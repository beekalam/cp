package leetcode

var ret []int

func inOrder(root *TreeNode) {
	if root == nil {
		return
	}

	if root.Left != nil {
		inOrder(root.Left)
	}
	ret = append(ret, root.Val)
	if root.Right != nil {
		inOrder(root.Right)
	}

}

func inorderTraversal(root *TreeNode) []int {
	// ret = make([]int,0)
	ret = []int{}
	inOrder(root)
	return ret
}
