// 2021-12-21
package leetcode

import (
	"fmt"
	"math"
)

func longestConsecutive(nums []int) int {

	maxInt := func(a int, b int) int {
		return int(math.Max(float64(a), float64(b)))
	}
	contains := func(m map[int]int, k int) bool {
		_, ok := m[k]
		return ok
	}
	makeMap := func(n []int) map[int]int {
		m := make(map[int]int)
		for _, v := range n {
			m[v] = 0
		}
		return m
	}

	m := makeMap(nums)
	mx := 0

	for _, k := range nums {

		if !contains(m, k-1) {
			current_num := k
			current_streak := 1
			for contains(m, current_num+1) {
				current_num += 1
				current_streak += 1
			}
			mx = maxInt(mx, current_streak)
		}

	}
	return mx
}

func TestLongestConsecutiveSequence() {
	nums := []int{100, 4, 200, 1, 3, 2}
	fmt.Println(longestConsecutive(nums))

	nums = []int{0, 3, 7, 2, 5, 8, 4, 6, 0, 1}
	fmt.Println(longestConsecutive(nums))

}
