// 2021-12-16
package leetcode

import (
	"errors"
	"fmt"
	"math"
)

func IntsToMap(s []int) map[int]int {
	m := make(map[int]int)
	for _, c := range s {
		if _, ok := m[c]; ok {
			m[c] += 1
		} else {
			m[c] = 1
		}
	}
	return m
}

func MaxKeyValueInt(s map[int]int) (error, int, int) {
	if len(s) == 0 {
		return errors.New("array is empty"), 0, 0
	}
	fmt.Printf("s: %v\n", s)
	var max_key *int
	var max_value *int
	for k, v := range s {
		if max_key == nil {
			max_key = new(int)
			max_value = new(int)
			*max_key = k
			*max_value = v
		} else if v > *max_value {
			*max_value = v
			*max_key = k
		}
	}
	return nil, *max_key, *max_value
}

func IndexOfInt(a []int, x int) int {
	for i, n := range a {
		if x == n {
			return i
		}
	}
	return -1
}

func LastIndexInt(a []int, x int) int {
	for i := len(a) - 1; i >= 0; i-- {
		if x == a[i] {
			return i
		}
	}
	return -1
}
func findShortestSubArray(nums []int) int {
	m := IntsToMap(nums)
	_, _, degree_value := MaxKeyValueInt(m)
	mn := len(nums) + 1
	for _, v := range nums {
		if m[v] == degree_value {
			mn = int(math.Min(float64(LastIndexInt(nums, v))-float64(IndexOfInt(nums, v)), float64(mn)))
		}
	}
	return mn + 1

}

func TestFuncShortestSubArray() {
	//fmt.Println(findShortestSubArray([]int{1, 2, 2, 3, 1}))
	nums := []int{1, 2, 2, 3, 1, 4, 2}
	fmt.Println(findShortestSubArray(nums))
}
