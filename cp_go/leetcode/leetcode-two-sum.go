// 2021-12-13
package leetcode

import "fmt"

func twoSum(nums []int, target int) []int {
	seen := make(map[int]int)
	for i, val := range nums {
		fmt.Printf("i: %d\n", i)
		rem := target - val
		fmt.Printf("rem: %d\n", rem)
		if _, ok := seen[rem]; ok {
			return []int{i, seen[rem]}
		}
		seen[val] = i
	}
	return []int{}
}

func TestTwoSum() {
	nums := []int{2, 7, 11, 15}
	target := 9
	fmt.Print(twoSum(nums, target))
}
