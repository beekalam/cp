// 2021-12-11
package leetcode

import (
	"testing"
)

func TestMainMaxSubArray(t *testing.T) {
	arr := []int{-2, 1, -3, 4, -1, 2, 1, -5, 4}
	ans := maxSubArray(arr)
	if ans != 6 {
		t.Errorf("test1")
	}

}

func TestMainMaxSubArray2(t *testing.T) {
	arr2 := []int{-2, -1}
	ans := maxSubArray(arr2)
	if ans != -1 {
		t.Errorf("test2")
	}

}
