package leetcode

import "fmt"

func longestCommonPrefix(strs []string) string {
	ans := ""

	candid := strs[0]
	for i := 0; i < 200; i += 1 {
		for _, s := range strs {
			if len(s) <= i {
				return ans
			}
			if candid[i] != s[i] {
				return ans
			}
		}
		ans += string(candid[i])
	}
	return ans

}
func TestLongestCommonPrefix() {
	strs := []string{"flower", "flow", "flight"}
	fmt.Println(longestCommonPrefix(strs))

}
