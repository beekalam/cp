// 2021-12-10
package leetcode

import "fmt"

func findDuplicates(nums []int) []int {
	freq := make(map[int]int)
	for _, val := range nums {
		if _, ok := freq[val]; ok {
			freq[val] += 1
		} else {
			freq[val] = 1
		}
	}
	fmt.Println(freq)
	ans := make([]int, 0)
	for i, val := range freq {
		if val == 2 {
			ans = append(ans, i)
		}
	}
	return ans
}

func MainFindDuplicates() {
	nums := []int{4, 3, 2, 7, 8, 2, 3, 1}
	fmt.Println(findDuplicates(nums))
}
