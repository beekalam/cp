package leetcode

func hasCycle(head *ListNode) bool {
	m := make(map[*ListNode]int)
	var current *ListNode = head
	for current != nil {
		if _, ok := m[current]; ok {
			return true
		}
		m[current] = 1
		current = current.Next
	}
	return false
}
