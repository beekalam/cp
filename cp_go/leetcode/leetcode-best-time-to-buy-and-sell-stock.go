// 2021-12-20
package leetcode

import "math"

func maxProfit(prices []int) int {
	ans := 0
	current := -1
	for _, i := range prices {
		if current == -1 {
			current = i
		} else if current > i {
			current = i
		} else {
			ans = int(math.Max(float64(ans), float64(i-current)))
		}
	}
	return ans
}
