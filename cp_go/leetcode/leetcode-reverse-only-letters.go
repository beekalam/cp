package leetcode

import (
	"regexp"
	"strings"
)

func reverseOnlyLetters(s string) string {
	IsLetter := regexp.MustCompile(`^[a-zA-Z]+$`).MatchString
	i := 0
	j := len(s) - 1
	ss := strings.Split(s, "")
	for i < j {
		if !IsLetter((ss[i])) {
			i++
			continue
		}
		if !IsLetter((ss[j])) {
			j--
			continue
		} else {
			ss[i], ss[j] = ss[j], ss[i]
			i++
			j--
		}
	}
	return strings.Join(ss, "")
}
