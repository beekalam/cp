package leetcode

// 2021-12-15

import (
	"strconv"
)

func btoi(s byte) int {
	res, _ := strconv.Atoi(string(s))
	return res
}

func addStrings(num1 string, num2 string) string {
	if len(num2) > len(num1) {
		return addStrings(num2, num1)
	}
	ans := ""
	carry := 0
	j := len(num2) - 1
	for i := len(num1) - 1; i >= 0; i -= 1 {
		sum := carry + btoi(num1[i])
		carry = 0
		if j >= 0 {
			sum += btoi(num2[j])
			j = j - 1
		}
		if sum >= 10 {
			carry = 1
			sum = sum - 10
		}
		ans = strconv.Itoa(sum) + ans
	}
	if carry == 1 {
		ans = "1" + ans
	}
	return ans
}
