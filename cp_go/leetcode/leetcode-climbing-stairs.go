package leetcode
import "fmt"

var ways int

func climbStairs(n int) int {
	cache := make([]int, n+1)
	cache[0] = 1
	for i := 1; i < n+1; i += 1 {
		res := 0
		for _, x := range []int{1, 2} {
			if i-x >= 0 {
				res += cache[i-x]
			}
		}
		cache[i] = res
	}
	return cache[n]
}

func MainWays() {
	fmt.Println(climbStairs(2))
	fmt.Println(climbStairs(3))
	fmt.Println(climbStairs(4))
}
