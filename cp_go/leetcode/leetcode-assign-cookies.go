package leetcode

import (
	"fmt"
	"sort"
)

func findContentChildren(g []int, s []int) int {
	sort.Ints(g)
	sort.Ints(s)
	ans := 0
	j := 0
	for _, v := range s {
		if j < len(g) && g[j] <= v {
			ans++
			j++
		}
	}
	return ans

}

func TestFindContentChildren() {
	g := []int{1, 2, 3}
	s := []int{1, 1}
	fmt.Println(findContentChildren(g, s))
	g = []int{1, 2}
	s = []int{1, 2, 3}
	fmt.Println(findContentChildren(g, s))
	g = []int{10, 9, 8, 7}
	s = []int{5, 6, 7, 8}
	fmt.Println(findContentChildren(g, s))
}
