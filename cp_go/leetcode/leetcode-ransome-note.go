package leetcode

// 2021-12-15

func counter(s string) map[string]int {
	m := make(map[string]int)
	for _, c := range s {
		sc := string(c)
		if _, ok := m[sc]; ok {
			m[sc] += 1
		} else {
			m[sc] = 1
		}
	}
	return m
}

func canConstruct(ransomNote string, magazine string) bool {
	r := counter(ransomNote)
	m := counter(magazine)
	for k, v := range r {
		if _, ok := m[k]; ok {
			if m[k] < v {
				return false
			}
		} else {
			return false
		}
	}
	return true
}
