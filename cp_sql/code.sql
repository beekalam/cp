-- @notsolved: first and second are good
-- https://www.db-fiddle.com/f/noxU5qGpTkHtjY9wGgKVcZ/3
-- Section1
   select name
   from singer
   where balance = 0;
-- Section2
	create table producer(
      name varchar(20),
      primary key (`name`)
    );
    insert into producer(name)
    select distinct song.producer from song;
-- Section3
   select singer.name,sum(song.total_sell) as total_sell,producer
   from song
   left join singer on song.singer = singer.name
   where producer = 'dr.dre'
   group by singer.name
   order by total_sell DESC
   limit 1,1;
