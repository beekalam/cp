-- https://www.db-fiddle.com/f/kbpowTk9FmTXL42mZxgjby/12
-- https://quera.ir/problemset/17906/
-- Section1
  insert into Player(id,team,age)
  select id,"Chelsea" as team,24 as age
  from Person
  where Person.id not in
  (
    select id from Player
    union 
    select id from Coach
    union 
    select id from Refree
  );

-- Section2
  select c.id,Person.name from Coach c
  left join Player p on c.id = p.id
  left join Person on c.id = Person.id
  where p.team <> c.team and c.id in (
    select id from Player);
  
-- Section3
	Alter table Player
    add foreign key (team) references Team(name)
    on delete cascade;
    
