-- https://www.db-fiddle.com/f/473Ki6nBEz4Kt6vvPjeMut/3
-- https://quera.ir/problemset/102260/
-- Section1
	select max(salary) as salary from employees;
-- Section2
	select teams.name as team, employees.name as employee, maxes.salary
	  from employees
		     left join (select teams.id as team_id, max(employees.salary) as salary
	    	              from employees
	    	                   left join teams on employees.team_id = teams.id
	    	             group by teams.id) maxes on maxes.team_id = employees.team_id
	       left join teams on teams.id = maxes.team_id
	 where employees.salary = maxes.salary
	 order by team desc;
